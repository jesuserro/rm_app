import 'package:test/test.dart';

void main() {
  group('Sanity test', () {

    test('Test framework should be sane', () async {
      expect(2, lessThan(5));
    });
  });
}
