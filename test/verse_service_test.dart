import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:test/test.dart';
import 'package:time_machine/time_machine.dart';

void main() {
  group('VerseService', () {

    test('Verse should be committed', () {
      Verse data = Verse('reference', 'passage',
        commit: null,
        review: null,
        level: -1,
      );
      Verse result = VerseService().committed(data);
      expect(result.commit, LocalDate.today());
      expect(result.review, null);
      expect(result.due, LocalDate.today());
      expect(result.level, 0);
    });

    test('Verse should be forgotten', () {
      Verse data = Verse('reference', 'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 3,
      );
      Verse result = VerseService().forgotten(data);
      expect(result.commit, LocalDate(2021, 1, 1));
      expect(result.review, LocalDate.today());
      expect(result.due, LocalDate.today());
      expect(result.level, 0);
    });

    test('Verse should be remembered', () {
      Verse data = Verse('reference', 'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 3,
      );
      Verse result = VerseService().remembered(data);
      expect(result.commit, LocalDate(2021, 1, 1));
      expect(result.review, LocalDate.today());
      expect(result.due, LocalDate.today().addDays(8));
      expect(result.level, 4);
    });

    test('Verse should be moved to due', () {
      Verse data = Verse('reference', 'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 3,
      );
      Verse result = VerseService().movedToDue(data);
      expect(result.commit, LocalDate(2021, 1, 1));
      expect(result.review, LocalDate.today().subtractDays(2));
      expect(result.due, LocalDate.today());
      expect(result.level, 2);
    });

    test('Verse should be moved to new', () {
      Verse data = Verse('reference', 'passage',
        commit: LocalDate(2021, 1, 1),
        review: LocalDate(2021, 1, 1),
        level: 3,
      );
      Verse result = VerseService().movedToNew(data);
      expect(result.commit, null);
      expect(result.review, null);
      expect(result.due, null);
      expect(result.level, -1);
    });

  });
}
