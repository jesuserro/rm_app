import 'package:remem_me/models/models.dart';
import 'package:remem_me/models/study/study.dart';
import 'package:remem_me/services/study_service.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:test/test.dart';

const isOutOfWordsException = TypeMatcher<OutOfWordsException>();

const _testLines = [
  ['one'],
  ['two', 'three'],
  ['four', 'five', 'six'],
];

void main() {

  group('passageToWords', () {
    test('Should handle simple phrase', () {
      String passage = 'One, two "three".';
       List<List<String>> result = TextService().textToWords(passage);
      expect(result, equals([['One,', 'two', '"three".']]));
    });
    test('Should handle multiple lines', () {
      String passage = 'One\n two "three".';
       List<List<String>> result = TextService().textToWords(passage);
      expect(result, equals([['One'], ['two', '"three".']]));
    });
    test('Should add subsequent non-word to preceding word', () {
      String passage = 'One - two, "three".';
      List<List<String>> result = TextService().textToWords(passage);
      expect(result, equals([['One -', 'two,', '"three".']]));
    });
    test('Should add preceding non-word to subsequent word', () {
      String passage = 'One\n- two, "three".';
       List<List<String>> result = TextService().textToWords(passage);
      expect(result, equals([['One'], ['- two,', '"three".']]));
    });
    test('Should add non-word before line break to preceding word', () {
      String passage = 'One -\ntwo, "three".';
       List<List<String>> result = TextService().textToWords(passage);
      expect(result, equals([['One -'], ['two,', '"three".']]));
    });
    test('Should handle three dots', () {
      String passage = 'One, two, ...\nthree';
      List<List<String>> result = TextService().textToWords(passage);
      expect(result, equals([['One,', 'two, ...'], ['three']]));
    });
    test('Should handle three dots in separate line', () {
      String passage = 'One,\n...\nthree';
      List<List<String>> result = TextService().textToWords(passage);
      expect(result, equals([['One,'], ['...'], ['three']]));
    });
  });

  group('LineUp', () {
    test('Should advance to first word', () {
      Pos result = StudyService().advanceByWord(_testLines, Pos(0, -1));
      expect(result, Pos(0, 0));
    });

    test('Should advance to last word', () {
      Pos result = StudyService().advanceByWord(_testLines, Pos(2, 1));
      expect(result, Pos(2, 2));
    });

    test('Should advance to next line', () {
      Pos result = StudyService().advanceByWord(_testLines, Pos(1, 1));
      expect(result, Pos(2, 0));
    });

    test('Advancing by word should throw exception', () {
      expect(() => StudyService().advanceByWord(_testLines, Pos(2, 2)),
          throwsA(isOutOfWordsException));
    });

    test('Should advance to end of line', () {
      Pos result = StudyService().advanceByLine(_testLines, Pos(0, 1));
      expect(result, Pos(1, 1));
    });

    test('Should advance to last line', () {
      Pos result = StudyService().advanceByLine(_testLines, Pos(1, 1));
      expect(result, Pos(2, 2));
    });

    test('Advancing by line should throw exception', () {
      expect(() => StudyService().advanceByLine(_testLines, Pos(2, 2)),
          throwsA(isOutOfWordsException));
    });

    test('Should retract word from last word', () {
      Pos result = StudyService().retractWord(_testLines, Pos(2, 2));
      expect(result, Pos(2, 1));
    });

    test('Should retract word to previous line', () {
      Pos result = StudyService().retractWord(_testLines, Pos(2, 0));
      expect(result, Pos(1, 1));
    });

    test('Should retract word before first word', () {
      Pos result = StudyService().retractWord(_testLines, Pos(0, 0));
      expect(result, Pos(0, -1));
    });

    test('Should not retract word', () {
      Pos result = StudyService().retractWord(_testLines, Pos(0, -1));
      expect(result, Pos(0, -1));
    });

    test('Should retract line', () {
      Pos result = StudyService().retractLine(_testLines, Pos(2, 2));
      expect(result, Pos(1, 1));
    });

    test('Should retract line to beginning', () {
      Pos result = StudyService().retractLine(_testLines, Pos(0, 1));
      expect(result, Pos(0, -1));
    });

    test('Should not retract line', () {
      Pos result = StudyService().retractLine(_testLines, Pos(0, -1));
      expect(result, Pos(0, -1));
    });

    test('Should look ahead', () {
      Pos result = StudyService().lookAhead(_testLines, Pos(0, -1));
      expect(result, Pos(0, 0));
    });

    test('Should look line ahead', () {
      Pos result = StudyService().lookAhead(_testLines, Pos(1, 1));
      expect(result, Pos(2, 2));
    });

    test('Looking ahead should throw exception', () {
      expect(() => StudyService().lookAhead(_testLines, Pos(2, 2)),
          throwsA(isOutOfWordsException));
    });
  });

  group('Typing', () {
    test('Should accept single character', () {
      bool result = TextService().startsWith('word', 'w');
      expect(result, isTrue);
    });
    test('Should accept multiple characters', () {
      bool result = TextService().startsWith('word', 'wo');
      expect(result, isTrue);
    });
    test('Should deny wrong beginning', () {
      bool result = TextService().startsWith('word', 'o');
      expect(result, isFalse);
    });
    test('Should skip quote', () {
      bool result = TextService().startsWith('"word', 'w');
      expect(result, isTrue);
    });
  });

  group('Puzzle', () {
    test('Choice should contain one at the solution index', () {
      Choice result = StudyService().puzzle(_testLines, Pos(0, 0));
      expect(result.solution, equals(result.options.indexOf('one')));
    });
    test('Choice should contain three at the solution index', () {
      Choice result = StudyService().puzzle(_testLines, Pos(1, 1));
      expect(result.solution, equals(result.options.indexOf('three')));
    });
    test('Choice should contain five at the solution index', () {
      Choice result = StudyService().puzzle(_testLines, Pos(2, 1));
      expect(result.solution, equals(result.options.indexOf('five')));
    });
  });

}
