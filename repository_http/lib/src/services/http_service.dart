import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:message_core/message_core.dart';
import 'package:repository_core/repository_core.dart';
import 'package:repository_http/repository_http.dart';

import '../utils.dart';


class HttpService {
  static HttpService? _instance;
  final ProgressBloc progressBloc; // todo: is ProgressBloc still needed?

  factory HttpService(ProgressBloc progressBloc) {
    if (_instance == null) {
      _instance = HttpService._internal(progressBloc);
    }
    return _instance!;
  }

  HttpService._internal(this.progressBloc);


  Future<http.Response> getList(String path,
      int? account, bool? deleted, Map<String, String> headers) async {
    Map<String, String> params = {'deleted': deleted.toString()};
    if(account != null) params['account'] = account.toString();
    final uri = await replicationUri('/v1/$path', params);
    final response = await http.get(uri, headers: headers);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    return response;
  }


  Future<http.Response> getItem(String path, int id,
      Map<String, String> headers) async {
    final response = await http
        .get(await replicationUri('/v1/$path$id/'), headers: headers);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    return response;
  }


  Future<http.Response?> postList(String path, List<Entity> entities,
      Map<String, String> headers) async {
    http.Response? response;
    for (final entity in entities) {
      response = await postItem(path, entity, headers);
      if (response.statusCode != 201) break;
    }
    progressBloc.add(ProgressStopped(entities[0].id));
    return response;
  }


  Future<http.Response> postItem(String path, Entity entity,
      Map<String, String> headers) async {
    final response = await http.post(await replicationUri('/v1/$path'),
        body: jsonEncode(entity.toJson()), headers: headers);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    return response;
  }


  Future<http.Response?> patchList(String path, List<Entity> entities,
      Map<String, String> headers) async {
    http.Response? response;
    for (final entity in entities) {
      response = await patchItem(path, entity, headers);
      if (response.statusCode != 200) break;
    }
    return response;
  }


  Future<http.Response?> deleteList(String path, List<Entity> entities,
      Map<String, String> headers) async {
    http.Response? response;
    for (final entity in entities) {
      response = await deleteItem(path, entity, headers);
      if (response.statusCode != 204) break;
    }
    return response;
  }


  Future<http.Response> patchItem(String path, Entity entity,
      Map<String, String> headers) async {
    final response = await http.patch(
        await replicationUri('/v1/$path${entity.id}/'),
        body: jsonEncode(entity.toJson()),
        headers: headers);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    return response;
  }


  Future<http.Response> deleteItem(String path, Entity entity,
      Map<String, String> headers) async {
    final response = await http.delete(
        await replicationUri('/v1/$path${entity.id}/'),
        body: jsonEncode(entity.toJson()),
        headers: headers);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    return response;
  }

}
