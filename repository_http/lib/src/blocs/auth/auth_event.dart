import 'package:equatable/equatable.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object?> get props => [];
}

class AuthLoadedFromSettings extends AuthEvent {}

class AuthTokenRefreshed extends AuthEvent {
  final String? accessToken;
  final String? refreshToken;

  const AuthTokenRefreshed({required this.accessToken, this.refreshToken});

  @override
  List<Object?> get props => [accessToken, refreshToken];

  @override
  String toString() => 'AuthTokenRefreshed { authToken: $accessToken, '
      'refreshtoken: $refreshToken }';
}


class PasswordForgotten extends AuthEvent {
  final String email;

  const PasswordForgotten({required this.email});

  @override
  List<Object> get props => [email];

  @override
  String toString() => 'PasswordForgotten { email: $email }';
}


abstract class Authenticated extends AuthEvent {
  final String email;
  final String password;

  const Authenticated({required this.email, required this.password});

  @override
  List<Object> get props => [email, password];

  @override
  String toString() => 'Authenticated { email: $email, password: $password }';
}


class Registered extends Authenticated {
  const Registered({required String email, required String password})
      : super(email: email, password: password);
}


class LoggedIn extends Authenticated {
  const LoggedIn({required String email, required String password})
      : super(email: email, password: password);
}

class LoggedOut extends AuthEvent {}
