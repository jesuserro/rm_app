import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:repository_hive/repository_hive.dart';

import '../../../repository_http.dart';
import 'auth.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  late AuthService service;

  AuthBloc() : super(NoAuth()) {
    service = AuthService(this);
  }

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is AuthLoadedFromSettings) {
      yield* mapAuthLoadedFromSettingsToState();
    } else if (event is LoggedIn) {
      yield* mapLoggedInToState(event);
    } else if (event is PasswordForgotten) {
      yield* mapPasswordForgottenToState(event);
    } else if (event is Registered) {
      yield* mapRegisteredToState(event);
    } else if (event is LoggedOut) {
      yield* mapLoggedOutToState();
    } else if (event is AuthTokenRefreshed) {
      yield* mapAuthTokenRefreshedToState(event);
    }
  }

  Stream<AuthState> mapAuthLoadedFromSettingsToState() async* {
    var accessToken = await Settings().load(Settings.ACCESS_TOKEN);
    if (accessToken != null) {
      var refreshToken = await Settings().load(Settings.REFRESH_TOKEN);
      yield LoginSuccess(accessToken: accessToken, refreshToken: refreshToken);
    } else {
      yield NoAuth();
    }
  }

  Stream<AuthState> mapPasswordForgottenToState(PasswordForgotten event) async* {
    yield AuthInProgress();
    yield await service.forgotPassword(email: event.email);
  }

  Stream<AuthState> mapLoggedInToState(Authenticated event) async* {
    yield AuthInProgress();
    AuthState result =
        await service.login(email: event.email, password: event.password);
    if (result is LoginSuccess) {
      Settings().save(Settings.ACCESS_TOKEN, result.accessToken!);
      Settings().save(Settings.REFRESH_TOKEN, result.refreshToken!);
      Settings().save(Settings.USER_EMAIL, event.email);
    }
    yield result;
  }

  Stream<AuthState> mapRegisteredToState(Authenticated event) async* {
    yield AuthInProgress();
    yield await service.register(email: event.email, password: event.password);
  }

  Stream<AuthState> mapAccountAttachedToState(Authenticated event) async* {
    yield AuthInProgress();
    yield await service.register(email: event.email, password: event.password);
  }

  Stream<AuthState> mapAuthTokenRefreshedToState(
      AuthTokenRefreshed event) async* {
    Settings().save(Settings.ACCESS_TOKEN, event.accessToken!);
    Settings().save(Settings.REFRESH_TOKEN, event.refreshToken!);
    yield LoginSuccess(
        accessToken: event.accessToken,
        refreshToken: event.refreshToken);
  }

  Stream<AuthState> mapLoggedOutToState() async* {
    Settings().delete(Settings.ACCESS_TOKEN);
    Settings().delete(Settings.REFRESH_TOKEN);
    Settings().delete(Settings.USER_EMAIL);
    yield NoAuth();
  }
}
