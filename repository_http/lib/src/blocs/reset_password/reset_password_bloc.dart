import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../repository_http.dart';
import 'reset_password.dart';

class ResetPasswordBloc extends Bloc<PasswordSubmitted, ResetPasswordState> {
  final PasswordService service;
  final String uidb64;
  final String token;

  ResetPasswordBloc(
      {required this.service, required this.uidb64, required this.token})
      : super(InitialResetPasswordState());

  @override
  Stream<ResetPasswordState> mapEventToState(PasswordSubmitted event) async* {
    if (event is PasswordSubmitted) {
      yield* mapPasswordSubmittedToState(event);
    }
  }

  Stream<ResetPasswordState> mapPasswordSubmittedToState(
      PasswordSubmitted event) async* {
    yield ResetPasswordInProgress(password: event.password);
    yield await service.submitPassword(
        password: event.password, uidb64: uidb64, token: token);
  }
}
