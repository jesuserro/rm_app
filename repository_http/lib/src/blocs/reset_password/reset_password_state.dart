import 'package:equatable/equatable.dart';

abstract class ResetPasswordState extends Equatable {

  const ResetPasswordState();

  @override
  List<Object?> get props => [];
}


class InitialResetPasswordState extends ResetPasswordState {}


class ResetPasswordInProgress extends ResetPasswordState {
  final String password;

  ResetPasswordInProgress({required this.password})
      : super();

  @override
  List<Object?> get props => [password];
}


class ResetPasswordFailure extends ResetPasswordState {
  final String password;
  final Map<String, List<String?>> messages;

  const ResetPasswordFailure(
      {required this.password, required this.messages})
      : super();

  @override
  List<Object?> get props => [password, messages];

  @override
  String toString() {
    return 'ResetPasswordFailure { messages: $messages }';
  }
}


class ResetPasswordSuccess extends ResetPasswordState {
  final Map<String, List<String>> messages;

  const ResetPasswordSuccess(
      {required this.messages})
      : super();

  @override
  List<Object?> get props => [messages];

  @override
  String toString() {
    return 'ResetPasswordSuccess { messages: $messages }';
  }
}
