import 'package:equatable/equatable.dart';

class PasswordSubmitted extends Equatable {
  final String password;

  const PasswordSubmitted(this.password);

  @override
  List<Object?> get props => [this.password];

  @override
  String toString() {
    return 'PasswordSubmitted { password: $password }';
  }

}

