import 'package:repository_core/repository_core.dart';

import '../../repository_http.dart';
import 'entities_repository.dart';

class TagsRepositoryHttp extends EntitiesRepositoryHttp<TagEntity>
    implements TagsRepository {
  static TagsRepositoryHttp? _instance;

  factory TagsRepositoryHttp(AuthService authService, HttpService httpService) {
    if (_instance == null) {
      _instance =
          TagsRepositoryHttp._internal('tags/', authService: authService, httpService: httpService);
    }
    return _instance!;
  }

  TagsRepositoryHttp._internal(String path, {required authService, required httpService})
      : super(path, authService: authService, httpService: httpService);

  @override
  TagEntity parseItem(Map<String, dynamic>? json) {
    return TagEntity.fromJson(json!);
  }

  @override
  List<TagEntity> sample() {
    return [
      TagEntity(
        'Bible Study',
        id: 1,
      ),
      TagEntity(
        'God\'s Unfailing Love',
        id: 2,
      ),
      TagEntity(
        'Human Failure',
        id: 3,
      ),
    ];
  }
}
