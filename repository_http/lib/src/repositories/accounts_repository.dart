import 'package:repository_core/repository_core.dart';
import 'package:repository_http/src/services/http_service.dart';

import '../../repository_http.dart';
import 'entities_repository.dart';

class AccountsRepositoryHttp extends EntitiesRepositoryHttp<AccountEntity>
    implements AccountsRepository {
  static AccountsRepositoryHttp? _instance;

  factory AccountsRepositoryHttp(
      AuthService authService, HttpService httpService) {
    if (_instance == null) {
      _instance = AccountsRepositoryHttp._internal('accounts/',
          authService: authService, httpService: httpService);
    }
    return _instance!;
  }

  AccountsRepositoryHttp._internal(String path,
      {required authService, required httpService})
      : super(path, authService: authService, httpService: httpService);

  @override
  AccountEntity parseItem(Map<String, dynamic>? json) {
    return AccountEntity.fromJson(json!);
  }

  @override
  List<AccountEntity> sample() {
    return [
      AccountEntity(
        'Test',
        id: 1,
      ),
      AccountEntity(
        'Demo',
        id: 2,
      ),
      AccountEntity(
        'My Verses',
        id: 3,
      ),
    ];
  }
}
