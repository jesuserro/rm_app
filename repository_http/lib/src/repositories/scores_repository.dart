import 'package:repository_core/repository_core.dart';

import '../../repository_http.dart';
import 'entities_repository.dart';

class ScoresRepositoryHttp extends EntitiesRepositoryHttp<ScoreEntity>
    implements ScoresRepository {
  static ScoresRepositoryHttp? _instance;

  factory ScoresRepositoryHttp(
      AuthService authService, HttpService httpService) {
    if (_instance == null) {
      _instance = ScoresRepositoryHttp._internal('scores/',
          authService: authService, httpService: httpService);
    }
    return _instance!;
  }

  ScoresRepositoryHttp._internal(String path,
      {required authService, required httpService})
      : super(path, authService: authService, httpService: httpService);

  @override
  ScoreEntity parseItem(Map<String, dynamic>? json) {
    return ScoreEntity.fromJson(json!);
  }
}
