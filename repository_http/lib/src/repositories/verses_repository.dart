import 'package:repository_core/repository_core.dart';
import 'package:time_machine/time_machine.dart';

import '../../repository_http.dart';
import 'entities_repository.dart';

class VersesRepositoryHttp extends EntitiesRepositoryHttp<VerseEntity>
    implements VersesRepository {
  static VersesRepositoryHttp? _instance;

  factory VersesRepositoryHttp(
      AuthService authService, HttpService httpService) {
    if (_instance == null) {
      _instance = VersesRepositoryHttp._internal('verses/',
          authService: authService, httpService: httpService);
    }
    return _instance!;
  }

  VersesRepositoryHttp._internal(String path,
      {required AuthService authService, required HttpService httpService})
      : super(path, authService: authService, httpService: httpService);

  @override
  VerseEntity parseItem(Map<String, dynamic>? json) {
    return VerseEntity.fromJson(json!);
  }

  List<VerseEntity> sample() {
    return [
      VerseEntity(
        'Joshua 1:8',
        'This Book of the Law shall not depart from your mouth, but you shall meditate on it day and night, so that you may be careful to do according to all that is written in it.'
            'For then you will make your way prosperous, and then you will have good success.',
        source: 'ESV',
        topic: 'Word of God',
        level: null,
        id: 1,
        commit: LocalDate.today(),
        tags: {1: null, 2: null},
      ),
      VerseEntity(
        'Isaiah 12:1',
        'In that day you will sing:'
            '“I will praise you, O Lord!'
            'You were angry with me, but not any more.'
            'Now you comfort me.',
        source: 'NIV',
        topic: 'Comfort',
        level: 3,
        id: 2,
        tags: {2: null},
      ),
      VerseEntity(
        '1 Kings 18:21',
        'How long will you go limping between two different opinions? '
            'If the Lord is God, follow him;'
            'but if Baal, then follow him.',
        level: 0,
        id: 3,
        commit: LocalDate.today().subtractDays(10),
        review: LocalDate.today(),
        tags: {},
      ),
      VerseEntity(
        'Psalm 16:11',
        'Lord, You will show me the way of life.',
        source: 'NIVUK',
        level: 23,
        id: 4,
        tags: {},
      ),
      VerseEntity(
        'Luke 23:42',
        'And he said, “Jesus, remember me when you come into your kingdom.”',
        topic: 'Remember Me',
        level: 12,
        id: 5,
        commit: LocalDate.today().subtractDays(10),
        review: LocalDate.today().subtractDays(4),
        tags: {2: null, 3: null},
      ),
    ];
  }
}
