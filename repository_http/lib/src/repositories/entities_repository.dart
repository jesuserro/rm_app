import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart';
import 'package:repository_core/repository_core.dart';
import 'package:repository_http/src/services/http_service.dart';

import '../../repository_http.dart';

abstract class EntitiesRepositoryHttp<T extends Entity>
    implements EntitiesRepository<T> {
  final String path;
  final AuthService authService;
  final HttpService httpService;

  EntitiesRepositoryHttp(this.path,
      {required this.authService, required this.httpService});

  @override
  Future<T> read(int id) async {
    final response = await (authService
            .call((headers) => httpService.getItem(path, id, headers)));
    return parseItem(jsonDecode(utf8.decode(response.body.codeUnits)));
  }

  @override
  Future create(T entity) async {
    final response = await authService
        .call((headers) => httpService.postItem(path, entity, headers));
  }

  @override
  Future update(T entity) async {
    final response = await authService
        .call((headers) => httpService.patchItem(path, entity, headers));
  }

  @override
  Future delete(T entity) async {
    final response = await authService
        .call((headers) => httpService.deleteItem(path, entity, headers));
  }

  @override
  Future<List<T>> readList({int? account, bool? deleted = false}) async {
    final response = await (authService.call(
            (headers) => httpService.getList(path, account, deleted, headers)));
    return parseList(jsonDecode(utf8.decode(response.body.codeUnits)));
  }

  @override
  Future createList(List<T> entities) async {
    final response = await authService
        .call((headers) => httpService.postList(path, entities, headers));
  }

  @override
  Future updateList(List<T> entities) async {
    final response = await authService
        .call((headers) => httpService.patchList(path, entities, headers));
  }

  @override
  Future deleteList(List<T> entities) async {
    final response = await authService
        .call((headers) => httpService.deleteList(path, entities, headers));
  }

  List<T> sample() {
    return [];
  }

  List<T> parseList(List<dynamic> items) {
    return items.map((item) => parseItem(item as Map<String, dynamic>)).toList();
  }

  T parseItem(Map<String, dynamic>? json);
}
