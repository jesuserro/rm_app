import 'package:repository_core/repository_core.dart';

import '../../repository_http.dart';
import 'entities_repository.dart';

class DecksRepositoryHttp extends EntitiesRepositoryHttp<DeckEntity>
    implements DecksRepository {
  static DecksRepositoryHttp? _instance;

  factory DecksRepositoryHttp(
      AuthService authService, HttpService httpService) {
    if (_instance == null) {
      _instance = DecksRepositoryHttp._internal('decks/',
          authService: authService, httpService: httpService);
    }
    return _instance!;
  }

  DecksRepositoryHttp._internal(String path,
      {required authService, required httpService})
      : super(path, authService: authService, httpService: httpService);

  @override
  DeckEntity parseItem(Map<String, dynamic>? json) {
    return DeckEntity.fromJson(json!);
  }

  @override
  Future<void> delete(DeckEntity entity) async {
    final response = await authService
        .call((headers) => httpService.deleteItem(path, entity, headers));
  }
}
