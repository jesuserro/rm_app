import 'dart:async';

import 'package:repository_hive/repository_hive.dart';

// const String REPLICATION_SERVER = 'http://192.168.1.110:8000';  // lan
// const String LEGACY_SERVER = 'http://192.168.1.110:8001';  // lan
// const String REPLICATION_SERVER = 'http://air.local:8000'; // ios
// const String LEGACY_SERVER = 'http://air.local:8001'; // ios
const REPLICATION_SERVER = 'https://rpl.remem.me'; // production
const LEGACY_SERVER = 'https://my.remem.me'; // production

Future<Uri> replicationUri(String unencodedPath,
    [Map<String, dynamic>? queryParameters]) async {
  return await _uri(Settings.REPLICATION_SERVER, REPLICATION_SERVER,
      unencodedPath, queryParameters);
}

Future<Uri> legacyUri(String unencodedPath,
    [Map<String, dynamic>? queryParameters]) async {
  return await _uri('', LEGACY_SERVER, unencodedPath, queryParameters);
}

Future<Uri> _uri(String settingsKey, String defaultAddress,
    String unencodedPath, Map<String, dynamic>? queryParameters) async {
  final address = await (Settings()
      .load(settingsKey, defaultValue: defaultAddress));
  final parts = address!.split('://');
  if (parts[0] == 'https') {
    return Uri.https(parts[1], unencodedPath, queryParameters);
  } else {
    return Uri.http(parts[1], unencodedPath, queryParameters);
  }
}
