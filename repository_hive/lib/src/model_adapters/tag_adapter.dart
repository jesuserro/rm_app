import 'package:hive/hive.dart';
import 'package:repository_core/repository_core.dart';
import 'package:repository_hive/src/model_adapters/entity_adapter.dart';

class TagAdapter extends EntityAdapter<TagEntity> {
  @override
  final int typeId;

  TagAdapter(this.typeId);

  @override
  int get numOfFields => super.numOfFields + 3;

  @override
  TagEntity create(Map<int, dynamic> fields) {
    return TagEntity(fields[1] as String,
        included: fields[2] as bool?,
        account: fields[3] as int?,
        id: fields[0]);
  }

  @override
  int writeFields(BinaryWriter writer, TagEntity obj) {
    var index = super.writeFields(writer, obj);
    writer
      ..writeByte(index++)
      ..write(obj.text)
      ..writeByte(index++)
      ..write(obj.included)
      ..writeByte(index++)
      ..write(obj.account);
    return index;
  }
}
