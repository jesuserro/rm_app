import 'package:hive/hive.dart';
import 'package:repository_core/repository_core.dart';

abstract class EntityAdapter<T extends Entity> extends TypeAdapter<T> {

  int get numOfFields => 1;

  @override
  T read(BinaryReader reader) {
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return create(fields);
  }

  T create(Map<int, dynamic> fields);

  @override
  void write(BinaryWriter writer, T obj) {
    writeFields(writer, obj);
  }

  int writeFields(BinaryWriter writer, T obj) {
    var index = 0;
    writer
      ..writeByte(index++)
      ..write(obj.id);
    return index;
  }
}
