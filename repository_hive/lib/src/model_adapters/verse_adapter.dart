import 'package:hive/hive.dart';
import 'package:repository_core/repository_core.dart';
import 'package:repository_hive/src/model_adapters/entity_adapter.dart';

class VerseAdapter extends EntityAdapter<VerseEntity> {
  @override
  final int typeId;

  VerseAdapter(this.typeId);

  @override
  int get numOfFields => super.numOfFields + 10;


  @override
  VerseEntity create(Map<int, dynamic> fields) {
    return VerseEntity(fields[1], fields[2],
        source: fields[3],
        topic: fields[4],
        image: fields[5],
        commit: fields[6],
        review: fields[7],
        level: fields[8],
        tags: fields[9] != null ? Map<int, String>.from(fields[9]) : {},
        account: fields[10],
        id: fields[0]);
  }


  @override
  int writeFields(BinaryWriter writer, VerseEntity obj) {
    var index = super.writeFields(writer, obj);
    writer
      ..writeByte(index++)
      ..write(obj.reference)
      ..writeByte(index++)
      ..write(obj.passage)
      ..writeByte(index++)
      ..write(obj.source)
      ..writeByte(index++)
      ..write(obj.topic)
      ..writeByte(index++)
      ..write(obj.image)
      ..writeByte(index++)
      ..write(obj.commit)
      ..writeByte(index++)
      ..write(obj.review)
      ..writeByte(index++)
      ..write(obj.level)
      ..writeByte(index++)
      ..write(obj.tags)
      ..writeByte(index++)
      ..write(obj.account);
    return index;
  }
}