import 'package:hive/hive.dart';
import 'package:repository_core/repository_core.dart';
import 'package:repository_hive/src/model_adapters/entity_adapter.dart';

class AccountAdapter extends EntityAdapter<AccountEntity> {
  @override
  final int typeId;

  AccountAdapter(this.typeId);

  @override
  int get numOfFields => super.numOfFields + 6;

  @override
  AccountEntity create(Map<int, dynamic> fields) {
    return AccountEntity(fields[1] as String,
        language: fields[2] as String,
        langRef: fields[3] as String?,
        reviewLimit: fields[4] as int?,
        dailyGoal: fields[5] as int?,
        reviewFrequency: fields[6] as double?,
        id: fields[0]);
  }

  @override
  int writeFields(BinaryWriter writer, AccountEntity obj) {
    var index = super.writeFields(writer, obj);
    writer
      ..writeByte(index++)
      ..write(obj.name)
      ..writeByte(index++)
      ..write(obj.language)
      ..writeByte(index++)
      ..write(obj.langRef)
      ..writeByte(index++)
      ..write(obj.reviewLimit)
      ..writeByte(index++)
      ..write(obj.dailyGoal)
      ..writeByte(index++)
      ..write(obj.reviewFrequency);
    return index;
  }
}
