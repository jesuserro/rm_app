import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:repository_core/repository_core.dart';
import 'package:repository_hive/repository_hive.dart';

abstract class EntitiesRepositoryHive<T extends Entity>
    implements EntitiesRepository<T> {
  final String boxName;

  Box<T?>? _box;

  EntitiesRepositoryHive(this.boxName);


  Future<Box<T>> getBox() async {
    await RepositoryHiveService().isInitialized.firstWhere((bool isInitialized) => isInitialized);
    print('Getting box $boxName');
    return Hive.openBox(boxName);
  }


  @override
  Future<List<T>> readList({ int? account, bool? deleted = false }) async {
    if (_box == null) _box = await getBox();
    print('Entities from box $boxName: ${_box!.values.length}');
    return _box!.values.isNotEmpty ? _box!.values.toList() as List<T> : sample();
  }


  @override
  Future createList(List<T> entities) {
    // TODO: implement createList
    throw UnimplementedError();
  }


  @override
  Future updateList(List<T> entities) async {
    if (_box == null) _box = await getBox();
    final entries = Map<String, T?>.fromIterable(entities,
        key: (v) => v.id.toString(), value: (v) => v);
    await _box!.putAll(entries);
  }


  @override
  Future deleteList(List<T> entities) {
    // TODO: implement createList
    throw UnimplementedError();
  }


  @override
  Future<T?> read(int id) async {
    if (_box == null) _box = await getBox();
    return _box!.get(id);
  }


  @override
  Future create(T entity) => update(entity);


  @override
  Future update(T entity) async {
    if (_box == null) _box = await getBox();
    await _box!.put(entity.id.toString(), entity);
  }


  @override
  Future delete(T entity) async {
    if (_box == null) _box = await getBox();
    await _box!.delete(entity.id.toString());
  }


  List<T> sample() {
    return [];
  }

}
