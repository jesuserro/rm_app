import 'package:hive/hive.dart';
import 'package:repository_hive/repository_hive.dart';

class Settings {
  static const ACCESS_TOKEN = 'access_token';
  static const REFRESH_TOKEN = 'refresh_token';
  static const CURRENT_ACCOUNT = 'current_account';
  static const USER_EMAIL = 'user_email';
  static const INCLUDE_NUMBERS = 'include_numbers';
  static const REPLICATION_SERVER = 'replication_server';

  static final Settings _instance = Settings._internal();

  factory Settings() {
    return _instance;
  }

  Settings._internal();

  final String boxName = 'settings';

  Box<String>? _box;


  Future<Box<String>> getBox() async {
    await RepositoryHiveService().isInitialized.firstWhere((bool isInitialized) => isInitialized);
    print('Getting box $boxName');
    return Hive.openBox(boxName);
  }

  /// Returns the value associated with the given [key]. If the key does not
  /// exist, `null` is returned.
  ///
  /// If [defaultValue] is specified, it is returned in case the key does not
  /// exist.
  Future<String?> load(String key, {String? defaultValue}) async {
    if (_box == null) _box = await getBox();
    print('Settings from box $boxName: ${_box!.values.length}');
    return _box!.get(key, defaultValue: defaultValue);
  }

  /// Saves the [key] - [value] pair.
  Future save(String key, String value) async {
    if (_box == null) _box = await getBox();
    await _box!.put(key, value);
  }


  Future<int?> loadInt(String key) async {
    final value = await load(key);
    return value != null ? int.parse(value) : null;
  }


  Future saveInt(String key, int value) async {
    return save(key, value.toString());
  }


  Future<bool?> loadBool(String key) async {
    final value = await load(key);
    return value != null ? value == 'true' : null;
  }


  Future saveBool(String key, bool value) async {
    return save(key, value.toString());
  }

  /// Deletes the [key] - [value] pair.
  Future delete(String key) async {
    if (_box == null) _box = await getBox();
    await _box!.delete(key);
  }

}
