
import 'package:repository_core/repository_core.dart';
import 'package:repository_hive/src/repositories/entities_repository.dart';

class AccountsRepositoryHive extends EntitiesRepositoryHive<AccountEntity> implements AccountsRepository {
  static final AccountsRepositoryHive _instance = AccountsRepositoryHive._internal('accounts');

  factory AccountsRepositoryHive() {
    return _instance;
  }

  AccountsRepositoryHive._internal(String boxName): super(boxName);

  @override
  List<AccountEntity> sample() {
    return [
      AccountEntity(
        'Bible Study',
        id: 1,
      ),
      AccountEntity(
        'God\'s Unfailing Love',
        id: 2,
      ),
      AccountEntity(
        'Human Failure',
        id: 3,
      ),
    ];

  }

}