
import 'package:repository_core/repository_core.dart';
import 'package:repository_hive/src/repositories/entities_repository.dart';

class TagsRepositoryHive extends EntitiesRepositoryHive<TagEntity> implements TagsRepository {
  static final TagsRepositoryHive _instance = TagsRepositoryHive._internal('tags');

  factory TagsRepositoryHive() {
    return _instance;
  }

  TagsRepositoryHive._internal(String boxName): super(boxName);

  @override
  List<TagEntity> sample() {
    return [
      TagEntity(
        'Bible Study',
        id: 1,
      ),
      TagEntity(
        'God\'s Unfailing Love',
        id: 2,
      ),
      TagEntity(
        'Human Failure',
        id: 3,
      ),
    ];

  }

}