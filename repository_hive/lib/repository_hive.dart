library repository_hive;

import 'dart:async';

import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:repository_hive/src/model_adapters/account_adapter.dart';
import 'package:repository_hive/src/model_adapters/tag_adapter.dart';
import 'package:repository_hive/src/model_adapters/verse_adapter.dart';
import 'package:rxdart/rxdart.dart';

import 'src/model_adapters/local_date_adapter.dart';
export 'src/repositories/verses_repository.dart';
export 'src/repositories/tags_repository.dart';
export 'src/repositories/settings_repository.dart';

/// Registers Hive adapters
/// Note: Since int keys are limited to 32 bit, convert them to String
///
class RepositoryHiveService {
  static final RepositoryHiveService _instance = RepositoryHiveService
      ._internal();

  factory RepositoryHiveService() {
    return _instance;
  }

  RepositoryHiveService._internal() {
    Hive.initFlutter().whenComplete(() {
        Hive.registerAdapter(LocalDateAdapter(0));
        Hive.registerAdapter(VerseAdapter(1));
        Hive.registerAdapter(TagAdapter(2));
        Hive.registerAdapter(AccountAdapter(3));
        _initController.sink.add(true);
    });
  }

  final _initController = BehaviorSubject<bool>.seeded(false);

  Stream<bool> get isInitialized => _initController.stream;
}
