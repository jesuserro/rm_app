import 'package:remem_me/blocs/ordered_verses/ordered_verses.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository_core/repository_core.dart';

import '../blocs.dart';

class OrderedVersesKnownBloc extends OrderedVersesBloc {

  OrderedVersesKnownBloc({required filteredBloc})
      : super(box: Box.KNOWN, filteredBloc: filteredBloc);


  @override
  mapVersesToBox(List<Verse>? verses, VerseOrder order) {
    return VerseService().mapVersesToBoxKnown(verses!, order);
  }

  @override
  VerseOrder get currentOrder {
    return currentAccount!.orderKnown;
  }

  @override
  Account currentAccountWithOrder(VerseOrder order) {
    return currentAccount!.copyWith(orderKnown: order);
  }

}
