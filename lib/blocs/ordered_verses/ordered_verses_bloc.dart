import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/ordered_verses/ordered_verses.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository_core/repository_core.dart';

import '../blocs.dart';

abstract class OrderedVersesBloc
    extends Bloc<OrderedVersesEvent, OrderedVersesState> {
  final Box box;
  final FilteredVersesBloc filteredBloc;
  late StreamSubscription versesSubscription;

  OrderedVersesBloc(
      {required Box box, required FilteredVersesBloc filteredBloc})
      : box = box,
        filteredBloc = filteredBloc,
        super(initialState(box, filteredBloc.state)) {
    versesSubscription = filteredBloc.stream.listen(_filteredBlocUpdated);
    _filteredBlocUpdated(filteredBloc.state);
  }

  static OrderedVersesState initialState(Box box, FilteredVersesState? state) {
    switch (state.runtimeType) {
      case FilterInProgress:
        return OrderInProgress();
      default:
        return InitialOrderedVersesState();
    }
  }

  void _filteredBlocUpdated(FilteredVersesState? state) {
    if (state is FilteredVersesLoadSuccess) {
      if (state.isUpdating) {
        add(FilteredVersesUpdated());
      } else {
        add(FilteredVersesLoaded(state.filteredVerses));
      }
    } else if (state is FilterInProgress) {
      add(OrderedVersesReloaded());
    } else if (state is FilterFailure) {
      add(FilterFailed());
    }
  }

  static OrderedVersesBloc from(BuildContext ctx, Box box) {
    switch (box) {
      case Box.NEW:
        return BlocProvider.of<OrderedVersesNewBloc>(ctx);
      case Box.DUE:
        return BlocProvider.of<OrderedVersesDueBloc>(ctx);
      case Box.KNOWN:
        return BlocProvider.of<OrderedVersesKnownBloc>(ctx);
      default:
        return BlocProvider.of<OrderedVersesAllBloc>(ctx);
    }
  }

  @override
  Stream<OrderedVersesState> mapEventToState(OrderedVersesEvent event) async* {
    if (event is OrderUpdated) {
      yield* mapOrderUpdatedToState(event);
    } else if (event is FilteredVersesLoaded) {
      yield* mapVersesLoadedToState(event);
    } else if (event is FilteredVersesUpdated) {
      yield* mapVersesUpdatedToState();
    } else if (event is OrderedVersesReloaded) {
      yield* _mapOrderedVersesReloadedToState();
    } else if (event is FilterFailed) {
      yield OrderFailure();
    }
  }

  Stream<OrderedVersesState> _mapOrderedVersesReloadedToState() async* {
    yield OrderInProgress();
  }

  Stream<OrderedVersesState> mapOrderUpdatedToState(
    OrderUpdated event,
  ) async* {
    if (filteredBloc.state is FilteredVersesLoadSuccess) {
      final verses =
          (filteredBloc.state as FilteredVersesLoadSuccess).filteredVerses;
      final AccountsRepository accountsRepository = filteredBloc.versesBloc.versesBloc.accountsBloc.repository;
      accountsRepository.update(currentAccountWithOrder(event.order));
      yield OrderSuccess(event.order, mapVersesToBox(verses, event.order));
    }
  }

  Stream<OrderedVersesState> mapVersesLoadedToState(
    FilteredVersesLoaded event,
  ) async* {
    final order = (this.state is OrderSuccess)
        ? (this.state as OrderSuccess).order
        : currentOrder;
    yield OrderSuccess(order, mapVersesToBox(event.verses, order));
  }

  Stream<OrderedVersesState> mapVersesUpdatedToState() async* {
    final OrderedVersesState _state = state;
    if (_state is OrderSuccess) {
      yield _state.copyWith(isUpdating: true);
    }
  }

  AccountsBloc get accountsBloc {
    return filteredBloc.versesBloc.versesBloc.accountsBloc;
  }

  Account? get currentAccount {
    final EntitiesState<AccountEntity>? _state = accountsBloc.state;
    return _state is AccountsLoadSuccess ? _state.currentAccount : null;
  }

  Account currentAccountWithOrder(VerseOrder order);

  VerseOrder get currentOrder;

  List<Verse> mapVersesToBox(List<Verse>? verses, VerseOrder order);

  @override
  Future<void> close() {
    versesSubscription.cancel();
    return super.close();
  }
}
