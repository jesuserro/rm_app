import 'package:remem_me/blocs/ordered_verses/ordered_verses.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository_core/repository_core.dart';

import '../blocs.dart';

class OrderedVersesDueBloc extends OrderedVersesBloc {

  OrderedVersesDueBloc({required FilteredVersesBloc filteredBloc})
      : super(box: Box.DUE, filteredBloc: filteredBloc);


  @override
  mapVersesToBox(List<Verse>? verses, VerseOrder order) {
    return VerseService().mapVersesToBoxDue(verses!, order);
  }

  @override
  VerseOrder get currentOrder {
    return currentAccount!.orderDue;
  }

  @override
  Account currentAccountWithOrder(VerseOrder order) {
    return currentAccount!.copyWith(orderDue: order);
  }
}
