import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository_core/repository_core.dart';


abstract class OrderedVersesEvent extends Equatable {
  const OrderedVersesEvent();

  @override
  List<Object?> get props => [];
}


class OrderedVersesReloaded extends OrderedVersesEvent {}
class FilteredVersesUpdated extends OrderedVersesEvent {}
class FilterFailed extends OrderedVersesEvent {}

class OrderUpdated extends OrderedVersesEvent {
  final VerseOrder order;

  const OrderUpdated(this.order);

  @override
  List<Object> get props => [order];

  @override
  String toString() => 'OrderUpdated { order: $order }';
}


class FilteredVersesLoaded extends OrderedVersesEvent {
  final List<Verse>? verses;

  const FilteredVersesLoaded(this.verses);

  @override
  List<Object?> get props => [verses];

  @override
  String toString() => 'VersesUpdated { verses: ${verses!.length} }';
}
