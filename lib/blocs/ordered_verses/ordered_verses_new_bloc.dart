import 'package:remem_me/blocs/ordered_verses/ordered_verses.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository_core/repository_core.dart';

import '../blocs.dart';

class OrderedVersesNewBloc extends OrderedVersesBloc {

  OrderedVersesNewBloc({required filteredBloc})
      : super(box: Box.NEW, filteredBloc: filteredBloc);


  @override
  mapVersesToBox(List<Verse>? verses, VerseOrder order) {
    return VerseService().mapVersesToBoxNew(verses!, order);
  }

  @override
  VerseOrder get currentOrder {
    return currentAccount!.orderNew;
  }

  @override
  Account currentAccountWithOrder(VerseOrder order) {
    return currentAccount!.copyWith(orderNew: order);
  }

}
