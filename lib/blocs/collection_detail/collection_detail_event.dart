import 'package:equatable/equatable.dart';
import 'package:remem_me/models/collections/collections.dart';

abstract class CollectionDetailEvent extends Equatable {
  const CollectionDetailEvent();

  @override
  List<Object?> get props => [];
}

class CollectionDetailLoaded extends CollectionDetailEvent {
  final int id;

  const CollectionDetailLoaded(this.id);

  @override
  List<Object?> get props => [id];
}
