import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:remem_me/models/collections/collections.dart';
import 'package:remem_me/services/collection_service.dart';

import 'collection_detail.dart';

class CollectionDetailBloc
    extends Bloc<CollectionDetailEvent, CollectionDetailState> {
  final CollectionService service;

  CollectionDetailBloc(this.service)
      : super(InitialCollectionDetailState());

  @override
  Stream<CollectionDetailState> mapEventToState(
      CollectionDetailEvent event) async* {
    if (event is CollectionDetailLoaded) {
      yield* mapCollectionDetailLoadedToState(event);
    }
  }

  Stream<CollectionDetailState> mapCollectionDetailLoadedToState(
      CollectionDetailLoaded event) async* {
    final collection = await service.fetchCollection(event.id);
    yield CollectionDetailLoadSuccess(collection);
  }
}
