import 'package:equatable/equatable.dart';
import 'package:remem_me/models/collections/collections.dart';
import 'package:remem_me/models/verse.dart';

abstract class CollectionDetailState extends Equatable {

  const CollectionDetailState();

  @override
  List<Object> get props => [];
}

class InitialCollectionDetailState extends CollectionDetailState {}


class CollectionDetailLoadSuccess extends CollectionDetailState {
  final Collection collection;

  CollectionDetailLoadSuccess(this.collection);

  @override
  List<Object> get props => [collection];
}

// todo: general failure state mixin
class CollectionDetailLoadFailure extends CollectionDetailState {
  final String errorMessage;

  CollectionDetailLoadFailure(this.errorMessage);

  @override
  List<Object> get props => [errorMessage];

  @override
  String toString() {
    return 'CollectionDetailLoadFailure { errorMessage: $errorMessage }';
  }
}