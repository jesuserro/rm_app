part of 'collections_bloc.dart';

abstract class CollectionsEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class SearchDisplayed extends CollectionsEvent {
  final bool visible;

  SearchDisplayed(this.visible);

  @override
  List<Object> get props => [visible];
}

class CollectionsInitiated extends CollectionsEvent {
  final Query query;
  final bool? isSearchVisible;

  CollectionsInitiated(this.query, {this.isSearchVisible});

  @override
  List<Object?> get props => [query, isSearchVisible];
}
class CollectionsFetched extends CollectionsEvent {}

