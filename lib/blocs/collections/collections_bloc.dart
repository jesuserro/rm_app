import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:remem_me/models/collections/collection.dart';
import 'package:remem_me/models/collections/collections.dart';
import 'package:remem_me/models/collections/fetch_status.dart';
import 'package:remem_me/models/collections/query.dart';
import 'package:remem_me/services/collection_service.dart';
import 'package:rxdart/rxdart.dart';

part 'collections_event.dart';

part 'collections_state.dart';

class CollectionsBloc extends Bloc<CollectionsEvent, CollectionsState> {
  final CollectionService service;

  CollectionsBloc(this.service) : super(const CollectionsState());

  @override
  Stream<Transition<CollectionsEvent, CollectionsState>> transformEvents(
    Stream<CollectionsEvent> events,
    TransitionFunction<CollectionsEvent, CollectionsState> transitionFn,
  ) {
    return super.transformEvents(
      events.debounceTime(const Duration(milliseconds: 500)),
      transitionFn,
    );
  }

  @override
  Stream<CollectionsState> mapEventToState(CollectionsEvent event) async* {
    if (event is SearchDisplayed) {
      yield _mapSearchDisplayedToState(event);
    } else if (event is CollectionsInitiated) {
      yield* _mapCollectionsInitiatedToState(event);
    } else if (event is CollectionsFetched) {
      yield await _mapCollectionsFetchedToState(state);
    }
  }

  CollectionsState _mapSearchDisplayedToState(SearchDisplayed event) {
    return state.copyWith(isSearchVisible: event.visible);
  }

  Stream<CollectionsState> _mapCollectionsInitiatedToState(
      CollectionsInitiated event) async* {
    yield CollectionsState(isSearchVisible: event.isSearchVisible ?? false);
    try {
      final collections = await service.fetchCollections(event.query);
      yield state.copyWith(
        query: event.query,
        fetchStatus: FetchStatus.SUCCESS,
        collections: collections,
        hasReachedMax: service.hasReachedMax(collections.length),
        isSearchVisible: event.isSearchVisible ?? state.isSearchVisible,
      );
    } on Exception {
      yield state.copyWith(
        fetchStatus: FetchStatus.FAILURE,
        isSearchVisible: event.isSearchVisible ?? state.isSearchVisible,
      );
    }
  }

  Future<CollectionsState> _mapCollectionsFetchedToState(
      CollectionsState state) async {
    print('_mapCollectionsFetchedToState: $state');
    if (state.hasReachedMax) return state;
    try {
      final collections = await service.fetchCollections(state.query,
          offset: state.collections.length);
      return collections.isEmpty
          ? state.copyWith(hasReachedMax: true)
          : state.copyWith(
              fetchStatus: FetchStatus.SUCCESS,
              collections: List.of(state.collections)..addAll(collections),
              hasReachedMax: service.hasReachedMax(collections.length),
            );
    } on Exception {
      return state.copyWith(fetchStatus: FetchStatus.FAILURE);
    }
  }
}
