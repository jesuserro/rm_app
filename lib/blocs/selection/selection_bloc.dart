import 'dart:async';

import 'package:bloc/bloc.dart';

import '../blocs.dart';

class SelectionBloc extends Bloc<SelectionEvent, SelectionState> {
  final TabBloc tabBloc;
  final VersesBloc versesBloc;
  late StreamSubscription tabSubscription;
  late StreamSubscription versesSubscription;

  SelectionBloc({
    required this.tabBloc,
    required this.versesBloc,
  }) : super(SelectionState({})) {
    tabSubscription = tabBloc.stream.listen((state) {
      add(SelectionCleared());
    });
    versesSubscription = versesBloc.stream.listen((state) {
      add(SelectionCleared());
    });

  }

  @override
  Stream<SelectionState> mapEventToState(SelectionEvent event) async* {
    if (event is SelectionUpdated) {
      yield SelectionState(event.selection);
    } else if (event is SelectionCleared) {
      yield SelectionState({});
    } else if (event is SearchActivated) {
      yield state.copyWith(isSearchActive: true);
    } else if (event is SearchDeactivated) {
      yield state.copyWith(isSearchActive: false);
    }
  }

  @override
  Future<void> close() {
    tabSubscription.cancel();
    versesSubscription.cancel();
    return super.close();
  }
}
