import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/models/study/study.dart';
import 'package:remem_me/services/text_service.dart';

abstract class StudyState extends Equatable {
  @override
  List<Object?> get props => [];
}

class NoStudy extends StudyState {}

class Study extends StudyState {
  final Verse? verse;
  final List<List<String>>? lines;
  final Pos pos;
  final bool isComplete;

  Study(this.verse,
      {required List<List<String>>? lines, Pos? pos, bool? isComplete})
      : this.lines = lines,
        this.isComplete = isComplete ?? false,
        this.pos = pos ?? TextService().lastPos(lines!);

  @override
  List<Object?> get props => [this.lines, this.pos, this.isComplete];

  @override
  String toString() =>
      'StudyState { verse: $verse, lines: [${lines!.length}], isComplete: $isComplete }';
}

class Obfuscation extends Study {
  final List<List<bool>>? obfuscated;
  final List<List<bool>>? revealed;

  Obfuscation(Verse? verse, List<List<String>>? lines,
      {this.obfuscated, this.revealed, bool? isComplete})
      : super(verse, lines: lines, isComplete: isComplete);

  @override
  List<Object?> get props => super.props + [this.obfuscated, this.revealed];

  Obfuscation copyWith({
    bool? isComplete,
    List<List<bool>>? obfuscated,
    List<List<bool>>? revealed,
  }) {
    return Obfuscation(
      this.verse,
      this.lines,
      isComplete: isComplete ?? this.isComplete,
      obfuscated: obfuscated ?? this.obfuscated,
      revealed: revealed ?? this.revealed,
    );
  }

  @override
  String toString() => 'Obfuscation { lines: [${lines!.length}], pos: $pos, '
      'obfuscated: $obfuscated, revealed: $revealed, isComplete: $isComplete }';
}

class Puzzle extends Study {
  final Choice? choice;

  Puzzle(
    Verse? verse, {
    List<List<String>>? words,
    Pos? pos,
    bool? isComplete,
    this.choice,
  }) : super(verse, lines: words, pos: pos, isComplete: isComplete);

  @override
  List<Object?> get props => super.props + [this.choice];

  Puzzle copyWith({
    Pos? pos,
    bool? isComplete,
    Choice? choice,
  }) {
    return Puzzle(
      this.verse,
      words: this.lines,
      isComplete: isComplete ?? this.isComplete,
      pos: pos ?? this.pos,
      choice: choice ?? this.choice,
    );
  }

  @override
  String toString() =>
      'Puzzle { lines: [${lines!.length}], pos: $pos, choice: $choice, '
      'isComplete: $isComplete }';
}

class LineUp extends Study {
  final Pos? revealed;
  final bool hasPlaceholders;
  final bool hasInitials;

  LineUp(
    Verse? verse, {
    List<List<String>>? lines,
    Pos? pos,
    bool? isComplete,
    this.revealed,
    this.hasPlaceholders = true,
    this.hasInitials = true,
  }) : super(verse, lines: lines, pos: pos, isComplete: isComplete);

  @override
  List<Object?> get props =>
      super.props + [this.revealed, this.hasPlaceholders, this.hasInitials];

  LineUp copyWith({
    Pos? pos,
    bool? isComplete,
    Pos? revealed,
    bool? hasPlaceholders,
    bool? hasInitials,
  }) {
    return LineUp(
      this.verse,
      lines: this.lines,
      isComplete: isComplete ?? this.isComplete,
      pos: pos ?? this.pos,
      revealed: revealed ?? this.revealed,
      hasPlaceholders: hasPlaceholders ?? this.hasPlaceholders,
      hasInitials: hasInitials ?? this.hasInitials,
    );
  }

  @override
  String toString() => 'LineUp { lines: [${lines!.length}], pos: $pos, '
      'isComplete: $isComplete, revealed: $revealed, '
      'hasPlaceholders: $hasPlaceholders, hasInitials: $hasInitials }';
}

class Typing extends LineUp {
  final String? letters;
  final String? nextWord;
  final bool? isCorrect;

  Typing(
    Verse? verse, {
    List<List<String>>? lines,
    Pos? pos,
    bool? isComplete,
    Pos? revealed,
    bool? hasPlaceholders,
    bool? hasInitials,
    this.letters,
    this.nextWord,
    this.isCorrect,
  }) : super(verse,
            lines: lines,
            pos: pos,
            isComplete: isComplete,
            revealed: revealed,
            hasPlaceholders: hasPlaceholders ?? true,
            hasInitials: hasInitials ?? false);

  @override
  List<Object?> get props =>
      super.props + [this.letters, this.nextWord, this.isCorrect];

  Typing copyWith({
    Pos? pos,
    bool? isComplete,
    Pos? revealed,
    bool? hasPlaceholders,
    bool? hasInitials,
    String? letters,
    String? nextWord,
    bool? isCorrect,
    List<String> nullValues = const [],
  }) {
    return Typing(
      this.verse,
      lines: this.lines,
      isComplete: isComplete ?? this.isComplete,
      pos: pos ?? this.pos,
      letters:
          letters ?? (nullValues.contains('letters') ? null : this.letters),
      nextWord: nextWord ?? this.nextWord,
      isCorrect: isCorrect ??
          (nullValues.contains('isCorrect') ? null : this.isCorrect),
      revealed: revealed ?? this.revealed,
      hasPlaceholders: hasPlaceholders ?? this.hasPlaceholders,
      hasInitials: hasInitials ?? this.hasInitials,
    );
  }

  @override
  String toString() => 'Typing { lines: [${lines!.length}], pos: $pos, '
      'isComplete: $isComplete, revealed: $revealed, '
      'hasPlaceholders: $hasPlaceholders, hasInitials: $hasInitials, '
      'letters: $letters, nextWord: $nextWord, isCorrect: $isCorrect }';
}
