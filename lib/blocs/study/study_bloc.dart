import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/models/study/out_of_words_exception.dart';
import 'package:remem_me/models/study/study.dart';
import 'package:remem_me/services/text_service.dart';

import '../../services/study_service.dart';
import 'study.dart';

class StudyBloc extends Bloc<StudyEvent, StudyState> {
  final service = StudyService();

  StudyBloc() : super(NoStudy());

  @override
  Stream<StudyState> mapEventToState(StudyEvent event) async* {
    if (event is StudyStarted) {
      yield* mapStudyStartedToState(event);
    } else if (event is Obfuscated) {
      yield* mapObfuscatedToState(event);
    } else if (event is Revealed) {
      yield* mapRevealedToState(event);
    } else if (event is Puzzled) {
      yield* mapPuzzledToState(event);
    } else if (event is LinedUp) {
      yield* mapLinedUpToState(event);
    } else if (event is Typed) {
      yield* mapTypedToState(event);
    } else if (event is Retracted) {
      yield* mapRetractedToState(event);
    } else if (event is Hinted) {
      yield* mapHintedToState(event);
    }
  }

  Stream<StudyState> mapStudyStartedToState(StudyStarted event) async* {
    if (event.verse != null) {
      final lines = TextService().textToWords(event.verse!.passage!);
      yield Study(event.verse, lines: lines, pos: TextService().lastPos(lines))
         ;
    } else {
      final _state = state as Study;
      yield Study(_state.verse, lines: _state.lines);
    }
  }

  Stream<StudyState> mapObfuscatedToState(Obfuscated event) async* {
    final _state = state as Study;
    if (_state is Obfuscation) {
      final obfuscated = service.obfuscate(_state.lines!, _state.obfuscated!);
      yield _state.copyWith(
          obfuscated: service.obfuscate(_state.lines!, _state.obfuscated!),
          revealed: TextService().wordsToMarks(_state.lines!),
          isComplete: TextService().countWordsOfRows(_state.lines!) ==
              TextService().countMarks(obfuscated));
    } else {
      yield Obfuscation(_state.verse, _state.lines,
          obfuscated: service.obfuscate(
              _state.lines!, TextService().wordsToMarks(_state.lines!)),
          revealed: TextService().wordsToMarks(_state.lines!));
    }
  }

  Stream<StudyState> mapRevealedToState(Revealed event) async* {
    final _state = state as Study;
    if (_state is Obfuscation) {
      final revealed = service.reveal(_state.revealed!, event.pos);
      yield _state.copyWith(revealed: revealed);
    }
  }

  Stream<StudyState> mapPuzzledToState(Puzzled event) async* {
    final _state = state as Study;
    if (_state is Puzzle) {
      if (event.isCorrect!) {
        try {
          final pos = service.advanceByWord(_state.lines!, _state.pos);
          final nextPos = service.advanceByWord(_state.lines!, pos);
          final choice = service.puzzle(_state.lines!, nextPos);
          yield _state.copyWith(pos: pos, choice: choice);
        } on OutOfWordsException {
          yield Study(_state.verse, lines: _state.lines);
        }
      }
    } else {
      yield Puzzle(_state.verse,
          words: _state.lines,
          pos: Pos(0, -1),
          choice: service.puzzle(_state.lines!, Pos(0, 0)));
    }
  }

  Stream<StudyState> mapLinedUpToState(LinedUp event) async* {
    final _state = state as Study;
    if (_state is LineUp) {
      try {
        final revealed = event.byLine!
            ? service.advanceByLine(_state.lines!, _state.revealed!)
            : service.advanceByWord(_state.lines!, _state.revealed!);
        var pos = service.lookAhead(_state.lines!, revealed);
        yield _state.copyWith(revealed: revealed, pos: pos);
      } on OutOfWordsException {
        yield _state.copyWith(revealed: _state.pos, isComplete: true)
           ;
      }
    } else {
      yield LineUp(
        _state.verse,
        lines: _state.lines,
        revealed: Pos(0, -1),
        pos: Pos(0, _state.lines![0].length - 1),
      );
    }
  }

  Stream<StudyState> mapRetractedToState(Retracted event) async* {
    final _state = state as Study;
    if (_state is LineUp) {
      final revealed = service.retractLine(_state.lines, _state.revealed!);
      if (_state is Typing) {
        final nextPos = service.advanceByWord(_state.lines!, revealed);
        yield _state.copyWith(
          pos: nextPos,
          revealed: revealed,
          nextWord: _state.lines![nextPos.line][nextPos.word],
          hasInitials: false,
          isComplete: false,
          nullValues: ['isCorrect', 'letters'],
        );
      } else {
        var pos = service.lookAhead(_state.lines!, revealed);
        yield _state.copyWith(revealed: revealed, pos: pos, isComplete: false);
      }
    }
  }

  Stream<StudyState> mapHintedToState(Hinted event) async* {
    final _state = state as Study;
    if (_state is LineUp) {
      yield _state.copyWith(
          hasPlaceholders: event.hasPlaceholders,
          hasInitials: event.hasInitials);
    }
  }

  Stream<StudyState> mapTypedToState(Typed event) async* {
    final _state = state as Study;
    if (_state is Typing) {
      if (TextService().startsWith(_state.nextWord, event.letters!)) {
        try {
          final nextPos = service.advanceByWord(_state.lines!, _state.pos);
          yield _state.copyWith(
            isCorrect: true,
            pos: nextPos,
            revealed: _state.pos,
            letters: event.letters,
            nextWord: _state.lines![nextPos.line][nextPos.word],
            hasInitials: false,
          );
        } on OutOfWordsException {
          yield _state.copyWith(revealed: _state.pos, isComplete: true);
        }
      } else {
        yield _state.copyWith(letters: event.letters, isCorrect: false);
      }
    } else {
      final pos = Pos(0, 0);
      final nextWord = _state.lines![pos.line][pos.word];
      yield Typing(
        _state.verse,
        lines: _state.lines,
        pos: Pos(0, 0),
        revealed: Pos(0, -1),
        nextWord: nextWord,
        hasInitials: false,
      );
    }
  }
}
