import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:remem_me/blocs/filtered_verses/filtered_verses.dart';
import 'package:remem_me/models/models.dart';

import '../blocs.dart';

// todo: move business logic to service
class FilteredVersesBloc
    extends Bloc<FilteredVersesEvent, FilteredVersesState> {
  final TaggedVersesBloc versesBloc;
  late StreamSubscription versesSubscription;

  FilteredVersesBloc({required this.versesBloc})
      : super(_initialState(versesBloc)) {
    versesSubscription = versesBloc.stream.listen(_versesBlocUpdated);
    _versesBlocUpdated(versesBloc.state);
  }

  static FilteredVersesState _initialState(TaggedVersesBloc versesBloc) {
    switch (versesBloc.state.runtimeType) {
      case TaggedVersesLoadSuccess:
        return FilteredVersesLoadSuccess(
          filteredVerses: (versesBloc.state as TaggedVersesLoadSuccess).verses,
          activeFilter: VisibilityFilter(),
        );
      case TaggedVersesLoadInProgress:
        return FilterInProgress();
      case InitialTaggedVersesState:
      default:
        return InitialFilterVersesState();
    }
  }

  void _versesBlocUpdated(TaggedVersesState? state) {
    if (state is TaggedVersesLoadSuccess) {
      if (state.isUpdating) {
        add(TaggedVersesUpdated());
      } else {
        add(TaggedVersesLoaded(state.verses));
      }
    } else if (state is TaggedVersesLoadInProgress) {
      add(FilteredVersesReloaded());
    } else if (state is TaggedVersesFailure) {
      add(TaggingFailed());
    }
  }

  @override
  Stream<FilteredVersesState> mapEventToState(
      FilteredVersesEvent event) async* {
    if (event is SearchFilterUpdated) {
      yield* _mapSearchFilterUpdatedToState(event);
    } else if (event is TagFilterUpdated) {
      yield* _mapTagFilterUpdatedToState(event);
    } else if (event is TaggedVersesLoaded) {
      yield* _mapVersesLoadedToState(event);
    } else if (event is TaggedVersesUpdated) {
      yield* _mapVersesUpdatedToState();
    } else if (event is FilteredVersesReloaded) {
      yield* _mapFilteredVersesReloadedToState();
    } else if (event is TaggingFailed) {
      yield FilterFailure();
    }
  }

  Stream<FilteredVersesState> _mapFilteredVersesReloadedToState() async* {
    yield FilterInProgress();
  }

  Stream<FilteredVersesState> _mapSearchFilterUpdatedToState(
    SearchFilterUpdated event,
  ) async* {
    if (state is FilteredVersesLoadSuccess) {
      final visibilityFilter = (state as FilteredVersesLoadSuccess)
          .activeFilter!
          .copyWith(query: event.query);
      yield _filteredVersesLoadSuccess(visibilityFilter);
    }
  }

  Stream<FilteredVersesState> _mapTagFilterUpdatedToState(
    TagFilterUpdated event,
  ) async* {
    if (state is FilteredVersesLoadSuccess) {
      final visibilityFilter = (state as FilteredVersesLoadSuccess)
          .activeFilter!
          .copyWith(tags: event.tags);
      yield _filteredVersesLoadSuccess(visibilityFilter);
    }
  }

  Stream<FilteredVersesState> _mapVersesLoadedToState(
    TaggedVersesLoaded event,
  ) async* {
    final visibilityFilter = state is FilteredVersesLoadSuccess
        ? (state as FilteredVersesLoadSuccess).activeFilter
        : VisibilityFilter();
    if (versesBloc.state is TaggedVersesLoadSuccess) {
      yield _filteredVersesLoadSuccess(visibilityFilter);
    }
  }

  Stream<FilteredVersesState> _mapVersesUpdatedToState() async* {
    final FilteredVersesState? _state = state;
    if (_state is FilteredVersesLoadSuccess) {
      yield _state.copyWith(isUpdating: true);
    }
  }

  FilteredVersesLoadSuccess _filteredVersesLoadSuccess(
      VisibilityFilter? visibilityFilter) {
    var verses = _mapVersesToFilteredVerses(
      (versesBloc.state as TaggedVersesLoadSuccess).verses,
      visibilityFilter,
    );
    return FilteredVersesLoadSuccess(
      filteredVerses: verses,
      activeFilter: visibilityFilter,
    );
  }

  List<Verse> _mapVersesToFilteredVerses(
      List<Verse> verses, VisibilityFilter? filter) {
    return verses.where((verse) {
      return filter?.query == null ||
          filter!.query.isEmpty ||
          verse.reference!
              .toLowerCase()
              .startsWith(filter.query.toLowerCase()) ||
          verse.passage!.toLowerCase().contains(filter.query.toLowerCase());
    }).toList();
  }

  @override
  Future<void> close() {
    versesSubscription.cancel();
    return super.close();
  }
}
