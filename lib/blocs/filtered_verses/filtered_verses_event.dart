import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';


abstract class FilteredVersesEvent extends Equatable {
  const FilteredVersesEvent();

  @override
  List<Object> get props => [];
}


class FilteredVersesReloaded extends FilteredVersesEvent {}
class TaggingFailed extends FilteredVersesEvent {}
class TaggedVersesUpdated extends FilteredVersesEvent {}


class SearchFilterUpdated extends FilteredVersesEvent {
  final String query;

  const SearchFilterUpdated(this.query);

  @override
  List<Object> get props => [query];

  @override
  String toString() => 'SearchFilterUpdated { query: $query }';
}


class TagFilterUpdated extends FilteredVersesEvent {
  final List<int> tags;

  const TagFilterUpdated(this.tags);

  @override
  List<Object> get props => [tags];

  @override
  String toString() => 'TagFilterUpdated { tags: $tags }';
}


class TaggedVersesLoaded extends FilteredVersesEvent {
  final List<Verse> verses;

  const TaggedVersesLoaded(this.verses);

  @override
  List<Object> get props => [verses];

  @override
  String toString() => 'TaggedVersesLoaded { verses: ${verses.length} }';
}
