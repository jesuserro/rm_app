import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';

abstract class FilteredVersesState extends Equatable {
  const FilteredVersesState();

  @override
  List<Object?> get props => [];
}

class InitialFilterVersesState extends FilteredVersesState {}
class FilterInProgress extends FilteredVersesState {}
class FilterFailure extends FilteredVersesState {}

class FilteredVersesLoadSuccess extends FilteredVersesState {
  final List<Verse>? filteredVerses;
  final VisibilityFilter? activeFilter;
  final bool isUpdating;

  const FilteredVersesLoadSuccess({
    this.filteredVerses,
    this.activeFilter,
    this.isUpdating = false,
  });

  @override
  List<Object?> get props => [filteredVerses, activeFilter, isUpdating];

  FilteredVersesLoadSuccess copyWith({
    List<Verse>? filteredVerses,
    VisibilityFilter? activeFilter,
    bool? isUpdating,
  }) =>
      FilteredVersesLoadSuccess(
        filteredVerses: filteredVerses ?? this.filteredVerses,
        activeFilter: activeFilter ?? this.activeFilter,
        isUpdating: isUpdating ?? this.isUpdating,
      );

  @override
  String toString() {
    return 'FilteredVersesLoadSuccess { filteredVerses: '
        '${filteredVerses!.length}, activeFilter: $activeFilter, '
        'isUpdating: $isUpdating }';
  }
}
