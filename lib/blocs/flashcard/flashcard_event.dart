import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/models/verse_action.dart';


abstract class FlashcardEvent extends Equatable {
  const FlashcardEvent();

  @override
  List<Object> get props => [];
}

class FlipStarted extends FlashcardEvent {}
class FlipEnded extends FlashcardEvent {}
class FlipReset extends FlashcardEvent {}
class Hinted extends FlashcardEvent {}
class LineRevealed extends FlashcardEvent {
  final int? lines;

  const LineRevealed({this.lines});
}


class FlashcardVerseUpdated extends FlashcardEvent {
  final Verse verse;

  FlashcardVerseUpdated(this.verse);
}


class SingleFlashcardLoaded extends FlashcardEvent {
  final Verse verse;
  final Box box;
  final bool inverse;

  const SingleFlashcardLoaded({required this.verse, required this.box, required this.inverse});

  @override
  List<Object> get props => [verse, box, inverse];

  @override
  String toString() => 'SingleLoaded { verse: $verse, box: $box, inverse: $inverse }';
}


class MultiFlashcardLoaded extends FlashcardEvent {
  final List<Verse> verses;
  final Box box;
  final bool inverse;
  final int index;

  const MultiFlashcardLoaded({required this.verses, required this.box,  required this.inverse, this.index = 0});

  @override
  List<Object> get props => [verses, box, index];

  @override
  String toString() => 'SingleLoaded { verses: ${verses.length}, box: $box, inverse: $inverse, index: $index }';
}


class FlashcardAction extends FlashcardEvent {
  final VerseAction action;

  FlashcardAction(this.action);

  @override
  List<Object> get props => [action];
}

