import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/entities/entities.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository_core/repository_core.dart';
import 'package:time_machine/time_machine.dart';

import '../blocs.dart';

class FlashcardBloc extends Bloc<FlashcardEvent, FlashcardState> {
  final VersesBloc versesBloc;
  final ScoresBloc scoresBloc;
  final VersesRepository versesRepository;

  FlashcardBloc(FlashcardState initialState,
      {required this.versesBloc,
      required this.scoresBloc,
      required this.versesRepository})
      : super(initialState);

  @override
  Stream<FlashcardState> mapEventToState(FlashcardEvent event) async* {
    if (event is SingleFlashcardLoaded) {
      yield* _mapSingleLoadedToState(event);
    } else if (event is MultiFlashcardLoaded) {
      yield* _mapMultiLoadedToState(event);
    } else if (event is FlashcardAction) {
      yield* _mapActionToState(event);
    } else if (event is FlipStarted) {
      yield* _mapFlipStartedToState(event);
    } else if (event is FlipEnded) {
      yield* _mapFlipEndedToState(event);
    } else if (event is FlipReset) {
      yield* _mapFlipResetToState(event);
    } else if (event is FlashcardVerseUpdated) {
      yield* _mapFlashcardVerseUpdatedToState(event);
    } else if (event is LineRevealed) {
      yield* _mapLineRevealedToState(event);
    } else if (event is Hinted) {
      yield* _mapHintedToState();
    }
  }

  Stream<FlashcardState> _mapSingleLoadedToState(
      SingleFlashcardLoaded event) async* {
    yield SingleFlashcardRun(
        verse: event.verse,
        box: event.box,
        inverse: event.inverse,
        reverse: [Box.NEW, Box.DUE].contains(event.box) ? true : false);
  }

  Stream<FlashcardState> _mapMultiLoadedToState(
      MultiFlashcardLoaded event) async* {
    yield MultiFlashcardRun(
        verses: [...event.verses],
        box: event.box,
        inverse: event.inverse,
        reverse: false,
        flipping: false);
  }

  Stream<FlashcardState> _mapActionToState(FlashcardAction event) async* {
    if (!(state is FlashcardRun)) yield state;
    final run = state as FlashcardRun?;
    switch (event.action) {
      case VerseAction.COMMITTED:
        versesBloc.add(
          EntitiesUpdated<Verse>([VerseService().committed(run!.verse)]),
        );
        break;
      case VerseAction.FORGOTTEN:
        if (!run!.inverse) {
          versesBloc.add(
            EntitiesUpdated<Verse>([VerseService().forgotten(run.verse)]),
          );
        }
        break;
      case VerseAction.REMEMBERED:
        if (!run!.inverse) {
          versesBloc.add(
            EntitiesUpdated<Verse>([VerseService().remembered(run.verse)]),
          );
          final currentLanguage =
              (versesBloc.accountsBloc.state as AccountsLoadSuccess)
                  .currentAccount!
                  .language;
          scoresBloc.add(EntitiesAdded<ScoreEntity>([
            ScoreEntity(
              vkey: run.verse.id.toString(),
              date: LocalDate.today(),
              change: TextService().countWordsOfText(run.verse.passage),
              account: run.verse.account,
            )
          ]));
        }
        break;
      case VerseAction.MOVED_TO_DUE:
        versesBloc.add(
          EntitiesUpdated<Verse>([VerseService().movedToDue(run!.verse)]),
        );
        break;
      case VerseAction.MOVED_TO_NEW:
        versesBloc.add(
          EntitiesUpdated<Verse>([VerseService().movedToNew(run!.verse)]),
        );
        break;
    }
    if (state is SingleFlashcardRun) {
      yield FlashcardEnd();
    } else if (state is MultiFlashcardRun) {
      final run = state as MultiFlashcardRun;
      if (run.inverse && event.action == VerseAction.REMEMBERED) {
        run.verses.removeAt(run.index);
        if (run.verses.length == 0) {
          yield FlashcardEnd();
        } else {
          yield run.copyWith(
              index: run.index % run.verses.length,
              reverse: false,
              visible: false,
              revealLines: 0);
        }
      } else if (!run.inverse && run.index == run.verses.length - 1) {
        yield FlashcardEnd();
      } else {
        yield run.copyWith(
            index: (run.index + 1) % run.verses.length,
            reverse: false,
            visible: false,
            revealLines: 0);
      }
    }
  }

  Stream<FlashcardState> _mapFlipStartedToState(FlipStarted event) async* {
    if (state is FlashcardRun)
      yield (state as FlashcardRun).copyWith(flipping: true);
  }

  Stream<FlashcardState> _mapFlipEndedToState(FlipEnded event) async* {
    if (state is FlashcardRun) {
      final run = state as FlashcardRun;
      yield run.copyWith(
          reverse: !run.reverse, flipping: false, hinting: false);
    }
  }

  Stream<FlashcardState> _mapFlipResetToState(FlipReset event) async* {
    if (state is FlashcardRun) {
      final run = state as FlashcardRun;
      yield run.copyWith(reverse: false, visible: true);
    }
  }

  Stream<FlashcardState> _mapFlashcardVerseUpdatedToState(
      FlashcardVerseUpdated event) async* {
    if (state is SingleFlashcardRun) {
      final run = state as SingleFlashcardRun;
      yield run.copyWith(verse: event.verse);
    } else if (state is MultiFlashcardRun) {
      final run = state as MultiFlashcardRun;
      yield run.copyWith(
          verses: run.verses
              .map((verse) => verse.id == event.verse.id ? event.verse : verse)
              .toList());
    }
  }

  Stream<FlashcardState> _mapHintedToState() async* {
    if (state is FlashcardRun) {
      final run = state as FlashcardRun;
      yield run.copyWith(hinting: true);
    }
  }

  Stream<FlashcardState> _mapLineRevealedToState(LineRevealed event) async* {
    if (state is FlashcardRun) {
      final run = state as FlashcardRun;
      yield run.copyWith(revealLines: event.lines ?? (run.revealLines + 1));
    }
  }
}
