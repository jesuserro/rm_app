import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';

abstract class FlashcardState extends Equatable {
  const FlashcardState();

  @override
  List<Object> get props => [];
}

class FlashcardLoadInProgress extends FlashcardState {}

class FlashcardEnd extends FlashcardState {}

abstract class FlashcardRun extends FlashcardState {
  final Box box;
  final bool inverse; // true -> passage on front, reference on back
  final bool flipping; // true -> flip animation is running
  final bool reverse; // true -> back is visible
  final bool visible; // false -> hide card (e.g. during app navigation)
  final bool hinting; // true -> hint (e.g. topic) is visible
  final int revealLines; // 2 -> display 2 lines of the passage

  Verse get verse;

  const FlashcardRun({
    required this.box,
    required this.inverse,
    required this.reverse,
    required this.flipping,
    required this.visible,
    required this.hinting,
    required this.revealLines,
  });

  FlashcardRun copyWith({
    bool? reverse,
    bool? flipping,
    bool? visible,
    bool? hinting,
    int? revealLines,
  });
}

class SingleFlashcardRun extends FlashcardRun {
  final Verse verse;

  const SingleFlashcardRun(
      {required this.verse,
      required box,
      required inverse,
      reverse = false,
      flipping = false,
      visible = true,
      hinting = false,
      revealLines = 0})
      : super(
            box: box,
            inverse: inverse,
            reverse: reverse,
            flipping: flipping,
            visible: visible,
            hinting: hinting,
            revealLines: revealLines);

  @override
  List<Object> get props =>
      [verse, box, inverse, reverse, flipping, visible, hinting, revealLines];

  @override
  SingleFlashcardRun copyWith({
    bool? reverse,
    bool? flipping,
    bool? visible,
    bool? hinting,
    int? revealLines,
    Verse? verse,
  }) {
    return SingleFlashcardRun(
        verse: verse ?? this.verse,
        box: this.box,
        inverse: this.inverse,
        reverse: reverse ?? this.reverse,
        flipping: flipping ?? this.flipping,
        visible: visible ?? this.visible,
        hinting: hinting ?? this.hinting,
        revealLines: revealLines ?? this.revealLines);
  }

  @override
  String toString() {
    return 'SingleFlashcardRun { verse: $verse, box: $box, inverse: $inverse, '
        'reverse: $reverse, flipping: $flipping, visible: $visible, '
        'hinting: $hinting, revealLines: $revealLines }';
  }
}

class MultiFlashcardRun extends FlashcardRun {
  final List<Verse> verses;
  final int index;

  const MultiFlashcardRun({
    required this.verses,
    required box,
    required inverse,
    reverse = false,
    flipping = false,
    visible = true,
    hinting = false,
    revealLines = 0,
    this.index = 0,
  }) : super(
            box: box,
            inverse: inverse,
            reverse: reverse,
            flipping: flipping,
            visible: visible,
            hinting: hinting,
            revealLines: revealLines);

  @override
  Verse get verse => verses[index];

  @override
  List<Object> get props => [
        verses,
        box,
        index,
        inverse,
        reverse,
        flipping,
        visible,
        hinting,
        revealLines
      ];

  @override
  MultiFlashcardRun copyWith({
    bool? reverse,
    bool? flipping,
    bool? visible,
    bool? hinting,
    int? revealLines,
    int? index,
    List<Verse>? verses,
  }) {
    return MultiFlashcardRun(
      verses: verses ?? this.verses,
      box: this.box,
      inverse: this.inverse,
      reverse: reverse ?? this.reverse,
      flipping: flipping ?? this.flipping,
      visible: visible ?? this.visible,
      hinting: hinting ?? this.hinting,
      revealLines: revealLines ?? this.revealLines,
      index: index ?? this.index,
    );
  }

  @override
  String toString() {
    return 'MultiFlashcardRun { verses: ${verses.length}, box: $box, '
        'inverse: $inverse, reverse: $reverse, flipping: $flipping, '
        'visible: $visible, hinting: $hinting, revealLines: $revealLines, '
        'index: $index }';
  }
}
