import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';


abstract class TtsEvent extends Equatable {
  const TtsEvent();

  @override
  List<Object?> get props => [];
}

class TtsStarted extends TtsEvent {
  final List<Verse>? verses;
  final int? index;


  const TtsStarted ({this.verses, this.index,});

  @override
  List<Object?> get props => [verses, index,];

  @override
  String toString() => 'TtsStarted { verses: ${verses!.length}, index: $index }';
}


class TtsStopped extends TtsEvent {}


class TtsFailed extends TtsEvent {
  final String message;

  TtsFailed(this.message);

  @override
  List<Object> get props => [message];

  @override
  String toString() => 'TtsFailed { message: $message }';
}


class TtsPaused extends TtsEvent {}

class TtsResumed extends TtsEvent {}

class TtsCompleted extends TtsEvent {}
