import 'package:equatable/equatable.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/models/models.dart';


abstract class TtsState extends Equatable {
  const TtsState();

  @override
  List<Object?> get props => [];
}

class TtsFailure extends TtsState {
  final String message;

  TtsFailure(this.message);

  @override
  List<Object> get props => [message];

  @override
  String toString() => 'TtsFailure { message: $message }';
}


class TtsOff extends TtsState {}


class TtsOn extends TtsState {
  final List<Verse>? verses;
  final int? index;
  final Progress status;

  const TtsOn ({this.verses, this.index, this.status = Progress.STOP,});

  @override
  List<Object?> get props => [verses, index, status];

  TtsOn copyWith({
    int? index,
    Progress? status,
  }) {
    return TtsOn(
        verses: this.verses,
        index: index ?? this.index,
        status: status ?? this.status,
    );
  }

  @override
  String toString() {
    return 'TtsOn { verses: ${verses!.length}, index: $index, status: $status }';
  }
}
