import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/text_to_speech/tts.dart';
import 'package:remem_me/services/tts_service.dart';

import '../blocs.dart';

class TtsBloc extends Bloc<TtsEvent, TtsState> {
  final AccountsBloc accountsBloc;
  late TtsService service;

  TtsBloc({required this.accountsBloc}) : super(TtsOff()) {
    service = TtsService(this);
  }

  @override
  Stream<TtsState> mapEventToState(TtsEvent event) async* {
    if (event is TtsStarted) {
      yield* _mapTtsStartedToState(event);
    } else if (event is TtsStopped) {
      yield* _mapTtsStoppedToState();
    } else if (event is TtsPaused) {
      yield* _mapTtsPausedToState();
    } else if (event is TtsResumed) {
      yield* _mapTtsResumedToState();
    } else if (event is TtsCompleted) {
      yield* _mapTtsCompletedToState();
    } else if (event is TtsFailed) {
      yield* _mapTtsFailedToState(event);
    }
  }

  Stream<TtsState> _mapTtsStartedToState(TtsStarted event) async* {
    yield TtsOn(
      verses: event.verses,
      index: event.index,
      status: Progress.RUN,
    );
    service.speak(event.verses![event.index!],
        (accountsBloc.state as AccountsLoadSuccess).currentAccount!);
  }

  Stream<TtsState> _mapTtsStoppedToState() async* {
    yield TtsOff();
    service.stop();
  }

  Stream<TtsState> _mapTtsPausedToState() async* {
    if (state is TtsOn) {
      yield (state as TtsOn).copyWith(status: Progress.PAUSE);
      service.stop();
    }
  }

  Stream<TtsState> _mapTtsResumedToState() async* {
    if (state is TtsOn) {
      final ttsOn = state as TtsOn;
      yield ttsOn.copyWith(status: Progress.RUN);
      service.speak(ttsOn.verses![ttsOn.index!],
          (accountsBloc.state as AccountsLoadSuccess).currentAccount!);
    }
  }

  Stream<TtsState> _mapTtsCompletedToState() async* {
    if (state is TtsOn) {
      final next = (state as TtsOn).index! + 1;
      final verses = (state as TtsOn).verses!;
      if (next < verses.length) {
        yield (state as TtsOn).copyWith(index: next);
        service.speak(verses[next],
            (accountsBloc.state as AccountsLoadSuccess).currentAccount!);
      } else {
        yield TtsOff();
      }
    }
  }

  Stream<TtsState> _mapTtsFailedToState(TtsFailed event) async* {
    yield TtsFailure(event.message);
  }
}
