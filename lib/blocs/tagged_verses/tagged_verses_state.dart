import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';

abstract class TaggedVersesState extends Equatable {
  const TaggedVersesState();

  @override
  List<Object> get props => [];
}

class InitialTaggedVersesState extends TaggedVersesState {}
class TaggedVersesLoadInProgress extends TaggedVersesState {}

class TaggedVersesFailure extends TaggedVersesState {}

class TaggedVersesLoadSuccess extends TaggedVersesState {
  final List<Verse> verses;
  final List<Tag> tags;
  final bool isUpdating;

  const TaggedVersesLoadSuccess({
    required this.verses,
    required this.tags,
    this.isUpdating = false,
  });

  @override
  List<Object> get props => [verses, tags, isUpdating];

  TaggedVersesLoadSuccess copyWith({bool? isUpdating}) {
    return TaggedVersesLoadSuccess(
        verses: this.verses,
        tags: this.tags,
        isUpdating: isUpdating ?? this.isUpdating);
  }

  @override
  String toString() {
    return 'TaggedVersesLoadSuccess { verses: ${verses.length}, tags: ${tags.length}, '
        'isUpdating: $isUpdating }';
  }
}
