import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:remem_me/blocs/entities/entities.dart';
import 'package:remem_me/blocs/tagged_verses/tagged_verses.dart';
import 'package:remem_me/blocs/verses/verses.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository_core/repository_core.dart';

import '../blocs.dart';

class TaggedVersesBloc extends Bloc<TaggedVersesEvent, TaggedVersesState> {
  final VersesBloc versesBloc;
  final TagsBloc tagsBloc;
  late StreamSubscription versesSubscription;
  late StreamSubscription tagsSubscription;

  TaggedVersesBloc({required this.versesBloc, required this.tagsBloc})
      : super(InitialTaggedVersesState()) {
    versesSubscription = versesBloc.stream.listen(_versesBlocUpdated);
    tagsSubscription = tagsBloc.stream.listen(_tagsBlocUpdated);
    _versesBlocUpdated(versesBloc.state);
    _tagsBlocUpdated(tagsBloc.state);
  }

  void _tagsBlocUpdated(EntitiesState<TagEntity>? state) {
    if (state is EntitiesLoadSuccess<TagEntity>) {
      if (state.isUpdating) {
        add(TagsUpdated());
      } else {
        add(TagsLoaded(state.entities));
      }
    }
  }

  void _versesBlocUpdated(EntitiesState<VerseEntity>? state) {
    if (state is EntitiesLoadSuccess<VerseEntity>) {
      if (state.isUpdating) {
        add(VersesUpdated());
      } else {
        add(VersesLoaded(state.entities));
      }
    } else if (state is EntitiesLoadInProgress<VerseEntity>) {
      add(TaggedVersesReloaded());
    } else if (state is EntitiesLoadFailure<VerseEntity>) {
      add(VersesFailed());
    }
  }

  @override
  Stream<TaggedVersesState> mapEventToState(TaggedVersesEvent event) async* {
    if (event is VersesUpdated) {
      yield* _mapUpdatedToState();
    } else if (event is TagsUpdated) {
      yield* _mapUpdatedToState();
    } else if (event is VersesLoaded) {
      yield* _mapVersesLoadedToState(event);
    } else if (event is TagsLoaded) {
      yield* _mapTagsLoadedToState(event);
    } else if (event is TaggedVersesReloaded) {
      yield* _mapTaggedVersesReloadedToState();
    } else if (event is VersesFailed) {
      yield TaggedVersesFailure();
    }
  }

  Stream<TaggedVersesState> _mapTaggedVersesReloadedToState() async* {
    yield TaggedVersesLoadInProgress();
  }

  Stream<TaggedVersesState> _mapUpdatedToState() async* {
    final TaggedVersesState? _state = state;
    if (_state is TaggedVersesLoadSuccess) {
      yield _state.copyWith(isUpdating: true);
    }
  }

  Stream<TaggedVersesState> _mapVersesLoadedToState(
    VersesLoaded event,
  ) async* {
    if (this.versesBloc.accountsBloc.state is AccountsLoadSuccess &&
        tagsBloc.state is EntitiesLoadSuccess<TagEntity>) {
      yield _taggedVersesLoadSuccess(
          (tagsBloc.state as EntitiesLoadSuccess<TagEntity>).entities,
          event.verses);
    }
  }

  Stream<TaggedVersesState> _mapTagsLoadedToState(
    TagsLoaded event,
  ) async* {
    if (this.versesBloc.accountsBloc.state is AccountsLoadSuccess &&
        versesBloc.state is EntitiesLoadSuccess<VerseEntity>) {
      yield _taggedVersesLoadSuccess(event.tags,
          (versesBloc.state as EntitiesLoadSuccess<VerseEntity>).entities);
    }
  }

  TaggedVersesLoadSuccess _taggedVersesLoadSuccess(
      List<TagEntity> tags, List<VerseEntity> entities) {
    final books = (this.versesBloc.accountsBloc.state as AccountsLoadSuccess)
        .currentAccount!
        .books;
    final verses = VerseService().fromEntities(entities, books);
    final nonEmptyTags =
        _mapVersesToTags(verses, tags).where((tag) => tag.size! > 0).toList();

    return TaggedVersesLoadSuccess(
      verses: _mapTagsToVerses(nonEmptyTags, verses),
      tags: nonEmptyTags,
    );
  }

  List<Verse> _mapTagsToVerses(List<TagEntity> tags, List<Verse> verses) {
    if (verses.isNotEmpty &&
        tags.isNotEmpty) {
      final Map<int?, TagEntity?> tagsMap =
          Map.fromIterable(tags, key: (tag) => tag.id, value: (tag) => tag);
      final isAnySelected =
          tagsMap.values.any((tag) => tag!.included == true); // ||
      return verses
          .where((verse) =>
              (!isAnySelected || _isIncluded(verse, tagsMap)) &&
              !_isExcluded(verse, tagsMap))
          .map((verse) => verse.copyWith(tags: _tagsOfVerse(verse, tagsMap)))
          .toList();
    } else {
      return verses;
    }
  }

  bool _isIncluded(Verse verse, Map<int?, TagEntity?> tagsMap) {
    return verse.tags.keys.any((id) => tagsMap[id]?.included == true);
  }

  bool _isExcluded(Verse verse, Map<int?, TagEntity?> tagsMap) {
    return verse.tags.keys.any((id) => tagsMap[id]?.included == false);
  }

  Map<int?, String?> _tagsOfVerse(Verse verse, Map<int?, TagEntity?> tagsMap) {
    final entries = verse.tags.entries
        .where((entry) => tagsMap[entry.key] != null)
        .map((entry) =>
            MapEntry<int?, String?>(entry.key, tagsMap[entry.key]!.text));
    return Map.fromEntries(entries);
  }

  List<Tag> _mapVersesToTags(List<Verse> verses, List<TagEntity> tags) {
    return tags.map((tag) {
      return Tag.fromEntity(tag,
          size:
              verses.where((verse) => verse.tags.keys.contains(tag.id)).length);
    }).toList();
  }

  @override
  Future<void> close() {
    versesSubscription.cancel();
    tagsSubscription.cancel();
    return super.close();
  }
}
