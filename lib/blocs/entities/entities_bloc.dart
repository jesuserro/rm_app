import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:collection/collection.dart' show IterableExtension;
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/entities/entities.dart';
import 'package:repository_core/repository_core.dart';

abstract class EntitiesBloc<T extends Entity, R extends EntitiesRepository>
    extends Bloc<EntitiesEvent<T>, EntitiesState<T>> {
  final R repository;

  EntitiesBloc({required this.repository, required EntitiesState? initialState})
      : super(initialState as EntitiesState<T>);

  @override
  Stream<EntitiesState<T>> mapEventToState(EntitiesEvent<T> event) async* {
    if (event is EntitiesLoaded<T>) {
      yield* mapEntitiesLoadedToState(event);
    } else if (event is EntitiesAdded<T>) {
      yield* mapEntitiesAddedToState(event);
    } else if (event is EntitiesUpdated<T>) {
      yield* mapEntitiesUpdatedToState(event);
    }
  }

  Stream<EntitiesState<T>> mapEntitiesLoadedToState(
      EntitiesLoaded<T> event) async* {
    yield EntitiesLoadInProgress();
    try {
      var entities = await repository.readList(account: event.account?.id);
      yield EntitiesLoadSuccess<T>(entities as List<T>);
    } on MessageException {
      yield EntitiesLoadFailure<T>('RemoteException');
    }
  }

  Stream<EntitiesState<T>> mapEntitiesAddedToState(
      EntitiesAdded<T> event) async* {
    final EntitiesState<T>? _state = state;
    if (_state is EntitiesLoadSuccess<T>) {
      try {
        if (event.persist) {
          yield _state.copyWith(isUpdating: true);
          await repository.createList(event.entities);
        }
        final List<T> updatedEntities =
            (state as EntitiesLoadSuccess<T>).entities + event.entities;
        yield EntitiesLoadSuccess<T>(updatedEntities);
      } on MessageException catch (e) {
        MessageService().warn(e.messages['messages']!.join('\n'));
        yield _state.copyWith(isUpdating: false);
      }
    }
  }

  Stream<EntitiesState<T>> mapEntitiesUpdatedToState(
      EntitiesUpdated<T> event) async* {
    final EntitiesState<T>? _state = state;
    if (_state is EntitiesLoadSuccess<T> && event.entities.isNotEmpty) {
      try {
        if (event.persist) {
          if (event.wait) {
            yield EntitiesLoadSuccess<T>(_state.entities, true);
            await repository.updateList(event.entities);
          } else {
            repository.updateList(event.entities);
          }
        }
        final List<T> allEntities = _state.entities
            .map((original) =>
                event.entities
                    .firstWhereOrNull((updated) => updated.id == original.id) ??
                original)
            .where((entity) => !entity.deleted)
            .toList();
        yield EntitiesLoadSuccess<T>(allEntities);
      } on MessageException catch (e) {
        MessageService().warn(e.messages['messages']!.join('\n'));
        yield _state.copyWith(isUpdating: false);
      }
    }
  }
}
