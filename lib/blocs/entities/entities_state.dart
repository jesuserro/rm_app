import 'package:equatable/equatable.dart';
import 'package:repository_core/repository_core.dart';

abstract class EntitiesState<T extends Entity> extends Equatable {
  const EntitiesState();

  @override
  List<Object?> get props => [];
}

class InitialEntitiesState<T extends Entity> extends EntitiesState<T> {}
class EntitiesLoadInProgress<T extends Entity> extends EntitiesState<T> {}

class EntitiesLoadSuccess<T extends Entity> extends EntitiesState<T> {
  final List<T> entities;
  final bool isUpdating;

  const EntitiesLoadSuccess([this.entities = const [], this.isUpdating = false]);

  @override
  List<Object?> get props => [entities, isUpdating];

  @override
  String toString() => 'EntitiesLoadSuccess { entities: ${entities.length}, isUpdating: $isUpdating }';

  EntitiesLoadSuccess<T> copyWith({bool? isUpdating}) {
    return EntitiesLoadSuccess(this.entities, isUpdating ?? this.isUpdating);
  }
}


// todo: mixin MessageClass

class EntitiesLoadFailure<T extends Entity> extends EntitiesState<T> {
  final String errorMessage;

  EntitiesLoadFailure(this.errorMessage);

  @override
  List<Object> get props => [errorMessage];

  @override
  String toString() {
    return 'EntitiesLoadFailure { errorMessage: $errorMessage }';
  }
}
