import 'dart:async';

import 'package:remem_me/blocs/entities/entities.dart';
import 'package:remem_me/blocs/verses/verses_event.dart';
import 'package:repository_core/repository_core.dart';

import '../blocs.dart';

class VersesBloc extends EntitiesBloc<VerseEntity, VersesRepository> {
  final AccountsBloc accountsBloc;
  late StreamSubscription accountsSubscription;

  VersesBloc({required versesRepository, required this.accountsBloc})
      : super(
            repository: versesRepository,
            initialState: InitialEntitiesState<VerseEntity>()) {
    accountsSubscription = accountsBloc.stream.listen(_accountsBlocUpdated);
    // listen() does not replay earlier events -> update with current state
    _accountsBlocUpdated(accountsBloc.state);
  }

  void _accountsBlocUpdated(EntitiesState<AccountEntity>? state) {
    if (state is AccountsLoadSuccess) {
      if(state.currentAccount != null) {
        add(EntitiesLoaded<VerseEntity>(account: state.currentAccount));
      }
    } else if (state is EntitiesLoadFailure<AccountEntity>) {
      add(AccountFailed());
    }
  }


  @override
  Stream<EntitiesState<VerseEntity>> mapEventToState(
      EntitiesEvent<VerseEntity> event) async* {
    yield* super.mapEventToState(event);
    if (event is TagDeleted) {
      yield* _mapTagDeletedToState(event);
    } else if(event is AccountFailed) {
      yield EntitiesLoadFailure<VerseEntity>('Account failed');
    }
  }


  Stream<EntitiesState<VerseEntity>> _mapTagDeletedToState(
      TagDeleted event,
      ) async* {
    if (state is EntitiesLoadSuccess<VerseEntity>) {
      final updatedVerses = (state as EntitiesLoadSuccess<VerseEntity>).entities
          .where((verse) => verse.tags.containsKey(event.tagId))
          .map((verse) {
        final tags = new Map.of(verse.tags);
        tags.remove(event.tagId);
        return verse.copyWith(tags: tags as Map<int, String>?);  // todo: batch update
      }).toList();
      yield* mapEntitiesUpdatedToState(EntitiesUpdated<VerseEntity>(updatedVerses));
    }
  }


  @override
  Future<void> close() {
    accountsSubscription.cancel();
    return super.close();
  }
}
