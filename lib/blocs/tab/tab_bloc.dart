import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:remem_me/blocs/tab/tab.dart';

import '../../models/box.dart';
class TabBloc extends Bloc<TabEvent, Box> {
  TabBloc() : super(Box.NEW);

  @override
  Stream<Box> mapEventToState(TabEvent event) async* {
    if (event is TabUpdated) {
      yield event.tab;
    }
  }
}
