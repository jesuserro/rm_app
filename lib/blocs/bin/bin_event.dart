import 'package:equatable/equatable.dart';
import 'package:remem_me/models/account.dart';
import 'package:repository_core/repository_core.dart';

abstract class BinEvent<T extends Entity> extends Equatable {
  const BinEvent();

  @override
  List<Object> get props => [];
}


class BinLoaded<T extends Entity> extends BinEvent<T> {
  final Account? account;

  BinLoaded({this.account});
}


class Shredded<T extends Entity> extends BinEvent<T> {}
class Restored<T extends Entity> extends BinEvent<T> {}
class AllWasteSelected<T extends Entity> extends BinEvent<T> {}
class NoWasteSelected<T extends Entity> extends BinEvent<T> {}


class WasteSelected<T extends Entity> extends BinEvent<T> {
  final T entity;

  const WasteSelected(this.entity);

  @override
  List<Object> get props => [entity];

  @override
  String toString() => 'WasteSelected { entity: $entity }';
}
