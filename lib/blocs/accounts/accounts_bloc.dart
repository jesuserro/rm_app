
import 'package:collection/collection.dart' show IterableExtension;
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/entities/entities.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/book_service.dart';
import 'package:repository_core/repository_core.dart';
import 'package:repository_hive/repository_hive.dart';
import 'package:repository_http/repository_http.dart';

import '../blocs.dart';

class AccountsBloc extends EntitiesBloc<AccountEntity, AccountsRepository> {
  AccountsBloc({required this.authBloc, required accountsRepository})
      : super(
            repository: accountsRepository,
            initialState: InitialEntitiesState<AccountEntity>()) {
    authBloc.stream.listen((state) {
      if (state is LoginSuccess) {
        add(EntitiesLoaded<AccountEntity>());
      }
    });
  }

  final AuthBloc authBloc;

  @override
  Stream<EntitiesState<AccountEntity>> mapEventToState(
      EntitiesEvent<AccountEntity> event) async* {
    yield* super.mapEventToState(event);
    if (event is AccountSelected) {
      yield* mapAccountSelectedToState(event);
    }
  }

  Stream<EntitiesState<AccountEntity>> mapEntitiesLoadedToState(
      EntitiesLoaded<AccountEntity> event) async* {
    yield EntitiesLoadInProgress();
    try {
      var entities = await repository.readList();
      if (entities.length > 0) {
        final currentId = await Settings().loadInt(Settings.CURRENT_ACCOUNT);
        AccountEntity? currentAccount;
        if (currentId != null) {
          currentAccount =
              entities.firstWhereOrNull((a) => a.id == currentId);
        }
        if (currentAccount == null) {
          currentAccount = entities[0];
          Settings().saveInt(Settings.CURRENT_ACCOUNT, currentAccount.id);
        }
        final account = await _withBooks(
            Account.fromEntity(currentAccount)); // todo: transformation
        yield AccountsLoadSuccess(account, entities);
      } else {
        yield AccountsLoadSuccess(null, <AccountEntity>[]);
      }
    } on MessageException {
      yield EntitiesLoadFailure<AccountEntity>('RemoteException');
    }
  }

  @override
  Stream<EntitiesState<AccountEntity>> mapEntitiesAddedToState(
      EntitiesAdded<AccountEntity> event) {
    // todo: update accounts list
    return super
        .mapEntitiesAddedToState(event)
        .asyncMap(_setCurrentAccount(event.entities));
  }

  Stream<EntitiesState<AccountEntity>> mapEntitiesUpdatedToState(
      EntitiesUpdated<AccountEntity> event) {
    return super
        .mapEntitiesUpdatedToState(event)
        .asyncMap(_setCurrentAccount(event.entities));
  }

  _setCurrentAccount(List<AccountEntity> accounts) =>
      (EntitiesState<AccountEntity> nextState) async {
        if (nextState is EntitiesLoadSuccess<AccountEntity>) {
          AccountEntity? account;
          if (accounts.length == 1 && !accounts[0].deleted) {
            account = accounts[0];
          } else if (accounts[0].deleted && nextState.entities.length > 0) {
            account = nextState.entities[0];
          }
          if (account != null) {
            account = await _withBooks(Account.fromEntity(account));
            return AccountsLoadSuccess(account as Account?, nextState.entities);
          }
        }
        return nextState;
      };

  Stream<EntitiesState<AccountEntity>> mapAccountSelectedToState(
      AccountSelected event) async* {
    if (state is AccountsLoadSuccess) {
      Settings().saveInt(Settings.CURRENT_ACCOUNT, event.account.id);
      final account = await _withBooks(
          Account.fromEntity(event.account)); // todo: transformation
      yield AccountsLoadSuccess(
          account, (state as EntitiesLoadSuccess<AccountEntity>).entities);
    }
  }

  Future<Account> _withBooks(Account account) async {
      final books = await BookService().loadBooks(account.referenceLanguage(), account.canon);
      return account.copyWith(books: books);
  }
}
