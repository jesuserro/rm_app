import 'package:remem_me/blocs/entities/entities.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository_core/repository_core.dart';

class AccountsLoadSuccess extends EntitiesLoadSuccess<AccountEntity> {
  final Account? currentAccount;

  const AccountsLoadSuccess(this.currentAccount, List<AccountEntity> accounts)
      : super(accounts);

  @override
  List<Object?> get props => [entities, currentAccount];

  @override
  String toString() =>
      'AccountsLoadSuccess { entities: $entities, currentAccount: $currentAccount }';
}
