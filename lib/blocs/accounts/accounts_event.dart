import 'package:remem_me/blocs/entities/entities.dart';
import 'package:repository_core/repository_core.dart';

class AccountSelected extends EntitiesEvent<AccountEntity> {
  final AccountEntity account;

  const AccountSelected(this.account);

  @override
  List<Object> get props => [account];

  @override
  String toString() => 'AccountSelected { account: ${account.id} }';
}
