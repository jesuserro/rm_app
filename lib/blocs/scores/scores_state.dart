import 'package:remem_me/blocs/blocs.dart';
import 'package:repository_core/repository_core.dart';


class ScoresLoadSuccess extends EntitiesLoadSuccess<ScoreEntity> {
  final int totalScore;

  const ScoresLoadSuccess(List<ScoreEntity> scores,  { required this.totalScore })
      : super(scores);

  @override
  List<Object> get props => [entities, totalScore];

  @override
  String toString() =>
      'ScoresLoadSuccess { entities: ${entities.length}, totalScore: $totalScore }';
}

