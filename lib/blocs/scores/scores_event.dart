import 'package:equatable/equatable.dart';
import 'package:remem_me/models/models.dart';

abstract class ScoresEvent extends Equatable {
  const ScoresEvent();
}

class ScoresUpdated extends ScoresEvent {
  final List<Verse> verses;

  const ScoresUpdated(this.verses);

  @override
  List<Object> get props => [verses];

  @override
  String toString() => 'UpdateScores { verses: $verses }';
}
