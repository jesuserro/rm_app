import 'dart:async';

import 'package:remem_me/services/score_service.dart';
import 'package:repository_core/repository_core.dart';

import '../blocs.dart';

class ScoresBloc extends EntitiesBloc<ScoreEntity, ScoresRepository> {
  final AccountsBloc accountsBloc;
  late StreamSubscription accountsSubscription;

  ScoresBloc({
    required ScoresRepository scoresRepository,
    required this.accountsBloc,
  }) : super(repository: scoresRepository, initialState: InitialEntitiesState<ScoreEntity>()) {
    accountsSubscription = accountsBloc.stream.listen((state) {
      if (state is AccountsLoadSuccess) {
        if (state.currentAccount != null) {
          add(EntitiesLoaded<ScoreEntity>(account: state.currentAccount));
        }
        /*else {
          add(EntitiesLoaded<ScoreEntity>());
        }*/
      }
    });
  }

  Stream<EntitiesState<ScoreEntity>> mapEntitiesLoadedToState(
      EntitiesLoaded<ScoreEntity> event) async* {
    yield EntitiesLoadInProgress();
    final entities = await repository.readList(account: event.account?.id);
    yield ScoresLoadSuccess(entities,
        totalScore: ScoreService().totalScore(entities));
  }

  Stream<EntitiesState<ScoreEntity>> mapEntitiesAddedToState(
      EntitiesAdded<ScoreEntity> event) async* {
    /* prevent multiple scoring for the same verse */
    final newScores = event.entities
        .where((newScore) => !(state as ScoresLoadSuccess).entities.any(
            (score) =>
                score.date == newScore.date && score.vkey == newScore.vkey))
        .toList();
    if (newScores.isNotEmpty) {
      final List<ScoreEntity> allScores =
          (state as ScoresLoadSuccess).entities + event.entities;
      yield ScoresLoadSuccess(allScores,
          totalScore: ScoreService().totalScore(allScores));
      repository.createList(
          newScores); // todo: EntitiesUpdateInProgress, EntitiesUpdateFailure
    }
  }

  @override
  Future<void> close() {
    accountsSubscription.cancel();
    return super.close();
  }
}
