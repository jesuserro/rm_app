import 'package:remem_me/blocs/entities/entities.dart';
import 'package:repository_core/repository_core.dart';

class TagsCustomState extends EntitiesState {
  final List<TagEntity> tags;

  const TagsCustomState([this.tags = const []]);

  @override
  List<Object> get props => [tags];

  @override
  String toString() => 'TagsCustomState { tags: $tags }';
}

