import 'package:remem_me/blocs/entities/entities.dart';
import 'package:repository_core/repository_core.dart';

class TagCustomized extends EntitiesEvent<TagEntity> {
  final TagEntity tag;

  const TagCustomized(this.tag);

  @override
  List<Object> get props => [tag];

  @override
  String toString() => 'TagCustomized { tag: $tag }';
}

