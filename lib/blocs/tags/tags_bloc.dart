import 'dart:async';

import 'package:repository_core/repository_core.dart';

import '../blocs.dart';

class TagsBloc extends EntitiesBloc<TagEntity, TagsRepository> {
  final AccountsBloc accountsBloc;
  late StreamSubscription accountsSubscription;

  TagsBloc({
    required TagsRepository tagsRepository, required this.accountsBloc,
  }) : super(
            repository: tagsRepository, initialState: InitialEntitiesState<TagEntity>()) {
    accountsSubscription = accountsBloc.stream.listen(_accountsBlocUpdated);
    // listen() does not replay earlier events -> update with current state
    _accountsBlocUpdated(accountsBloc.state);
  }

  void _accountsBlocUpdated(EntitiesState<AccountEntity>? state) {
    if (state is AccountsLoadSuccess) {
      if(state.currentAccount != null) {
        add(EntitiesLoaded<TagEntity>(account: state.currentAccount));
      } /*else {
        add(EntitiesLoaded<TagEntity>());
      }*/
    }
  }


  @override
  Stream<EntitiesState<TagEntity>> mapEventToState(EntitiesEvent<TagEntity> event) async* {
    yield* super.mapEventToState(event);
    if (event is TagCustomized) {
      // yield* _mapTagCustomizedToState();
    }
  }


  @override
  Future<void> close() {
    accountsSubscription.cancel();
    return super.close();
  }
}
