// Copyright 2018 The Flutter Architecture Sample Authors. All rights reserved.
// Use of this source code is governed by the MIT license that can be found
// in the LICENSE file.

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/ordered_verses/ordered_verses.dart';
import 'package:remem_me/blocs/study/study.dart';
import 'package:remem_me/screens/bin_screen.dart';
import 'package:remem_me/screens/collection_detail_screen.dart';
import 'package:remem_me/screens/decks_screen.dart';
import 'package:remem_me/screens/flashcard_screen.dart';
import 'package:remem_me/screens/reset_password_screen.dart';
import 'package:remem_me/screens/tags_screen.dart';
import 'package:remem_me/utils/build_context.dart';
import 'package:repository_core/repository_core.dart';
import 'package:repository_hive/repository_hive.dart';
import 'package:repository_http/repository_http.dart';

import 'blocs/bin/bin.dart';
import 'blocs/blocs.dart';
import 'blocs/entities/entities.dart';
import 'models/collections/collections.dart';
import 'models/models.dart';
import 'screens/screens.dart';
import 'services/collection_service.dart';

class AppRoutes {
  static const start = '/';
  static const home = '/verses';
  static const addVerse = '/verse-add';
  static const editVerse = '/verse-edit';
  static const verseBin = '/verse-bin';
  static const addAccount = '/account-add';
  static const editAccount = '/account-edit';
  static const accountBin = '/account-bin';
  static const scores = '/scores';
  static const tags = '/tags';
  static const tagBin = '/tag-bin';
  static const decks = '/decks';
  static const flashcards = '/flashcards';
  static const collections = '/collections';
  static const collection = '/collection';
  static const styles = '/styles';
  static const study = '/study';
  static const registerSuccess = '/register-success';
  static const registerError = '/register-error';
  static const resetPassword = '/reset-password';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    // for deep linking see
    // https://sellsbrothers.com/understanding-flutter-deep-links-on-the-web
    String? route;
    Map<String, String>? queryParameters;
    int? id;
    if (settings.name != null) {
      final uriData = Uri.parse(settings.name!);
      route = uriData.path;
      queryParameters = uriData.queryParameters;
      if (uriData.pathSegments.length > 1) {
        id = int.parse(uriData.pathSegments[1]);
        route = '/${uriData.pathSegments[0]}';
      }
    }

    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;

    switch (route) {
      case AppRoutes.registerSuccess:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => MessageScreen(messages: {
                  'messages': [
                    L10n.current!.t8('User.register.success.description')!
                  ]
                }, title: L10n.current!.t8('User.register.success.title')!));

      case AppRoutes.registerError:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => MessageScreen(messages: {
                  'messages': [
                    L10n.current!.t8('User.register.error.description')!
                  ]
                }, title: L10n.current!.t8('User.register.error.title')!));

      case AppRoutes.resetPassword:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => BlocProvider<ResetPasswordBloc>(
                create: (ctx) {
                  return ResetPasswordBloc(
                      service: PasswordService(http.Client()),
                      uidb64: queryParameters!['uidb64']!,
                      token: queryParameters['token']!);
                },
                child: ResetPasswordScreen()));

      case AppRoutes.start:
        return MaterialPageRoute<Verse>(
            settings: settings,
            builder: (ctx) {
              return StartScreen();
            });

      case AppRoutes.addVerse:
        return CupertinoPageRoute<Verse>(
            settings: settings,
            builder: (ctx) {
              return EditVerseScreen(onSave: (Verse verse) async {
                await RepositoryProvider.of<VersesRepository>(ctx)
                    .create(verse);
                BlocProvider.of<VersesBloc>(ctx)
                    .add(EntitiesAdded<Verse>([verse], persist: false));
              });
            });

      case AppRoutes.editVerse:
        return CupertinoPageRoute<Verse>(
            settings: settings,
            builder: (ctx) {
              return EditVerseScreen(
                  onSave: (verse) async {
                    await RepositoryProvider.of<VersesRepository>(ctx)
                        .update(verse);
                    BlocProvider.of<VersesBloc>(ctx)
                        .add(EntitiesUpdated<Verse>([verse], persist: false));
                  },
                  verse: args as Verse?);
            });

      case AppRoutes.verseBin:
        return CupertinoPageRoute(
            settings: settings,
            builder: (ctx) {
              return BlocProvider<BinBloc<VerseEntity>>(
                  create: (ctx) => BinBloc<VerseEntity>(
                        initialState: BinLoadInProgress<VerseEntity>(),
                        repository:
                            RepositoryProvider.of<VersesRepository>(ctx),
                        entitiesBloc: BlocProvider.of<VersesBloc>(ctx),
                      )..add(BinLoaded<VerseEntity>(account: args as Account?)),
                  child: BinScreen<VerseEntity>(
                      typeIcon: Icons.menu_book_rounded));
            });

      case AppRoutes.addAccount:
        return CupertinoPageRoute<Account>(
            settings: settings,
            builder: (ctx) {
              return EditAccountScreen(onSave: (account) async {
                await RepositoryProvider.of<AccountsRepository>(ctx)
                    .create(account);
                Settings().saveInt(Settings.CURRENT_ACCOUNT, account.id);
                BlocProvider.of<AccountsBloc>(ctx)
                    .add(EntitiesAdded<Account>([account], persist: false));
              });
            });

      case AppRoutes.editAccount:
        return CupertinoPageRoute<Account>(
            settings: settings,
            builder: (ctx) {
              final repository = RepositoryProvider.of<AccountsRepository>(ctx);
              final bloc = BlocProvider.of<AccountsBloc>(ctx);
              return EditAccountScreen(
                  onSave: (account) async {
                    await repository.update(account);
                    BlocProvider.of<AccountsBloc>(ctx).add(
                        EntitiesUpdated<Account>([account], persist: false));
                  },
                  onDelete: (account) async {
                    final deleted = account.copyWith(deleted: true);
                    await repository.update(account.copyWith(deleted: true));
                    bloc.add(
                        EntitiesUpdated<Account>([deleted], persist: false));
                  },
                  account: args as Account?);
            });

      case AppRoutes.accountBin:
        return CupertinoPageRoute(
            settings: settings,
            builder: (ctx) {
              return BlocProvider<BinBloc<AccountEntity>>(
                  create: (ctx) => BinBloc<AccountEntity>(
                        initialState: BinLoadInProgress<AccountEntity>(),
                        repository:
                            RepositoryProvider.of<AccountsRepository>(ctx),
                        entitiesBloc: BlocProvider.of<AccountsBloc>(ctx),
                      )..add(BinLoaded<AccountEntity>()),
                  child: BinScreen<AccountEntity>(
                      typeIcon: Icons.folder_shared_rounded));
            });

      case AppRoutes.scores:
        return CupertinoPageRoute(
            settings: settings,
            builder: (_) {
              return MultiBlocProvider(providers: [
                BlocProvider<FilteredVersesBloc>(
                    create: (ctx) => FilteredVersesBloc(
                          versesBloc: BlocProvider.of<TaggedVersesBloc>(ctx),
                        ))
              ], child: ScoresScreen());
            });

      case AppRoutes.styles:
        return CupertinoPageRoute(
            settings: settings,
            builder: (_) {
              return StylesScreen();
            });

      case AppRoutes.study:
        return CupertinoPageRoute(
            settings: settings,
            builder: (ctx) {
              return BlocProvider<StudyBloc>(
                  create: (ctx) =>
                      StudyBloc()..add(StudyStarted(args as Verse?)),
                  child: StudyScreen());
            });

      case AppRoutes.tags:
        return CupertinoPageRoute(
            settings: settings, builder: (_) => TagsScreen());

      case AppRoutes.tagBin:
        return CupertinoPageRoute(
            settings: settings,
            builder: (ctx) {
              return BlocProvider<BinBloc<TagEntity>>(
                  create: (ctx) => BinBloc<TagEntity>(
                        initialState: BinLoadInProgress<TagEntity>(),
                        repository: RepositoryProvider.of<TagsRepository>(ctx),
                        entitiesBloc: BlocProvider.of<TagsBloc>(ctx),
                      )..add(BinLoaded<TagEntity>(account: args as Account?)),
                  child: BinScreen<TagEntity>(typeIcon: Icons.label_rounded));
            });

      case AppRoutes.decks:
        return CupertinoPageRoute(
            settings: settings,
            builder: (ctx) {
              return RepositoryProvider<DecksRepository>(
                  create: (ctx) => DecksRepositoryHttp(
                      ctx.read<AuthBloc>().service, ctx.read<HttpService>()),
                  child: DecksScreen());
            });

      case AppRoutes.collections:
        return CupertinoPageRoute(
            settings: settings,
            builder: (_) {
              return BlocProvider<CollectionsBloc>(
                  create: (ctx) {
                    return CollectionsBloc(CollectionService(http.Client()))
                      ..add(CollectionsInitiated(Query(
                          language: currentAccount(ctx)?.language != null
                              ? currentAccount(ctx)?.language.substring(0, 2)
                              : null)));  // todo: improve for Chinese
                  },
                  child: CollectionsScreen());
            });

      case AppRoutes.collection:
        return MaterialPageRoute(
            settings: settings,
            builder: (ctx) {
              return BlocProvider<CollectionDetailBloc>(
                  create: (ctx) =>
                      CollectionDetailBloc(CollectionService(http.Client()))
                        ..add(CollectionDetailLoaded(id!)),
                  child: CollectionDetailScreen(onImport: (verses) {
                    BlocProvider.of<VersesBloc>(ctx)
                        .add(EntitiesUpdated<Verse>(verses as List<Verse>));
                  }));
            });

      case AppRoutes.flashcards:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) {
              return BlocProvider<FlashcardBloc>(
                  create: (ctx) => FlashcardBloc(
                        args as FlashcardState,
                        versesBloc: BlocProvider.of<VersesBloc>(ctx),
                        scoresBloc: BlocProvider.of<ScoresBloc>(ctx),
                        versesRepository:
                            RepositoryProvider.of<VersesRepository>(ctx),
                      ),
                  child: FlashcardScreen());
            });

      case AppRoutes.home:
      default:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) {
              return MultiBlocProvider(
                providers: [
                  BlocProvider<FilteredVersesBloc>(
                      create: (ctx) => FilteredVersesBloc(
                          versesBloc: BlocProvider.of<TaggedVersesBloc>(ctx))),
                  BlocProvider<TabBloc>(create: (context) => TabBloc()),
                ],
                child: MultiBlocProvider(
                  providers: [
                    BlocProvider<SelectionBloc>(
                        create: (ctx) => SelectionBloc(
                            tabBloc: BlocProvider.of<TabBloc>(ctx),
                            versesBloc: BlocProvider.of<VersesBloc>(ctx))),
                    BlocProvider<OrderedVersesAllBloc>(
                        // todo: inject them only before they're needed
                        create: (ctx) => OrderedVersesAllBloc(
                            filteredBloc:
                                BlocProvider.of<FilteredVersesBloc>(ctx))),
                    BlocProvider<OrderedVersesNewBloc>(
                        create: (ctx) => OrderedVersesNewBloc(
                            filteredBloc:
                                BlocProvider.of<FilteredVersesBloc>(ctx))),
                    BlocProvider<OrderedVersesDueBloc>(
                        create: (ctx) => OrderedVersesDueBloc(
                            filteredBloc:
                                BlocProvider.of<FilteredVersesBloc>(ctx))),
                    BlocProvider<OrderedVersesKnownBloc>(
                        create: (ctx) => OrderedVersesKnownBloc(
                            filteredBloc:
                                BlocProvider.of<FilteredVersesBloc>(ctx))),
                  ],
                  child: HomeScreen(),
                ),
              );
            });
    }
  }
}
