import 'package:intl/intl.dart';
import 'package:message_core/message_core.dart';
import 'package:time_machine/time_machine.dart';


/// Compares a DateTime to today in days
class FromToday {
  FromToday(this.l10ns);

  final L10n? l10ns;

  String? format(LocalDate? date) {
    if(date == null) {
      return '';
    }
    final days = date.epochDay - LocalDate.today().epochDay;
    if(days > 1) {
      return l10ns!.t8('future_format_string', [days.toString()]);
    } else if(days == 1) {
      return l10ns!.t8('tomorrow');
    } else if (days == 0) {
      return l10ns!.t8('today');
    } else if (days == -1) {
      return l10ns!.t8('yesterday');
    } else {
      return l10ns!.t8('past_format_string', [(-days).toString()]);
    }
  }

  static bool isFuture(String dateString) {
    DateTime dt = DateUtil.parse(dateString);
    return DateUtil.toDate(dt).difference(DateUtil.today()).inDays > 0;
  }

}


class DateUtil {
  static final formatter = new DateFormat('yyyy-MM-dd');

  static int now() {
    return new DateTime.now().millisecondsSinceEpoch;
  }

  static DateTime parse(String dateString) {
    return DateTime.parse(dateString).add(Duration(hours: 2));
  }

  static String format(DateTime dt) {
    return formatter.format(toDate(dt));
  }

  static DateTime today() {
    return toDate(DateTime.now());
  }

  static DateTime toDate(DateTime dt) {
    dt = dt.subtract(Duration(hours: 2));
    return DateTime(dt.year, dt.month, dt.day);
  }

  static int toDays(DateTime dt) {
    return toDate(dt).difference(DateTime(2001, 1, 1)).inDays;
  }

}