const MAX_INT = 9007199254740991; // pow(2, 53) - 1;
const MS_PER_DAY = 24 * 60 * 60 * 1000;
const String PREF_STRING = 'String';
const String PREF_INT = 'int';
const String PREF_DOUBLE = 'double';
const String PREF_BOOL = 'bool';
const String PREF_GROUP = 'GROUP';
const String PREF_HIDDEN = 'HIDDEN';  // don't sync
