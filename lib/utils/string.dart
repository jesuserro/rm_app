class StringUtil {
  static int countWords(String text, String language) {
    assert (language.length == 2);
    if(['zh'].contains(language)) {
      return 0; // todo: implement Chinese, Korean, Hebrew, Arabic...
    } else {
      return text.split(RegExp(r'\s+')).length;
    }
  }
}