import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository_core/repository_core.dart';
import 'package:url_launcher/url_launcher.dart';

Account? currentAccount(BuildContext ctx) {
  final EntitiesState<AccountEntity>? state =
      BlocProvider.of<AccountsBloc>(ctx).state;
  return state is AccountsLoadSuccess ? state.currentAccount : null;
}

void Function()? launchDocumentation(BuildContext ctx) {
  return () async {
    var languagePath = '';
    if (['de'].contains(L10n.of(ctx)?.locale.languageCode)) {
      languagePath = '${L10n.of(ctx)?.locale.languageCode}/';
    }
    final url = 'https://doc.remem.me/${languagePath}docs/';
    if (await canLaunch(url)) {
      await launch(url);
    }
  };
}
