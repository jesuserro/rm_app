import 'package:flutter/material.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/models/models.dart';

class DeleteVerseSnackBar extends SnackBar {
  final L10n localizations;

  DeleteVerseSnackBar({
    Key? key,
    required Verse verse,
    required VoidCallback onUndo,
    required this.localizations,
  }) : super(
          key: key,
          content: Text(
            localizations.t8('delete_success_1')!,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          duration: Duration(seconds: 2),
          action: SnackBarAction(
            label: localizations.t8('ok')!,
            onPressed: onUndo,
          ),
        );
}
