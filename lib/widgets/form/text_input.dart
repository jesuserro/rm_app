import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:message_core/message_core.dart';

typedef OptionBuilder = List<PopupMenuEntry<String>> Function(BuildContext);

class StringInput extends StatelessWidget {
  final GlobalKey<FormFieldState>? fieldKey;
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final int maxLines;
  final bool obscureText;
  final String? l10nKey;
  final String? l10nKeySummary;
  final IconData? iconData;
  final List<String>? autofillHints;
  final String? initialValue;
  final bool? autofocus;
  final FormFieldValidator<String>? validator;
  final AutovalidateMode? autovalidateMode;
  final OptionBuilder? optionBuilder;
  final ValueChanged<String>? onChanged;
  final FormFieldSetter<String>? onSaved;

  const StringInput(
      {Key? key,
      this.fieldKey,
      this.keyboardType,
      this.inputFormatters,
      this.maxLines = 1,
      this.obscureText = false,
      this.l10nKey,
      this.l10nKeySummary,
      this.iconData,
      this.autofillHints,
      this.initialValue,
      this.autofocus,
      this.validator,
      this.autovalidateMode,
      this.optionBuilder,
      this.onChanged,
      this.onSaved})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return TextFormField(
      key: fieldKey,
      decoration: DefaultInputDecoration(
        iconData: iconData,
        labelText: L10n.of(ctx)!.t8(l10nKey!),
        helperText:
            l10nKeySummary != null ? L10n.of(ctx)!.t8(l10nKeySummary!) : '',
        optionButton:
            optionBuilder != null ? _optionButton(optionBuilder!) : null,
      ),
      autofillHints: autofillHints,
      initialValue: initialValue ?? '',
      autofocus: autofocus ?? false,
      keyboardType: keyboardType,
      inputFormatters: inputFormatters,
      maxLines: maxLines,
      obscureText: obscureText,
      validator: validator,
      autovalidateMode: autovalidateMode,
      onChanged: onChanged,
      onSaved: ((value) => onSaved!(value!.isNotEmpty ? value : null)),
    );
  }

  _optionButton(OptionBuilder builder) {
    return PopupMenuButton<String>(
      icon: Icon(Icons.arrow_drop_down),
      onSelected: (String result) {
        fieldKey!.currentState!.didChange(result);
      },
      itemBuilder: builder,
    );
  }
}

class IntegerInput extends StringInput {
  IntegerInput(
      {Key? key,
      String? l10nKey,
      String? l10nKeySummary,
      IconData? iconData,
      int? initialValue,
      bool? autofocus,
      FormFieldValidator<int>? validator,
      FormFieldSetter<int>? onSaved})
      : super(
            key: key,
            keyboardType: TextInputType.number,
            inputFormatters: [FilteringTextInputFormatter.digitsOnly],
            l10nKey: l10nKey,
            l10nKeySummary: l10nKeySummary,
            iconData: iconData,
            initialValue: initialValue == null || initialValue == 0
                ? ''
                : initialValue.toString(),
            autofocus: autofocus,
            validator: validator != null
                ? (value) => validator(value!.isNotEmpty ? int.parse(value) : 0)
                : null,
            onSaved: (value) => onSaved!(
                value != null && value.isNotEmpty ? int.parse(value) : 0));
}

class DecimalInput extends StringInput {
  DecimalInput(
      {Key? key,
      String? l10nKey,
      String? l10nKeySummary,
      IconData? iconData,
      double? initialValue,
      bool? autofocus,
      FormFieldValidator<double>? validator,
      FormFieldSetter<double>? onSaved})
      : super(
            key: key,
            keyboardType: TextInputType.number,
            l10nKey: l10nKey,
            l10nKeySummary: l10nKeySummary,
            iconData: iconData,
            initialValue: initialValue == null || initialValue == 0.0
                ? ''
                : initialValue.toString(),
            autofocus: autofocus,
            validator: validator != null
                ? (value) =>
                    validator(value!.isNotEmpty ? double.parse(value) : 0)
                : null,
            onSaved: (value) =>
                onSaved!(value!.isNotEmpty ? double.parse(value) : 0.0));
}

class DropdownInput<T> extends StatelessWidget {
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final String? l10nKey;
  final String? l10nKeySummary;
  final IconData? iconData;
  final T? initialValue;
  final bool? autofocus;
  final Map<T, String?>? items;
  final FormFieldValidator<T>? validator;
  final FormFieldSetter<T>? onSaved;

  const DropdownInput(
      {Key? key,
      this.keyboardType,
      this.inputFormatters,
      this.l10nKey,
      this.l10nKeySummary,
      this.iconData,
      this.initialValue,
      this.autofocus,
      this.items,
      this.validator,
      this.onSaved})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return DropdownButtonFormField<T>(
      decoration: DefaultInputDecoration(
        iconData: iconData,
        labelText: L10n.of(ctx)!.t8(l10nKey!),
        helperText:
            l10nKeySummary != null ? L10n.of(ctx)!.t8(l10nKeySummary!) : '',
      ),
      value: initialValue,
      items: items!.entries.map<DropdownMenuItem<T>>((entry) {
        return DropdownMenuItem<T>(
          value: entry.key,
          child: Text(entry.value!),
        );
      }).toList(),
      validator: validator,
      onSaved: onSaved,
      onChanged: (value) {},
    );
  }
}

class DefaultInputDecoration extends InputDecoration {
  DefaultInputDecoration(
      {String? labelText,
      String? helperText,
      IconData? iconData,
      Widget? optionButton})
      : super(
            prefixIcon: iconData != null ? Icon(iconData) : null,
            suffixIcon: optionButton,
            isDense: true,
            filled: true,
            labelText: labelText,
            helperText: helperText,
            border: InputBorder.none);
}
