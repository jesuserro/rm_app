import 'package:flutter/material.dart';
import 'package:message_core/message_core.dart';
import 'package:repository_core/repository_core.dart';

typedef OnSaveCallback<T extends Entity> = Function(T entity);

// Parent class for all edit screens
// abstract class EditScreen<T extends Entity> extends StatefulWidget {}

abstract class EntityForm<T extends Entity> extends StatefulWidget {
  final OnSaveCallback<T> onSave;
  final OnSaveCallback<T>? onDelete;
  final T? entity;

  const EntityForm({
    Key? key,
    required this.onSave,
    this.onDelete,
    this.entity,
  }) : super(key: key);
}


abstract class EntityFormState<T extends Entity, W extends EntityForm<T>> extends State<W> {
  final formKey = GlobalKey<FormState>();
  final spacing = 16.0;
  bool isUpdating = false;

  Map<String, List<String>> errors = {};

  List<Widget> errorMessages(BuildContext ctx) {
    return errors['messages'] != null
    ? [
    Text(errors['messages']!.join('\n'),
        style: Theme.of(ctx)
            .textTheme
            .bodyText1!
            .copyWith(color: Theme.of(ctx).errorColor)),
    SizedBox(height: spacing),
    ]
        : [];
  }

  void saveForm() async {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      final result = widget.entity != null ? updatedEntity() : createdEntity();
      try {
        setState(() {
          isUpdating = true;
        });
        await widget.onSave(result);
        Navigator.of(context).pop(result);
      } on MessageException catch (e) {
        setState(() {
          errors = e.messages;
        });
      } finally {
        setState(() {
          isUpdating = false;
        });
      }
    }
  }

  void deleteEntity() async {
    if(widget.onDelete != null) {
      try {
        await widget.onDelete!(widget.entity!);
        Navigator.of(context).pop();
      } on MessageException catch (e) {
        setState(() {
          errors = e.messages;
        });
      }
    }
  }

  T updatedEntity();

  T createdEntity();

  List<Widget> fields(BuildContext ctx) {
    return errorMessages(ctx);
  }

}
