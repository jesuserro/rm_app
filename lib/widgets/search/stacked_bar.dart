
import 'package:flutter/material.dart';


class StackedBar extends StatelessWidget implements PreferredSizeWidget {
  final PreferredSizeWidget base;
  final Widget overlay;

  const StackedBar({Key? key, required this.base, required this.overlay}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      base,
      Positioned(top: 0, right: 0, left: 0, child: overlay)
    ],);
  }

  @override
  Size get preferredSize => base.preferredSize;
}