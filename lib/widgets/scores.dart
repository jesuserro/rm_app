import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:repository_core/repository_core.dart';
import 'package:remem_me/blocs/scores/scores.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:remem_me/widget_keys.dart';

class Scores extends StatelessWidget {
  Scores({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ScoresBloc, EntitiesState<ScoreEntity>>(
      builder: (context, state) {
        if (state is EntitiesLoadInProgress<ScoreEntity>) {
          return LoadingIndicator(
            key: RememMeKeys.scoresLoadInProgressIndicator,
          );
        } else if (state is ScoresLoadSuccess) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    L10n.of(context)!.t8('total')!,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 24.0),
                  child: Text(
                    '${state.entities.length}',
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    L10n.of(context)!.t8('scores_long')!,
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 24.0),
                  child: Text(
                    "${state.totalScore}",
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                )
              ],
            ),
          );
        } else {
          return Container(key: RememMeKeys.emptyScoresContainer);
        }
      },
    );
  }
}
