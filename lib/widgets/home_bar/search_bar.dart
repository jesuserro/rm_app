import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';

import '../../theme.dart';

class SearchBar extends StatelessWidget {
  const SearchBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return AppBar(
        elevation: 0,
        title: _searchField(ctx),
        leading: _clearSearchBtn(ctx),
        actions: [
          BlocBuilder<FilteredVersesBloc, FilteredVersesState>(
              builder: (ctx, FilteredVersesState state) =>
                  state is FilteredVersesLoadSuccess &&
                          state.activeFilter!.query.isNotEmpty
                      ? _confirmSearchBtn(ctx)
                      : SizedBox.shrink()),
        ]);
  }

  IconButton _clearSearchBtn(BuildContext ctx) {
    return IconButton(
          icon: Icon(Icons.close_rounded),
          onPressed: () {
            BlocProvider.of<SelectionBloc>(ctx).add(SearchDeactivated());
            BlocProvider.of<FilteredVersesBloc>(ctx)
                .add(SearchFilterUpdated(''));
          });
  }

  Theme _searchField(BuildContext ctx) {
    return Theme(
          data: AppTheme.dark,
          child: TextFormField(
            autofocus: true,
            initialValue: (BlocProvider.of<FilteredVersesBloc>(ctx).state
                    as FilteredVersesLoadSuccess)
                .activeFilter!
                .query,
            decoration: InputDecoration(
              hintText: 'Search',
              border: InputBorder.none,
            ),
            onChanged: (String value) {
              BlocProvider.of<FilteredVersesBloc>(ctx)
                  .add(SearchFilterUpdated(value));
            },
          ));
  }

  IconButton _confirmSearchBtn(BuildContext ctx) {
    return IconButton(
                        icon: Icon(Icons.check_rounded),
                        onPressed: () {
                          BlocProvider.of<SelectionBloc>(ctx)
                              .add(SearchDeactivated());
                        });
  }
}
