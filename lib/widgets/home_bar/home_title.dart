
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/accounts/accounts.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:repository_core/repository_core.dart';

class HomeTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) =>
      BlocBuilder<AccountsBloc, EntitiesState<AccountEntity>>(
          builder: (ctx, accountsState) =>
              BlocBuilder<FilteredVersesBloc, FilteredVersesState>(
                  builder: (ctx, filteredState) {
                    if (filteredState is FilteredVersesLoadSuccess &&
                        filteredState.activeFilter!.query.isNotEmpty) {
                      return Text('«${filteredState.activeFilter!.query}»');
                    } else if (accountsState is AccountsLoadSuccess &&
                        accountsState.currentAccount != null) {
                      return Text(accountsState.currentAccount!.name);
                    } else {
                      return Text(L10n.of(ctx)!.t8('app_name')!);
                    }
                  }));
}