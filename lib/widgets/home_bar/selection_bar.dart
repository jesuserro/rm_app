import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/dialogs/dialogs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/verse_service.dart';
import 'package:repository_core/repository_core.dart';

// This is the type used by the popup menu.
enum SelectionMenuAction { selectAll, setTags, delete, commit, moveToNew, moveToDue }

class SelectionBar extends StatelessWidget {
  const SelectionBar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return AppBar(
      elevation: 0,
      title: Text(
          '${BlocProvider.of<SelectionBloc>(ctx).state.selection.length} selected'),
      leading: IconButton(
          icon: Icon(Icons.arrow_back_rounded),
          onPressed: () =>
              BlocProvider.of<SelectionBloc>(ctx).add(SelectionUpdated({}))),
      actions: _menu(ctx, BlocProvider.of<SelectionBloc>(ctx).state.selection),
    );
  }

  /// Creates a menu for actions related to the selected verses
  List<Widget> _menu(BuildContext ctx, Map<int, Verse> selection) {
    return <Widget>[
      _btnSelectAll(ctx, selection),
      _btnSetTags(ctx, selection),
      PopupMenuButton<SelectionMenuAction>(
        onSelected: (SelectionMenuAction action) => _do(ctx, selection, action),
        itemBuilder: (BuildContext context) {
          final Box? box = BlocProvider.of<TabBloc>(ctx).state;
          final popupItems = <PopupMenuItem<SelectionMenuAction>>[
            _itmDelete(ctx),
          ];
          if ([Box.NEW].contains(box)) {
            popupItems.add(_itmCommit(ctx));
          }
          if ([Box.KNOWN].contains(box)) {
            popupItems.add(_itmMoveToDue(ctx));
          }
          if ([Box.DUE, Box.KNOWN].contains(box)) {
            popupItems.add(_itmMoveToNew(ctx));
          }
          return popupItems;
        },
      )
    ];
  }

  IconButton _btnSelectAll(BuildContext ctx, Map<int, Verse> selection) {
    return IconButton(
      icon: Icon(Icons.select_all_rounded),
      onPressed: () => _do(ctx, selection, SelectionMenuAction.selectAll),
    );
  }

  IconButton _btnSetTags(BuildContext ctx, Map<int, Verse> selection) {
    return IconButton(
      icon: Icon(Icons.label_rounded),
      onPressed: () => _do(ctx, selection, SelectionMenuAction.setTags),
    );
  }

  PopupMenuItem<SelectionMenuAction> _itmDelete(BuildContext ctx) {
    return PopupMenuItem<SelectionMenuAction>(
      value: SelectionMenuAction.delete,
      child: Text(L10n.of(ctx)!.t8('menu_delete')!),
    );
  }

  PopupMenuItem<SelectionMenuAction> _itmCommit(BuildContext ctx) {
    return PopupMenuItem<SelectionMenuAction>(
      value: SelectionMenuAction.commit,
      child: Text(L10n.of(ctx)!.t8('commit')!),
    );
  }

  PopupMenuItem<SelectionMenuAction> _itmMoveToDue(BuildContext ctx) {
    return PopupMenuItem<SelectionMenuAction>(
      value: SelectionMenuAction.moveToDue,
      child: Text(L10n.of(ctx)!.t8('menu_to_due')!),
    );
  }

  PopupMenuItem<SelectionMenuAction> _itmMoveToNew(BuildContext ctx) {
    return PopupMenuItem<SelectionMenuAction>(
      value: SelectionMenuAction.moveToNew,
      child: Text(L10n.of(ctx)!.t8('menu_to_new')!),
    );
  }

  /// Perfoms actions initiated from the menu
  _do(BuildContext ctx, Map<int, Verse> selection,
      SelectionMenuAction action) async {
    switch (action) {
      case SelectionMenuAction.selectAll:
        _selectAll(ctx);
        break;
      case SelectionMenuAction.setTags:
        await _setTags(ctx, selection);
        break;
      case SelectionMenuAction.delete:
        _delete(ctx, selection);
        break;
      case SelectionMenuAction.commit:
        _commit(ctx, selection);
        break;
      case SelectionMenuAction.moveToDue:
        _moveToDue(ctx, selection);
        break;
      case SelectionMenuAction.moveToNew:
        _moveToNew(ctx, selection);
        break;
    }
  }

  void _selectAll(BuildContext ctx) {
    final Box box = BlocProvider.of<TabBloc>(ctx).state;
    late List<Verse> verses;
    switch (box) {
      case Box.NEW:
        verses =
            (BlocProvider.of<OrderedVersesNewBloc>(ctx).state as OrderSuccess)
                .verses;
        break;
      case Box.DUE:
        verses =
            (BlocProvider.of<OrderedVersesDueBloc>(ctx).state as OrderSuccess)
                .verses;
        break;
      case Box.KNOWN:
        verses =
            (BlocProvider.of<OrderedVersesKnownBloc>(ctx).state as OrderSuccess)
                .verses;
        break;
      case Box.ALL:
        break;
    }
    BlocProvider.of<SelectionBloc>(ctx).add(SelectionUpdated(
        Map.fromEntries(verses.map((verse) => MapEntry(verse.id, verse)))));
  }

  Future _setTags(BuildContext ctx, Map<int, Verse> verseSelection) async {
    final updatedVerses = await showDialog<List<Verse>>(
        context: ctx,
        builder: (BuildContext context) {
          return TagSelectionDialog(
            tags: List.from((BlocProvider.of<TagsBloc>(ctx).state
                    as EntitiesLoadSuccess<TagEntity>)
                .entities),
            selection: verseSelection,
          );
        });
    if (updatedVerses != null) {
      BlocProvider.of<VersesBloc>(ctx)
          .add(EntitiesUpdated(updatedVerses, persist: false));
    }
  }

  void _delete(BuildContext ctx, Map<int, Verse> selection) {
    BlocProvider.of<VersesBloc>(ctx).add(EntitiesUpdated<Verse>(selection.values
        .map((verse) => verse.copyWith(deleted: true))
        .toList()));
  }

  void _commit(BuildContext ctx, Map<int, Verse> selection) {
    BlocProvider.of<VersesBloc>(ctx).add(EntitiesUpdated<Verse>(selection.values
        .map((verse) => VerseService().committed(verse))
        .toList()));
  }

  void _moveToDue(BuildContext ctx, Map<int, Verse> selection) {
    BlocProvider.of<VersesBloc>(ctx).add(EntitiesUpdated<Verse>(selection.values
        .map((verse) => VerseService().movedToDue(verse))
        .toList()));
  }

  void _moveToNew(BuildContext ctx, Map<int, Verse> selection) {
    BlocProvider.of<VersesBloc>(ctx).add(EntitiesUpdated<Verse>(selection.values
        .map((verse) => VerseService().movedToNew(verse))
        .toList()));
    //Navigator.pop(ctx); see https://medium.com/@felangelov/hi-eliseu-codinhoto-zeucxb-glad-you-enjoyed-the-post-4e2ea186e39f
  }
}
