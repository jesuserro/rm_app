import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/entities/entities.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/widgets/loading_indicator.dart';
import 'package:repository_core/repository_core.dart';

import '../../routes.dart';

class NavigationTags extends StatelessWidget {
  final bool popping;

  const NavigationTags({Key? key, this.popping = true}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Drawer(
        elevation: 0,
        child: BlocBuilder<TaggedVersesBloc, TaggedVersesState>(
            builder: (context, state) {
              if (state is TaggedVersesLoadSuccess) {
                final items = state.tags
                    .map((tag) =>
                    ListTile(
                        leading: _leadingIcon(tag, state),
                        title: Text(tag.text),
                        trailing: Text(tag.size.toString()),
                        onTap: () {
                          (BlocProvider.of<TagsBloc>(context)).add(
                              EntitiesUpdated<TagEntity>([_toggleTap(tag)],
                                  wait: false));
                        },
                        onLongPress: () {
                          (BlocProvider.of<TagsBloc>(context)).add(
                              EntitiesUpdated<TagEntity>([_toggleLongPress(tag)],
                                  wait: false));
                        },
                    ))
                    .toList();
                items.insertAll(0, [
                  ListTile(
                    leading: Icon(Icons.edit_rounded),
                    title: Text(L10n.of(ctx)!.t8('Tags.edit')!),
                    onTap: () async {
                      if(popping) Navigator.pop(ctx);
                      Navigator.pushNamed(
                        ctx,
                        AppRoutes.tags,
                      );
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.apps_outlined),
                    title: Text(L10n.of(ctx)!.t8('Verses.all')!),
                    onTap: () {
                      (BlocProvider.of<TagsBloc>(context)).add(
                          EntitiesUpdated<TagEntity>(_tagsToClear(state.tags),
                              wait: false));
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.label_off_outlined),
                    title: Text(L10n.of(ctx)!.t8('Verses.untagged')!),
                    onTap: () {
                      (BlocProvider.of<TagsBloc>(context)).add(
                          EntitiesUpdated<TagEntity>(_tagsToExclude(state.tags),
                              wait: false));
                    },
                  ),
                ]);
                return Stack(
                  children: [
                    ListView(children: items),
                    if (state.isUpdating) LoadingIndicator(),
                  ],
                );
              } else {
                return SizedBox.shrink();
              }
            }));
  }

  Widget _leadingIcon(Tag tag, TaggedVersesLoadSuccess state) {
    return Icon(tag.included != null
        ? tag.included!
        ? Icons.visibility_rounded
        : Icons.visibility_off_outlined
        : Icons.visibility_outlined);
  }

  Tag _toggleTap(Tag tag) {
    if (tag.included == null) {
      return tag.copyWith(included: false);
    } else {
      return tag.copyWith(nullValues: ['included']);
    }
  }

  Tag _toggleLongPress(Tag tag) {
    if (tag.included == true) {
      return tag.copyWith(included: false);
    } else {
      return tag.copyWith(included: true);
    }
  }

  Tag _nextState(Tag tag) {
    if (tag.included == false) {
      return tag.copyWith(nullValues: ['included']);
    } else if (tag.included == true) {
      return tag.copyWith(included: false);
    } else {
      return tag.copyWith(included: true);
    }
  }

  List<Tag> _tagsToClear(List<Tag> tags) {
    return tags
        .where((tag) => tag.included != null)
        .map((tag) => tag.copyWith(nullValues: ['included']))
        .toList();
  }

  List<Tag> _tagsToExclude(List<Tag> tags) {
    return tags
        .where((tag) => tag.included != false)
        .map((tag) => tag.copyWith(included: false))
        .toList();
  }
}
