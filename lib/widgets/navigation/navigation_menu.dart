import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/routes.dart';
import 'package:remem_me/utils/build_context.dart';
import 'package:remem_me/widgets/user/user.dart';
import 'package:repository_http/repository_http.dart';
import 'package:url_launcher/url_launcher.dart';

import 'navigation.dart';

class NavigationMenu extends StatelessWidget {
  final void Function(HomeMenuAction, {dynamic payload})? onAction;
  final bool? popping;

  const NavigationMenu({Key? key, this.onAction, this.popping})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Column(children: [
      ListTile(
        leading: Icon(Icons.public_rounded),
        title: Text(L10n.of(ctx)!.t8('Collections.search')!),
        onTap: () async {
          if (popping!) Navigator.pop(ctx);
          Navigator.pushNamed(
            ctx,
            AppRoutes.collections,
          );
        },
      ),
      ListTile(
        leading: Icon(Icons.dashboard_customize),
        title: Text(L10n.of(ctx)!.t8('Decks.edit')!),
        onTap: () async {
          if (popping!) Navigator.pop(ctx);
          Navigator.pushNamed(
            ctx,
            AppRoutes.decks,
          );
        },
      ),
      ListTile(
        leading: Icon(Icons.trending_up_rounded),
        title: Text(L10n.of(ctx)!.t8('scores')!),
        onTap: () async {
          if (popping!) Navigator.pop(ctx);
          Navigator.pushNamed(
            ctx,
            AppRoutes.scores,
          );
        },
      ),
      ListTile(
        leading: Icon(Icons.folder_shared_rounded),
        title: Text(L10n.of(ctx)!.t8('menu_account')!),
        onTap: () {
          if (popping!) Navigator.pop(ctx);
          onAction!(HomeMenuAction.editAccount);
        },
      ),
      ListTile(
        leading: Icon(Icons.restore_from_trash_rounded),
        title: Text(L10n.of(ctx)!.t8('Verses.restore')!),
        onTap: () async {
          if (popping!) Navigator.pop(ctx);
          Navigator.pushNamed(
            ctx,
            AppRoutes.verseBin,
            arguments: currentAccount(ctx),
          );
        },
      ),
      ListTile(
        leading: Icon(Icons.info_outline_rounded),
        title: Text(L10n.of(ctx)!.t8('menu_about')!),
        onTap: launchDocumentation(ctx),
      ),
      /* ListTile(
        leading: Icon(Icons.style_rounded),
        title: Text('Styles'),
        onTap: () async {
          if(popping!) Navigator.pop(ctx);
          Navigator.pushNamed(
            ctx,
            AppRoutes.styles,
          );
        },
      ), */
      BlocBuilder<AuthBloc, AuthState>(
        builder: (context, state) {
          if (state is LoginSuccess) {
            return ListTile(
              leading: Icon(Icons.logout),
              title: Text(L10n.of(ctx)!.t8('auth_logout')!),
              onTap: () {
                if (popping!) Navigator.pop(ctx);
                onAction!(HomeMenuAction.logout);
              },
            );
          } else {
            return ListTile(
              leading: Icon(Icons.login_rounded),
              title: Text(L10n.of(ctx)!.t8('auth_login')!),
              onTap: () async {
                await showDialog<int>(
                    context: ctx,
                    builder: (BuildContext context) => Dialog(
                        child: UserForm(
                            errors: state is LoginFailure
                                ? state.messages
                                : const {},
                            initialValues: state is AuthMessageState
                                ? {
                                    'email': state.credentials['email'],
                                    'password': state.credentials['password']
                                  }
                                : const {},
                            hasRepeatPassword: false,
                            titleL10nKey: 'User.login.title',
                            submitL10nKey: 'User.login.submit',
                            onSubmit: (Map<String, String> credentials) {
                              BlocProvider.of<AuthBloc>(ctx).add(LoggedIn(
                                  email: credentials['email']!,
                                  password: credentials['password']!));
                              Navigator.pop(ctx);
                            })));
                if (popping!) Navigator.pop(ctx);
              },
            );
          }
        },
      ),
    ]);
  }
}
