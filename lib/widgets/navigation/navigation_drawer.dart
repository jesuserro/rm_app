import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/entities/entities.dart';
import 'package:repository_core/repository_core.dart';

import 'navigation.dart';

enum HomeMenuAction {
  attachAccount,
  addAccount,
  editAccount,
  accountBin,
  login,
  logout,
}

class NavigationDrawer extends StatefulWidget {
  final void Function(HomeMenuAction, {dynamic payload})? onAction;
  final bool popping;

  const NavigationDrawer({Key? key, this.onAction, this.popping = true})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _NavigationDrawerState();
}

class _NavigationDrawerState extends State<NavigationDrawer> {
  bool isInAccountMode = false;

  @override
  Widget build(BuildContext ctx) {
    final items = <Widget>[];
    items.add(_header(ctx));
    if (isInAccountMode) {
      items.add(NavigationAccounts(
        onAction: widget.onAction,
        popping: widget.popping,
      ));
    } else {
      items.add(NavigationMenu(
        onAction: widget.onAction,
        popping: widget.popping,
      ));
    }
    return Drawer(
      elevation: 0,
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: Column(children: [
        Expanded(
            child: ListView(
                // Important: Remove any padding from the ListView.
                padding: EdgeInsets.zero,
                children: items)),
        FutureBuilder(
          future: PackageInfo.fromPlatform(),
          builder: (BuildContext context, AsyncSnapshot<PackageInfo> snapshot) {
            return Text('Version ' + (snapshot.data?.version ?? ''));
          },
        ),
      ]),
    );
  }

  Widget _header(BuildContext ctx) {
    return BlocBuilder<AccountsBloc, EntitiesState<AccountEntity>>(
        builder: (context, state) => UserAccountsDrawerHeader(
              accountName: Text(
                  state is AccountsLoadSuccess && state.currentAccount != null
                      ? state.currentAccount!.name
                      : L10n.of(ctx)!.t8('Account.none')!),
              accountEmail: null,
              onDetailsPressed: () =>
                  setState(() => isInAccountMode = !isInAccountMode),
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/img/bg_title.png'),
                    fit: BoxFit.cover,
                    alignment: Alignment.topCenter),
              ),
            ));
  }
}
