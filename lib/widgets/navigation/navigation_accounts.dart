import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/entities/entities.dart';
import 'package:remem_me/widgets/navigation/navigation.dart';
import 'package:repository_core/repository_core.dart';

class NavigationAccounts extends StatelessWidget {
  final void Function(HomeMenuAction, {dynamic payload})? onAction;
  final bool? popping;

  const NavigationAccounts({Key? key, this.onAction, this.popping}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<AccountsBloc, EntitiesState<AccountEntity>>(
        builder: (context, state) {
      if (state is EntitiesLoadSuccess<AccountEntity>) {
        final tiles = state.entities
            .map((AccountEntity account) => ListTile(
                  leading: Icon(Icons.folder_shared_rounded),
                  title: Text(account.name),
                  onTap: () {
                    (BlocProvider.of<AccountsBloc>(context))
                        .add(AccountSelected(account));
                    if(popping!) Navigator.pop(ctx);
                  },
                ))
            .toList();
        tiles.add(ListTile(
          leading: Icon(Icons.drive_folder_upload),
          title: Text(L10n.of(ctx)!.t8('Account.add')!),
          onTap: () {
            if(popping!) Navigator.pop(ctx);
            onAction!(HomeMenuAction.attachAccount);
          },
        ));
        tiles.add(ListTile(
          leading: Icon(Icons.add),
          title: Text(L10n.of(ctx)!.t8('Account.create')!),
          onTap: () async {
            if(popping!) Navigator.pop(ctx);
            onAction!(HomeMenuAction.addAccount);
          },
        ));
        tiles.add(ListTile(
          leading: Icon(Icons.restore_from_trash_rounded),
          title: Text(L10n.of(ctx)!.t8('Account.restore')!),
          onTap: () async {
            if(popping!) Navigator.pop(ctx);
            onAction!(HomeMenuAction.accountBin);
          },
        ));
        return Column(children: tiles);
      } else {
        return SizedBox.shrink();
      }
    });
  }
}
