import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/widgets/form/form.dart';
import 'package:repository_hive/repository_hive.dart';

class Setting extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SettingState();
  }
}

class _SettingState extends State<Setting> {
  String _value = '';

  @override
  void initState() {
    Settings().load(Settings.REPLICATION_SERVER).then((value) => setState(() {
          _value = value ?? _value;
        }));
    super.initState();
  }

  @override
  Widget build(BuildContext ctx) {
    return IconButton(
      icon: Icon(Icons.settings_rounded),
      onPressed: () async {
        await edit();
      },
    );
  }

  Future<String?> edit() {
    return showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return Dialog(child: StringInput(
            l10nKey: 'Replication server',
            initialValue: _value,
            onChanged: (value) async {
              await Settings().save(Settings.REPLICATION_SERVER, value);
              setState(() {
                _value = value;
              });
            },
          ));
        });
  }
}
