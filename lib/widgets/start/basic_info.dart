import 'package:flutter/material.dart';
import 'package:message_core/message_core.dart';
import 'package:repository_http/repository_http.dart';

import '../../theme.dart';
import '../svg.dart';

class BasicInfo extends StatelessWidget {
  final AuthState authState;
  final void Function()? onRegister;
  final void Function()? onLogin;

  const BasicInfo(
      {Key? key, required this.authState, this.onRegister, this.onLogin})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: ConstrainedBox(
              constraints: constraints.copyWith(
                  minHeight: constraints.maxHeight, maxHeight: double.infinity),
              child: Center(
                  child: constraints.maxWidth > 600
                      ? _landscapeLayout(ctx)
                      : _portraitLayout(ctx))));
    });
  }

  Widget _portraitLayout(BuildContext ctx) {
    return Container(
        constraints: BoxConstraints(maxWidth: 340.0, maxHeight: 800),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [_icon(), const SizedBox(height: 16), _text(ctx)]));
  }

  Widget _landscapeLayout(BuildContext ctx) {
    return Container(
        constraints: BoxConstraints(maxHeight: 300, maxWidth: 816),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [_icon(), const SizedBox(width: 16), _text(ctx)]));
  }

  Widget _icon() {
    return Container(
      color: Colors.transparent,
      child: SvgAsset('appicon', width: 240, height: 240),
    );
  }

  Widget _text(BuildContext ctx) {
    return Container(
      constraints: BoxConstraints(maxWidth: 300),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text('Remember My Word.',
              style: Theme.of(ctx)
                  .textTheme
                  .headline4!
                  .copyWith(fontWeight: FontWeight.w300, letterSpacing: -1.5)),
          Text('Remember Me.',
              style: Theme.of(ctx)
                  .textTheme
                  .headline4!
                  .copyWith(fontWeight: FontWeight.w300, letterSpacing: 5.5)),
          const SizedBox(height: 16),
          Text(L10n.of(ctx)!.t8('Start.summary')!,
              style: Theme.of(ctx).textTheme.headline6),
          const SizedBox(height: 16),
          _actions(ctx, authState),
        ],
      ),
    );
  }

  Widget _actions(BuildContext ctx, AuthState authState) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        authState is RegisterSuccess || authState is ForgotPasswordSuccess
            ? Container(child: Text(
                (authState as AuthMessageState)
                    .messages['messages']!
                    .join('\n'),
                style: Theme.of(ctx).textTheme.subtitle1))
            : ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: AppTheme.appBar.colorScheme.secondaryVariant),
                child: Text(L10n.of(ctx)!.t8('Start.register')!.toUpperCase()),
                onPressed: onRegister),
        const SizedBox(height: 16),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: AppTheme.appBar.colorScheme.primary),
          child: Text(L10n.of(ctx)!.t8('Start.login')!.toUpperCase()),
          onPressed: onLogin,
        ),
      ],
    );
  }
}
