import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/text_to_speech/tts.dart';

import '../../theme.dart';

class TtsFabRow extends StatelessWidget {
  Widget build(BuildContext ctx) {
    return BlocBuilder<TtsBloc, TtsState>(
        builder: (BuildContext ctx, TtsState state) {
      final ttsOn = state as TtsOn;
      var buttons = <Widget>[];
      switch (ttsOn.status) {
        case Progress.RUN:
          buttons = <Widget>[TtsFabStop(), SizedBox(width: 10), TtsFabPause()];
          break;
        case Progress.PAUSE:
          buttons = <Widget>[TtsFabStop(), SizedBox(width: 10), TtsFabResume()];
          break;
        case Progress.STOP:
          break;
      }
      return Row(mainAxisSize: MainAxisSize.min, children: buttons);
    });
  }
}

class TtsFabPause extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return FloatingActionButton(
      heroTag: 'pauseBtn',
      onPressed: () {
        BlocProvider.of<TtsBloc>(ctx).add(TtsPaused());
      },
      backgroundColor: AppTheme.dark.backgroundColor,
      child: Icon(
        Icons.pause_rounded,
        color: Colors.white,
      ),
    );
  }
}

class TtsFabResume extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return FloatingActionButton(
      heroTag: 'resumeBtn',
      onPressed: () {
        BlocProvider.of<TtsBloc>(ctx).add(TtsResumed());
      },
      backgroundColor: AppTheme.dark.backgroundColor,
      child: Icon(
        Icons.play_arrow_rounded,
        color: Colors.white,
      ),
    );
  }
}

class TtsFabStop extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return FloatingActionButton(
      heroTag: 'stopBtn',
      onPressed: () {
        BlocProvider.of<TtsBloc>(ctx).add(TtsStopped());
      },
      backgroundColor: AppTheme.dark.backgroundColor,
      child: Icon(
        Icons.stop_rounded,
        color: Colors.white,
      ),
    );
  }
}
