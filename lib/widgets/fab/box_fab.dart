import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/text_to_speech/tts.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/routes.dart';
import 'package:remem_me/widgets/widgets.dart';

class BoxFabRow extends StatelessWidget {
  Widget build(BuildContext ctx) {
    return BlocBuilder<TabBloc, Box>(builder: (BuildContext ctx, Box state) {
      var buttons = <Widget>[];
      switch (state) {
        case Box.NEW:
          buttons = [BoxFabTts(), SizedBox(width: 10), BoxFabAdd()];
          break;
        case Box.DUE:
          buttons = [BoxFabTts(), SizedBox(width: 10), BoxFabReview()];
          break;
        case Box.KNOWN:
          buttons = [BoxFabTts(), SizedBox(width: 10), BoxFabInverse()];
          break;
        case Box.ALL:
          // TODO: SpeedDial? https://pub.dev/packages/flutter_speed_dial
          break;
      }
      return Row(mainAxisSize: MainAxisSize.min, children: buttons);
    });
  }
}

/// FloatingActionButton that is aware of which box it is used on
abstract class BoxFab extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) =>
      BlocBuilder<TabBloc, Box>(builder: (BuildContext ctx, Box state) {
        return buildWithBloc(ctx, OrderedVersesBloc.from(ctx, state));
      });

  Widget buildWithBloc(BuildContext ctx, OrderedVersesBloc bloc);
}

class BoxFabAdd extends BoxFab {
  @override
  Widget buildWithBloc(BuildContext ctx, OrderedVersesBloc bloc) {
    return FloatingActionButton(
      heroTag: 'addBtn',
      onPressed: () {
        Navigator.pushNamed(ctx, AppRoutes.addVerse);
      },
      backgroundColor: Theme.of(ctx).colorScheme.secondary,
      child: Icon(
        Icons.add_rounded,
        color: Colors.white,
      ),
      tooltip: L10n.of(ctx)!.t8('menu_add'),
    );
  }
}

class BoxFabReview extends BoxFab {
  @override
  Widget buildWithBloc(BuildContext ctx, OrderedVersesBloc bloc) {
    return FloatingActionButton(
      heroTag: 'reviewBtn',
      onPressed: () {
        Navigator.pushNamed(ctx, AppRoutes.flashcards,
            arguments: MultiFlashcardRun(
                inverse: false,
                verses: [...(bloc.state as OrderSuccess).verses],
                box: Box.DUE));
      },
      backgroundColor: Theme.of(ctx).colorScheme.secondary,
      child: SvgAsset('cards_outline',
          height: 20, color: Colors.white),
    );
  }
}

class BoxFabInverse extends BoxFab {
  @override
  Widget buildWithBloc(BuildContext ctx, OrderedVersesBloc bloc) {
    return FloatingActionButton(
      heroTag: 'inverseBtn',
      onPressed: () {
        Navigator.pushNamed(ctx, AppRoutes.flashcards,
            arguments: MultiFlashcardRun(
                inverse: true,
                verses: [...(bloc.state as OrderSuccess).verses],
                box: Box.KNOWN));
      },
      backgroundColor: Theme.of(ctx).colorScheme.secondary,
      child: SvgAsset('cards_filled',
          height: 20, color: Colors.white),
    );
  }
}

class BoxFabTts extends BoxFab {
  @override
  Widget buildWithBloc(BuildContext ctx, OrderedVersesBloc bloc) {
    return FloatingActionButton(
      heroTag: 'ttsBtn',
      onPressed: () {
        BlocProvider.of<TtsBloc>(ctx).add(TtsStarted(
            verses: [...(bloc.state as OrderSuccess).verses], index: 0));
      },
      // backgroundColor: AppTheme.dark.backgroundColor,
      child: Icon(
        Icons.play_arrow_rounded,
        color: Colors.white,
      ),
    );
  }
}
