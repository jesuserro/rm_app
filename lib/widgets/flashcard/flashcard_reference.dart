import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:remem_me/services/text_service.dart';

import '../../theme.dart';
import 'flashcard.dart';

class FlashcardReference extends FlashcardLayout {
  const FlashcardReference({
    Key? key,
    required run,
    onTap,
  }) : super(key: key, run: run, onTap: onTap);

  final gradientBegin = Alignment.topRight;
  final gradientEnd = Alignment.bottomLeft;

  @override
  Widget content(BuildContext ctx) {
    return Padding(padding: EdgeInsets.all(16.0),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(run.verse.reference!,
            style: AppTheme.dark.textTheme.headline6!.copyWith(
                fontSize: 24.0,
                fontWeight: FontWeight.w600,
                shadows: textShadows())),
        SizedBox.fromSize(size: Size.fromHeight(8.0)),
        Text(
            run.hinting
                ? TextService().beginning(run.verse.passage!)
                : run.verse.topic ?? '',
            style: AppTheme.dark.textTheme.subtitle1!.copyWith(
                fontSize: 20.0,
                fontWeight: FontWeight.w600,
                shadows: textShadows())),
      ],
    ));
  }

  @override
  List<Widget> backgroundLayers() {
    final layers = <Widget>[];
    if (run.verse.level! < 0) {
      layers.add(defaultBackground());
    } else {
      layers.add(levelBackground());
    }
    if (run.verse.image != null) {
      layers.add(imageBackground());
    }
    return layers;
  }
}
