import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/utils/color.dart';

abstract class FlashcardLayout extends StatelessWidget {
  const FlashcardLayout({
    Key? key,
    required this.run,
    this.onTap,
  }) : super(key: key);

  final FlashcardRun run;
  final Function? onTap;

  Alignment get gradientBegin;

  Alignment get gradientEnd;

  Widget content(BuildContext ctx);

  @override
  Widget build(BuildContext ctx) {
    final layers = backgroundLayers();
    layers.add(Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: Colors.white.withOpacity(0.1),
        highlightColor: Colors.transparent,
        borderRadius: BorderRadius.circular(8),
        onTap: onTap as void Function()?,
        child: content(ctx),
      ),
    ));
    return Container(
      width: double.infinity,
      constraints: BoxConstraints(maxWidth: 700),
      child: Card(
        elevation: 4,
        clipBehavior: Clip.hardEdge,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
        child: Stack(children: layers),
      ),
    );
  }

  List<Widget> backgroundLayers();

  Widget levelBackground() {
    return Stack(children: [
      Container(
          color: run.verse.color ?? Flat.grey, margin: EdgeInsets.all(1.0)),
      lightGradientLayer()
    ]);
  }

  Widget blurredImageBackground() {
    return Stack(children: [
      ImageFiltered(
          imageFilter: ImageFilter.blur(sigmaX: 2.0, sigmaY: 2.0),
          child: imageBackground()),
      darkGradientLayer(),
    ]);
  }

  Container imageBackground() {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            fit: BoxFit.cover,
            image: CachedNetworkImageProvider(
              run.verse.image!,
            )),
      ),
    );
  }

  Container defaultBackground() {
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
      colors: [Color(0xFFB30000), Color(0xFFDD1133), Color(0xFF880033)],
      begin: gradientBegin,
      end: gradientEnd,
    )));
  }

  Container darkGradientLayer() {
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
      colors: [Color(0x66331100), Color(0x55001122), Color(0x77000022)],
      begin: gradientBegin,
      end: gradientEnd,
    )));
  }

  Container lightGradientLayer() {
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
      colors: [Color(0x44441100), Color(0x11001133), Color(0x55001144)],
      begin: gradientBegin,
      end: gradientEnd,
    )));
  }

  List<Shadow> textShadows() {
    return run.verse.image != null
        ? [
            Shadow(
              blurRadius: 4.0,
              color: Colors.black,
              offset: Offset(1.0, 2.0),
            ),
          ]
        : [];
  }
}
