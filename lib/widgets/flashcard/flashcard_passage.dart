import 'package:flutter/material.dart';

import 'flashcard.dart';

class FlashcardPassage extends FlashcardLayout {
  final gradientBegin = Alignment.topLeft;
  final gradientEnd = Alignment.bottomRight;

  const FlashcardPassage({
    Key? key,
    required run,
    onTap,
  }) : super(key: key, run: run, onTap: onTap);


  @override
  Widget content(BuildContext ctx) {
    return PassageScroller(run: run);
  }


  @override
  List<Widget> backgroundLayers() {
    final layers = <Widget>[];
    if (run.verse.level! < 0) {
      layers.add(defaultBackground());
    } else {
      layers.add(levelBackground());
    }
    if (run.verse.image != null) {
      layers.add(blurredImageBackground());
    }
    return layers;
  }
}



