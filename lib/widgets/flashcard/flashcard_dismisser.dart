import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';

class CommittedDismisser extends StatelessWidget {
  final Widget child;

  const CommittedDismisser({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Dismissible(
      // only backside should be dismissible
      key: UniqueKey(),
      direction: DismissDirection.startToEnd,
      background: Container(
          alignment: Alignment.center,
          child: Icon(Icons.launch_rounded, size: 64.0)),
      onDismissed: (direction) {
        if (direction == DismissDirection.startToEnd) BlocProvider.of<FlashcardBloc>(ctx)
            .add(FlashcardAction(VerseAction.COMMITTED));
      },
      child: child,
    );
  }
}

class ReviewedDismisser extends StatelessWidget {
  final Widget child;

  const ReviewedDismisser({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Dismissible(
      // only backside should be dismissible
      key: UniqueKey(),
      direction: DismissDirection.horizontal,
      background: _backgroundRemembered(ctx),
      secondaryBackground: _backgroundForgotten(ctx),
      onDismissed: (direction) => BlocProvider.of<FlashcardBloc>(ctx)
          .add(FlashcardAction(direction == DismissDirection.startToEnd
          ? VerseAction.REMEMBERED : VerseAction.FORGOTTEN)),
      child: child,
    );
  }

  Container _backgroundRemembered(BuildContext ctx) {
    return Container(
        alignment: Alignment.center, child: Icon(Icons.done, size: 64.0));
  }

  Container _backgroundForgotten(BuildContext ctx) {
    return Container(
        alignment: Alignment.center,
        child: Icon(
          Icons.clear_rounded,
          size: 64.0,
        ));
  }
}
