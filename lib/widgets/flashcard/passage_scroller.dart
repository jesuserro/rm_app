import 'package:flutter/material.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/services/text_service.dart';

import '../../theme.dart';

class PassageScroller extends StatefulWidget {
  final FlashcardRun run;

  const PassageScroller({Key? key, required this.run}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _PassageScrollerState();
  }
}

class _PassageScrollerState extends State<PassageScroller> {
  final ScrollController _scrollController = ScrollController();
  var lines;

  @override
  void initState() {
    super.initState();
    final lines = TextService().lines(widget.run.verse.passage!);
    if((widget.run.revealLines > 0 && widget.run.revealLines <= lines.length)) {
      WidgetsBinding.instance!.addPostFrameCallback((_) {
        _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 500),
          curve: Curves.easeOut,
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewport) => Scrollbar(
            child: SingleChildScrollView(
                key: const PageStorageKey<String>('passage_scroller'),
                controller: _scrollController,
                child: Container(
                    padding: EdgeInsets.all(16.0),
                    constraints: BoxConstraints(
                      minHeight: viewport.maxHeight,
                      minWidth: double.infinity,
                    ),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            _text(widget.run),
                            style: AppTheme.dark.textTheme.subtitle2!
                                .copyWith(height: 1.25),
                          )
                        ])))));
  }

  _text(FlashcardRun run) {
    final lines = TextService().lines(run.verse.passage!);
    if (run.revealLines == 0 || run.revealLines == lines.length) {
      return run.verse.passage;
    } else {
      return lines.getRange(0, run.revealLines).join('\n') +
          '\n...'/* * (lines.length - run.revealLines)*/;
    }
  }
}