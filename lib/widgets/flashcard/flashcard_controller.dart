import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/widgets/animation/flipper.dart';
import 'package:remem_me/widgets/loading_indicator.dart';

import 'flashcard.dart';

class FlashcardController extends StatefulWidget {
  @override
  _FlashcardControllerState createState() => _FlashcardControllerState();
}

class _FlashcardControllerState extends State<FlashcardController>
    with SingleTickerProviderStateMixin {
  AnimationController? _animationController;

  @override
  void initState() {
    super.initState();

    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 600));
  }

  @override
  void dispose() {
    _animationController!.dispose();
    super.dispose();
  }

  void _flip(BuildContext ctx, FlashcardRun run) {
    if (_animationController!.isAnimating) return;
    // ignore: close_sinks
    final bloc = BlocProvider.of<FlashcardBloc>(ctx);
    bloc.add(FlipStarted());
    if (run.reverse) {
      _animationController!.reverse().then((_) {
        bloc.add(LineRevealed(lines: 0));
        bloc.add(FlipEnded());
      });
    } else {
      _animationController!.forward().then((_) => bloc.add(FlipEnded()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<FlashcardBloc, FlashcardState>(
      listener: (ctx, state) {
        if (state is FlashcardEnd) {
          Navigator.pop(ctx);
        }
      },
      builder: (ctx, state) {
        if (state is FlashcardRun) {
          if (!state.visible) {
            _animationController!.reset();
            BlocProvider.of<FlashcardBloc>(ctx).add(FlipReset());
            return SizedBox.shrink();
          }
          return ClipRect(
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Center(
                child: Column(
                  children: <Widget>[
                    Expanded(
                        child: Container(
                            margin: EdgeInsets.fromLTRB(0.0, 4.0, 0.0, 0.0),
                            child: _buildCard(ctx, state))),
                    Container(
                        margin: EdgeInsets.fromLTRB(4.0, 0.0, 4.0, 0.0),
                        height: 40.0,
                        child: _buildToolbar(ctx, state))
                  ],
                ),
              ),
            ),
          );
        } else {
          return LoadingIndicator();
        }
      },
    );
  }

  Widget _buildCard(BuildContext ctx, FlashcardRun state) {
    if (state is MultiFlashcardRun) {
      return Flipper(
          animationController: _animationController,
          front: _buildFront(ctx, state, onTap: () => _flip(ctx, state)),
          back: ReviewedDismisser(
            child: _buildBack(ctx, state, onTap: () => _flip(ctx, state)),
          ),
          onTap: () => _flip(ctx, state));
    } else {
      switch (state.box) {
        case Box.NEW:
          return CommittedDismisser(child: _buildBack(ctx, state));
        case Box.DUE:
          return ReviewedDismisser(child: _buildBack(ctx, state));
        default:
          return Flipper(
              animationController: _animationController,
              front: _buildFront(ctx, state, onTap: () => _flip(ctx, state)),
              back: _buildBack(ctx, state, onTap: () => _flip(ctx, state)));
      }
    }
  }

  Widget _buildFront(BuildContext ctx, FlashcardRun state,
      {void Function()? onTap}) {
    return state.inverse
        ? FlashcardPassage(run: state, onTap: onTap)
        : FlashcardReference(run: state, onTap: onTap);
  }

  Widget _buildBack(BuildContext ctx, FlashcardRun state,
      {void Function()? onTap}) {
    return state.inverse
        ? FlashcardReference(run: state, onTap: onTap)
        : FlashcardPassage(run: state, onTap: onTap);
  }

  Widget _buildToolbar(
    BuildContext ctx,
    FlashcardRun state,
  ) {
    if (state.flipping) return SizedBox.shrink();
    if (state is MultiFlashcardRun && !state.reverse)
      return FlipToolbar(
        onTap: () {
          if (!state.inverse) {
            BlocProvider.of<FlashcardBloc>(ctx).add(LineRevealed(lines: 1));
          }
          _flip(ctx, state);
        },
      );
    switch (state.box) {
      case Box.NEW:
        return CommitToolbar();
      case Box.DUE:
        return ReviewToolbar(run: state);
      case Box.KNOWN:
        return state is MultiFlashcardRun
            ? ReviewToolbar(run: state)
            : FlipToolbar(onTap: () => _flip(ctx, state));
      default:
        return FlipToolbar(onTap: () => _flip(ctx, state));
    }
  }
}
