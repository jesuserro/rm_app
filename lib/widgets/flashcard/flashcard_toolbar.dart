import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/text_service.dart';

class ReviewToolbar extends StatelessWidget {
  final FlashcardRun run;

  const ReviewToolbar({Key? key, required this.run}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[_forgottenButton(ctx)],
      ),
      Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
        Icon(Icons.arrow_back_rounded),
        Icon(Icons.touch_app_rounded),
        Icon(Icons.arrow_forward_rounded),
      ]),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[_rememberedButton(ctx)],
      ),
    ]);
  }

  IconButton _rememberedButton(BuildContext ctx) {
    final revealAll = run.revealLines == 0 ||
        run.revealLines == TextService().lines(run.verse.passage!).length;
    return IconButton(
      icon: Icon(revealAll ? Icons.check_circle_rounded : Icons.help_rounded,
          color: Theme.of(ctx).colorScheme.primaryVariant),
      onPressed: () => BlocProvider.of<FlashcardBloc>(ctx).add(
          revealAll ? FlashcardAction(VerseAction.REMEMBERED) : LineRevealed()),
    );
  }

  IconButton _forgottenButton(BuildContext ctx) {
    return IconButton(
      icon: Icon(
        Icons.cancel_rounded,
        color: Theme.of(ctx).colorScheme.secondaryVariant,
      ),
      onPressed: () => BlocProvider.of<FlashcardBloc>(ctx)
          .add(FlashcardAction(VerseAction.FORGOTTEN)),
    );
  }
}

class CommitToolbar extends StatelessWidget {
  const CommitToolbar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      SizedBox.fromSize(size: Size.fromWidth(36)),
      Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
        SizedBox.fromSize(size: Size.fromWidth(36)),
        Icon(Icons.touch_app_rounded),
        Icon(Icons.arrow_forward_rounded),
      ]),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.launch_rounded,
                color: Theme.of(ctx).colorScheme.primary),
            onPressed: () => BlocProvider.of<FlashcardBloc>(ctx)
                .add(FlashcardAction(VerseAction.COMMITTED)),
          )
        ],
      ),
    ]);
  }
}

class FlipToolbar extends StatelessWidget {
  final Function onTap;

  const FlipToolbar({Key? key, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<FlashcardBloc, FlashcardState>(
        builder: (BuildContext ctx, FlashcardState state) =>
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    if (state is FlashcardRun && !state.inverse)
                      IconButton(
                          icon: Icon(Icons.help_outline_rounded),
                          onPressed: state is FlashcardRun && state.hinting
                              ? null
                              : () => BlocProvider.of<FlashcardBloc>(ctx)
                                  .add(Hinted())),
                  ]),
              Text(state is MultiFlashcardRun
                  ? '${state.index + 1}/${state.verses.length}'
                  : ''),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    IconButton(
                      icon: Icon(state is MultiFlashcardRun && !state.inverse
                          ? Icons.rule_rounded
                          : Icons.flip_rounded),
                      onPressed: onTap as void Function()?,
                    )
                  ]),
            ]));
  }
}
