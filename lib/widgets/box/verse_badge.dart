import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/models/verse.dart';
import 'package:remem_me/theme.dart';
import 'package:remem_me/utils/color.dart';
import 'package:remem_me/widgets/animation/animation.dart';

import '../svg.dart';

class VerseBadge extends StatefulWidget {
  final Verse verse;
  final void Function()? onSelect;
  final bool isSelected;

  const VerseBadge(
      {Key? key, required this.verse, this.onSelect, this.isSelected = false})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _VerseBadgeState();
}

class _VerseBadgeState extends State<VerseBadge>
    with SingleTickerProviderStateMixin {
  AnimationController? _animationController;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 400));
    if (widget.isSelected) {
      _animationController!.value = 1.0; // start from reverse
    }
  }

  @override
  Widget build(BuildContext ctx) {
    return Flipper(
      animationController: _animationController,
      onTap: _flip,
      front: _front(),
      back: _back(),
    );
  }

  Widget _front() {
    final layers = <Widget>[];
    if(widget.verse.level! < 0) {
      layers.add(_placeholder());
    } else {
      layers.add(_level());
    }
    if(widget.verse.image != null) {
      layers.add(_image());
    }
    return ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
        child: Stack(
          fit: StackFit.expand,
          children: layers,
        ));
  }

  Container _level() {
    return Container(
      color: widget.verse.color ?? Flat.grey,
      padding: EdgeInsets.all(4.0),
      width: double.infinity,
      height: double.infinity,
      child: widget.verse.level! > 0
          ? Text(widget.verse.level.toString(),
          style: AppTheme.dark.textTheme.overline)
          : SizedBox.shrink(),
    );
  }

  Widget _image() {
    return FittedBox(
        fit: BoxFit.cover,
        child: CachedNetworkImage(
        imageUrl: widget.verse.image!,
    ));
  }

  SvgAsset _placeholder() => SvgAsset('app_logo');

  Container _back() {
    return Container(
      decoration: BoxDecoration(
          color: Flat.grey, borderRadius: BorderRadius.circular(4)),
      width: double.infinity,
      height: double.infinity,
      child: Icon(
        Icons.check_rounded,
        color: Colors.white,
        size: 32.0,
      ),
    );
  }

  _flip() {
    if (!widget.isSelected) {
      _animationController!.forward().then((_) => widget.onSelect!());
    } else {
      _animationController!.reverse().then((_) => widget.onSelect!());
    }
  }
}
