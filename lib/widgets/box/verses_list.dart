import 'package:flutter/material.dart';
import 'package:remem_me/models/models.dart';

class VersesList extends StatelessWidget {
  final List<Verse> verses;
  final Widget Function(Verse verse) createTile;
  final ScrollController? controller;

  const VersesList(
      {Key? key,
      required this.verses,
      required this.createTile,
      this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return Scrollbar(
        child: ListView.builder(
      controller: controller,
      itemCount: verses.length,
      itemBuilder: (BuildContext context, int index) {
        final verse = verses[index];
        return createTile(verse);
      },
    ));
  }
}
