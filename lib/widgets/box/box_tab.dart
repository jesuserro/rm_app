import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/dialogs/dialogs.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository_core/repository_core.dart';

import '../../theme.dart';
import '../svg.dart';
import '../widgets.dart';

class BoxTabBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Widget build(BuildContext ctx) {
    final controller = DefaultTabController.of(ctx);
    controller!.addListener(() {
      if (!controller.indexIsChanging) {
        BlocProvider.of<TabBloc>(ctx)
            .add(TabUpdated(Box.values[controller.index]));
      }
    });
    return TabBar(
      // todo: display "all" without tabs, from top actions
      //indicatorColor: Flat.amber,
      labelStyle: Theme.of(ctx).textTheme.caption,
      labelPadding: EdgeInsets.symmetric(horizontal: 2.0),
      tabs: [
        BlocProvider<OrderedVersesBloc>.value(
          value: BlocProvider.of<OrderedVersesNewBloc>(ctx),
          child: _BoxTab(
              iconData: Icons.inbox_rounded,
              l10nKey: 'new_verses',
              tab: Box.NEW),
        ),
        BlocProvider<OrderedVersesBloc>.value(
          value: BlocProvider.of<OrderedVersesDueBloc>(ctx),
          child: _BoxTab(
              iconData: Icons.check_box_outline_blank_rounded,
              l10nKey: 'due_verses',
              tab: Box.DUE),
        ),
        BlocProvider<OrderedVersesBloc>.value(
          value: BlocProvider.of<OrderedVersesKnownBloc>(ctx),
          child: _BoxTab(
              iconData: Icons.check_box_rounded,
              l10nKey: 'reviewed_verses',
              tab: Box.KNOWN),
        ),
      ],
      onTap: (i) async {
        if (!controller.indexIsChanging) {
          final prevOrder =
              (OrderedVersesBloc.from(ctx, Box.values[i]).state as OrderSuccess)
                  .order;
          final nextOrder = await showDialog<VerseOrder>(
              context: ctx,
              builder: (BuildContext context) {
                return BoxOrderDialog(
                  tab: Box.values[i],
                  prevOrder: prevOrder,
                );
              });
          if (nextOrder != null) {
            OrderedVersesBloc.from(ctx, Box.values[i])
                .add(OrderUpdated(nextOrder));
          }
        }
      },
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(48.0);
}

class _BoxTab extends StatelessWidget {
  final Box? tab;
  final String? l10nKey;
  final IconData? iconData;

  const _BoxTab({Key? key, this.tab, this.l10nKey, this.iconData})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<TabBloc, Box>(builder: (context, state) {
      final List<Widget> stackItems = [
        SizedBox(
            width: double.infinity,
            height: 48.0,
            child: Tab(
              icon: Icon(iconData, size: 28.0),
              iconMargin: EdgeInsets.all(1.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(L10n.of(ctx)!.t8(l10nKey!)!.toUpperCase() + ' ',
                      style: AppTheme.dark.textTheme.caption),
                  BoxCount(),
                ],
              ),
            )),
      ];
      if (state == tab) {
        stackItems.add(Positioned(
          bottom: 2,
          right: 0,
          child: SvgAsset('triangle',
              height: 10, color: AppTheme.dark.textTheme.caption!.color),
        ));
      }

      return Stack(children: stackItems);
    });
  }
}
