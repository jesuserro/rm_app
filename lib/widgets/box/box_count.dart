import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/ordered_verses/ordered_verses.dart';

import '../../theme.dart';

class BoxCount extends StatelessWidget {

  const BoxCount({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<OrderedVersesBloc, OrderedVersesState>(
        builder: (ctx, OrderedVersesState state) =>
            Text(state is OrderSuccess ? state.verses.length.toString() : '',
              style: AppTheme.dark.textTheme.caption,
            ));
  }
}
