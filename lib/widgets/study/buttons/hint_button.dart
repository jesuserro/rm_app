import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/study/study.dart';

import '../study.dart';

class HintButton extends StudyButton {
  final bool Function(LineUp) ifPlaceholders;
  final bool Function(LineUp) ifInitials;

  const HintButton(
      {Key? key,
      bool small = false,
      required this.ifPlaceholders,
      required this.ifInitials})
      : super(key: key, small: small);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StudyBloc, StudyState>(
        builder: (BuildContext ctx, StudyState state) {
      return IconButton(
          padding: padding(),
          onPressed: (state as Study).isComplete ? null : () => nextHint(ctx, state as LineUp),
          icon: Icon(hintIcon(ctx, state as LineUp)));
    });
  }

  IconData hintIcon(BuildContext ctx, LineUp state) {
    final p = state.hasPlaceholders, i = state.hasInitials;
    if (p && i) return Icons.format_underline_rounded;
    if (p && !i) return Icons.minimize_rounded;
    if (!p && i) return Icons.title_outlined;
    return Icons.help_outline_rounded; // !p && !i
  }

  void nextHint(BuildContext ctx, LineUp state) {
    BlocProvider.of<StudyBloc>(ctx).add(Hinted(
        hasPlaceholders: ifPlaceholders(state),
        hasInitials: ifInitials(state)));
  }
}
