import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/study/study.dart';
import 'package:remem_me/widgets/study/study.dart';

class MenuButton extends StudyButton {

  const MenuButton({Key? key, bool small = false}) : super(key: key, small: small);

  @override
  Widget build(BuildContext ctx) {
    return IconButton(
        padding: EdgeInsets.all(small! ? 4.0 : 8.0),
        onPressed: () {
          BlocProvider.of<StudyBloc>(ctx).add(StudyStarted());
        },
        icon: Icon(Icons.more_horiz_rounded));
  }

}