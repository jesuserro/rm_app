import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/study/study.dart';
import 'package:remem_me/utils/color.dart';

import '../../../theme.dart';

class TypingBox extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return Row(
      children: [
        _display(ctx),
        _listener(ctx),
      ],
    );
  }

  Widget _display(BuildContext ctx) {
    return BlocBuilder<StudyBloc, StudyState>(
        builder: (BuildContext ctx, StudyState state) {
          final _state = state as Typing;
          return Container(
              padding: EdgeInsets.only(left: 8, right: 8),
              constraints: BoxConstraints(minWidth: 32),
              decoration: BoxDecoration(
                color: _state.isCorrect == null
                    ? AppTheme.dark.floatingActionButtonTheme.backgroundColor
                    : _state.isCorrect!
                    ? Flat.amber
                    : Flat.indigo,
                borderRadius: BorderRadius.all(Radius.circular(8)),
              ),
              child: Center(
                  child: Text(
                    _state.letters ?? '',
                    style: AppTheme.dark.textTheme.button!
                        .copyWith(fontSize: 18, fontWeight: FontWeight.w400),
                  )),
              height: 32);
        });
  }

  SizedBox _listener(BuildContext ctx) {
    final textController = TextEditingController();
    return SizedBox.shrink(
        child: TextField(
          controller: textController,
          autofocus: true,
          showCursor: false,
          onChanged: (value) {
            textController.clear();
            BlocProvider.of<StudyBloc>(ctx).add(Typed(letters: value));
          },
        ));
  }
}
