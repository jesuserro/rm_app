import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/study/study.dart';

import '../study.dart';

class RetractButton extends StudyButton {

  const RetractButton({Key? key, bool small = false}) : super(key: key, small: small);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StudyBloc, StudyState>(
        builder: (BuildContext ctx, StudyState state) {
      final _state = state as LineUp;
      return IconButton(
          padding: padding(),
          onPressed: _state.revealed!.word > -1 || _state.revealed!.line > 0
              ? () {
                  BlocProvider.of<StudyBloc>(ctx).add(Retracted());
                }
              : null,
          icon: Icon(Icons.backspace_outlined));
    });
  }
}
