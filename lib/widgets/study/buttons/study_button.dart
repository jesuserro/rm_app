import 'package:flutter/cupertino.dart';

abstract class StudyButton extends StatelessWidget {
  final bool? small;

  const StudyButton({Key? key, this.small}) : super(key: key);

  EdgeInsets padding() {
    return EdgeInsets.all(small! ? 4.0 : 8.0);
  }

}