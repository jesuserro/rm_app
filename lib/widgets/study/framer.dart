import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/study/study.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/text_service.dart';
import 'package:remem_me/utils/color.dart';

typedef FormatCallback = Widget Function(String);

class Framer {
  final BuildContext ctx;
  final Study state;

  Framer(this.ctx, this.state);

  Widget format(Pos pos) {
    final _state = state;
    if (_state is Obfuscation && _state.revealed![pos.line][pos.word]) {
      return _blurShow(_state.lines![pos.line][pos.word]);
    } else if (_state is Obfuscation && _state.obfuscated![pos.line][pos.word]) {
      return _blur(pos);
    } else if ((_state is LineUp) &&
        (pos.line > _state.revealed!.line ||
            pos.line == _state.revealed!.line &&
                pos.word > _state.revealed!.word)) {
      return _formatHint(_state.lines![pos.line][pos.word]);
    } else {
      return _strip(_state.lines![pos.line][pos.word], _plain);
    }
  }

  Widget _plain(String word) {
    return Text(word);
  }

  Widget _blur(Pos pos) {
    return InkWell(
        child: ImageFiltered(
            imageFilter: ImageFilter.blur(sigmaX: 4.0, sigmaY: 4.0),
            child: Text(state.lines![pos.line][pos.word])),
        onTap: () => BlocProvider.of<StudyBloc>(ctx).add(Revealed(pos)));
  }

  Widget _blurShow(String word) {
    return Text(word, style: TextStyle(shadows: _shadows()));
  }

  Widget _underline(String word) {
    return Stack(children: [
      _lineFill(),
      Text(word, style: _styleTransparent()),
    ]);
  }

  Widget _lineShow(String word) {
    return Stack(children: [
      _lineFill(),
      Text(word),
    ]);
  }

  Widget _initial(String word) {
    return _lineShow(word.substring(0, 1));
  }

  Widget _initialUnderline(String word) {
    return Stack(children: [
      _lineFill(),
      Text(word, style: _styleTransparent()),
      Text(word.substring(0, 1)),
    ]);
  }

  Widget _formatHint(String word) {
    final _state = state;
    if (_state is LineUp) {
      final p = _state.hasPlaceholders, i = _state.hasInitials;
      if (p && i) return _strip(word, _initialUnderline);
      if (p && !i) return _strip(word, _underline);
      if (!p && i) return _strip(word, _initial);
    }
    return SizedBox.shrink();
  }

  _strip(String word, FormatCallback format) {
    final before = TextService().punctuationBefore(word);
    final after = TextService().punctuationAfter(word);
    return Row(mainAxisSize: MainAxisSize.min, children: [
      if (before > 0) Text(word.substring(0, before)),
      if (before < word.length && after < word.length)
        format(word.substring(before, word.length - after)),
      if (after > 0 && after < word.length)
        Text(word.substring(word.length - after, word.length)),
    ]);
  }

  Widget _lineFill() {
    return Positioned.fill(
        child: Container(
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(
                  color: Theme.of(ctx).colorScheme.secondary.withOpacity(0.32),
                  width: 7.0))),
    ));
  }

  TextStyle? stylePlain() {
    return Theme.of(ctx).textTheme.subtitle2;
  }

  TextStyle _styleTransparent() {
    return Theme.of(ctx)
        .textTheme
        .subtitle2!
        .copyWith(color: Colors.transparent);
  }

  TextStyle _styleUnderline() {
    return TextStyle(
      decoration: TextDecoration.underline,
      decorationThickness: 8.0,
      decorationColor: Flat.amber,
    );
  }

  TextStyle _styleUnderlineTransparent() {
    return _styleUnderline().copyWith(color: Colors.transparent);
  }

  List<Shadow> _shadows() {
    return [
      Shadow(
        blurRadius: 10.0,
        color: Theme.of(ctx).textTheme.subtitle2!.color!,
      )
    ];
  }

  @Deprecated('use blur() instead')
  Widget _blurDeprecated(Study state, Pos pos) {
    return InkWell(
      child: Text(state.lines![pos.line][pos.word],
          style: TextStyle(color: Colors.transparent, shadows: _shadows())),
      onTap: () => BlocProvider.of<StudyBloc>(ctx).add(Revealed(pos)),
    );
  }

  @Deprecated('use underline() instead')
  Widget lineDeprecated(String word) {
    return Text(word, style: _styleUnderlineTransparent());
  }

  @Deprecated('use lineShow() instead')
  Widget lineShowDeprecated(String word) {
    return Stack(children: [
      Text(word, style: _styleUnderlineTransparent()),
      Text(word)
    ]);
  }

  @Deprecated('use initialUnderline() instead')
  Widget initialLineDeprecated(String word) {
    return Stack(children: [
      Text(word, style: _styleUnderlineTransparent()),
      Text(word.substring(0, 1))
    ]);
  }
}
