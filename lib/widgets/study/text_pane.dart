import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/study/study.dart';
import 'package:remem_me/models/models.dart';

import 'framer.dart';

class TextPane extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _TextPaneState();
  }
}

class _TextPaneState extends State<TextPane> {
  final ScrollController _scrollController = ScrollController();
  Study? study;

  @override
  void didUpdateWidget(Widget oldWidget) {
    super.didUpdateWidget(oldWidget as TextPane);
    if (study != null && [LineUp, Puzzle, Typing].contains(study.runtimeType)) {
      WidgetsBinding.instance!.addPostFrameCallback((_) {
        _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 500),
          curve: Curves.easeOut,
        );
      });
    }
  }

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<StudyBloc, StudyState>(
        builder: (BuildContext ctx, StudyState state) {
      if (state is Study) {
        final study = state;
        final framer = Framer(ctx, state);
        var wraps = <Widget>[];
        for (var i = 0; i < study.lines!.length && i <= study.pos.line; i++) {
          var items = <Widget>[];
          for (var j = 0;
          i < study.pos.line && j < study.lines![i].length ||
              i == study.pos.line && j <= study.pos.word;
          j++) {
            items.add(framer.format(Pos(i, j)));
          }
          wraps.add(Wrap(
            spacing: 8.0, // gap between adjacent chips
            runSpacing: 2.0, // gap between lines
            children: items,
          ));
        }

        return Scrollbar(
            child: SingleChildScrollView(
                key: const PageStorageKey<String>('study_scroller'),
                controller: _scrollController,
                child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: DefaultTextStyle(
                        style: framer.stylePlain()!,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: wraps)))));
      } else {
        return SizedBox.shrink();
      }
    });
  }
}
