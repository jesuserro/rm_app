import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/study/study.dart';

import '../study.dart';

class LineUpControl extends StudyControl<LineUp> {
  @override
  List<Widget> buttons(BuildContext ctx, LineUp state) {
    return [
      HintButton(
        ifPlaceholders: (state) => !state.hasPlaceholders,
        ifInitials: (state) => state.hasPlaceholders == state.hasInitials,
      ),
      Expanded(
          child: state.isComplete
              ? MenuButton()
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _btnNextWord(ctx),
                    _btnNextLine(ctx),
                  ],
                )),
      RetractButton(),
    ];
  }

  IconButton _btnNextWord(BuildContext ctx) {
    return IconButton(
        onPressed: () {
          BlocProvider.of<StudyBloc>(ctx).add(LinedUp(byLine: false));
        },
        icon: Icon(Icons.plus_one_rounded));
  }

  IconButton _btnNextLine(BuildContext ctx) {
    return IconButton(
        onPressed: () {
          BlocProvider.of<StudyBloc>(ctx).add(LinedUp(byLine: true));
        },
        icon: Icon(Icons.playlist_add_rounded));
  }

}
