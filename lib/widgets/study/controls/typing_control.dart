import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/study/study.dart';
import 'package:remem_me/widgets/study/study.dart';

class TypingControl extends StudyControl<Typing> {
  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<StudyBloc, StudyState>(
        builder: (BuildContext ctx, StudyState state) {
      return Container(
          height: 32, alignment: Alignment.center, child: bar(ctx, state as Typing));
    });
  }

  @override
  Widget bar(BuildContext ctx, Typing state) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.max,
      children: buttons(ctx, state),
    );
  }

  @override
  List<Widget> buttons(BuildContext ctx, Typing state) {
    return [
      HintButton(
        small: true,
        ifPlaceholders: (state) =>
            state.hasInitials && !state.hasPlaceholders ||
            !state.hasInitials && state.hasPlaceholders,
        ifInitials: (state) => true,
      ),
      state.isComplete
          ? MenuButton() : TypingBox(),
      RetractButton(small: true),
    ];
  }
}
