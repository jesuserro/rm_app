import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/study/study.dart';

class StudyControl<T extends Study> extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<StudyBloc, StudyState>(
        builder: (BuildContext ctx, StudyState state) {
      return Container(
          height: 56,
          alignment: Alignment.center,
          child: state is Study ? bar(ctx, state as T) : SizedBox.shrink());
    });
  }

  Widget bar(BuildContext ctx, T state) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      mainAxisSize: MainAxisSize.max,
      children: buttons(ctx, state),
    );
  }

  List<Widget> buttons(BuildContext ctx, T state) {
    return [
      IconButton(
          onPressed: () {
            BlocProvider.of<StudyBloc>(ctx).add(Obfuscated());
          },
          icon: Icon(Icons.wb_cloudy_rounded)),
      IconButton(
          onPressed: () {
            BlocProvider.of<StudyBloc>(ctx).add(Puzzled());
          },
          icon: Icon(Icons.widgets_rounded)),
      IconButton(
          onPressed: () {
            BlocProvider.of<StudyBloc>(ctx).add(LinedUp());
          },
          icon: Icon(Icons.subject_rounded)),
      IconButton(
          onPressed: () {
            BlocProvider.of<StudyBloc>(ctx).add(Typed());
          },
          icon: Icon(Icons.keyboard_rounded)),
    ];
  }
}
