import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/study/study.dart';

import '../study.dart';

class ObfuscationControl extends StudyControl<Obfuscation> {

  @override
  List<Widget> buttons(BuildContext ctx, Obfuscation state) {
    if (state.isComplete) {
      return [
        IconButton(
            onPressed: () {
              BlocProvider.of<StudyBloc>(ctx).add(StudyStarted());
            },
            icon: Icon(Icons.more_horiz_rounded)),
      ];
    } else {
      return [
        IconButton(
            onPressed: () {
              BlocProvider.of<StudyBloc>(ctx).add(Obfuscated());
            },
            icon: Icon(Icons.wb_cloudy_rounded)),
      ];
    }
  }
}
