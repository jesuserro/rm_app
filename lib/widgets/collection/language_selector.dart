import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:remem_me/blocs/blocs.dart';

class LanguageSelector extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return Expanded(
        child: Row(children: [
      Icon(
        Icons.language_rounded,
        color: Theme.of(ctx).textTheme.caption!.color,
      ),
      SizedBox(width: 8.0),
      Expanded(child: BlocBuilder<CollectionsBloc, CollectionsState>(
          builder: (BuildContext ctx, CollectionsState state) {
        return DropdownButton<String>(
            value: state.query.language,
            underline: SizedBox.shrink(),
            onChanged: (String? value) {
              BlocProvider.of<CollectionsBloc>(ctx).add(
                  CollectionsInitiated(state.query.copyWith(language: value)));
            },
            items: <String>['en', 'de', 'fr', 'es']
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(LocaleNames.of(ctx)!.nameOf(value)!),
              );
            }).toList());
      }))
    ]));
  }
}
