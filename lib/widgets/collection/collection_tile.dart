import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:remem_me/models/collections/collections.dart';
import 'package:remem_me/widgets/collection/collection.dart';

import '../../routes.dart';

/// Layout for displaying a collection in a list
class CollectionTile extends StatelessWidget {
  CollectionTile(this.collection);

  final Collection collection;

  @override
  Widget build(BuildContext ctx) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(ctx, '${AppRoutes.collection}/${collection.id}');
      },
      onLongPress: () {},
      child: _card(ctx),
    );
  }

  Widget _card(BuildContext ctx) {
    return Card(
      clipBehavior: Clip.antiAlias,
      child: CollectionContent(collection, collapsed: true),
    );
  }

  CachedNetworkImage _image() {
    return CachedNetworkImage(
      imageUrl: collection.image!,
    );
  }


}
