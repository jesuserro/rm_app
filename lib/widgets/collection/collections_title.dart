
import 'package:flutter/material.dart';
import 'package:message_core/message_core.dart';

class CollectionsTitle extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) => Text(L10n.of(ctx)!.t8('menu_collections')!);
}