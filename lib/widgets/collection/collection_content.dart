import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:intl/intl.dart'; //for date format
import 'package:message_core/message_core.dart';
import 'package:remem_me/models/collections/collections.dart';

import '../svg.dart';

/// Layout for displaying a collection in a list
class CollectionContent extends StatelessWidget {
  CollectionContent(this.collection, { this.collapsed = false });

  final Collection? collection;
  final bool collapsed;

  @override
  Widget build(BuildContext ctx) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ListTile(
          title: Text(collection!.name),
          subtitle: _topInfoRow(ctx),
        ),
        collection!.description.isNotEmpty
            ? Padding(
                padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
                child: Text(collection!.description,
                    maxLines: collapsed ? 3 : null,
                    overflow: collapsed ? TextOverflow.ellipsis : null,
                    style: TextStyle(
                        color: Theme.of(ctx).textTheme.caption!.color)))
            : SizedBox.shrink(),
        Divider(
          height: 0,
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: _bottomInfoRow(ctx),
        ),
        /*ButtonBar(
            alignment: MainAxisAlignment.start,
            children: [
              FlatButton(
                textColor: const Color(0xFF6200EE),
                onPressed: () {
                  // Perform some action
                },
                child: const Text('ACTION 1'),
              ),
            ],
          ),*/
      ],
    );
  }

  CachedNetworkImage _image() {
    return CachedNetworkImage(
      imageUrl: collection!.image!,
    );
  }

  Widget _topInfoRow(BuildContext ctx) {
    return Row(
      children: [
        _infoItem(ctx, Icons.menu_book_sharp,
            L10n.of(ctx)!.t8('verses_num', [collection!.size.toString()])!),
        SizedBox(width: 16.0),
        _infoItem(ctx, Icons.person_outline, collection!.publisher!.name!, flex: 2),
      ],
    );
  }

  Widget _bottomInfoRow(BuildContext ctx) {
    return Row(
      children: [
        _infoItem(
            ctx,
            Icons.calendar_today_rounded,
            DateFormat.yMMMd(L10n.of(ctx)!.locale.toString())
                .format(collection!.created!.toDateTimeUnspecified())),
        SizedBox(width: 16.0),
        _infoItem(ctx, Icons.language_rounded,
            LocaleNames.of(ctx)!.nameOf(collection!.publisher!.language!)!),
        // todo: flutter_localized_locales
        SizedBox(width: 16.0),
        _infoItem(
            ctx,
            Icons.download_rounded,
            L10n.of(ctx)!
                .t8('Collection.downloads', [collection!.downloads.toString()])!),
      ],
    );
  }

  Widget _infoItem(BuildContext ctx, IconData icon, String text, {int flex = 1}) {
    return Expanded(
      flex: flex,
        child:Row(
      children: [
        Icon(icon, size: 16.0, color: Theme.of(ctx).textTheme.caption!.color),
        SizedBox(width: 4.0),
        Expanded(child: Text(
          text,
          overflow: TextOverflow.ellipsis,
        ))
      ],
    ));
  }

  SvgAsset _placeholder() => SvgAsset('app_logo');
}
