import 'package:flutter/material.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/widgets/form/form.dart';
import 'package:remem_me/widgets/form/validator.dart';

typedef OnSubmitCallback = Function(Map<String, String> credentials);

class UserForm extends StatefulWidget {
  final Map<String, String?>? initialValues;
  final OnSubmitCallback onSubmit;
  final OnSubmitCallback? onForgotPassword;
  final String? titleL10nKey;
  final String? submitL10nKey;
  final bool hasRepeatPassword;
  final bool hasUsername;
  final bool hasEmailAsUsername;
  final Map<String, List<String?>>? errors;

  const UserForm(
      {Key? key,
      this.initialValues,
      required this.onSubmit,
      this.onForgotPassword,
      this.hasRepeatPassword = false,
      this.hasUsername = true,
      this.hasEmailAsUsername = true,
      this.titleL10nKey,
      this.submitL10nKey,
      this.errors = const {}})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _UserFormState();
  }
}

/// Form for credentials
class _UserFormState extends State<UserForm> {
  final _formKey = GlobalKey<FormState>();
  final _fieldKeyEmail = GlobalKey<FormFieldState>();
  final _fieldKeyPassword = GlobalKey<FormFieldState>();
  final _credentials = <String, String>{};

  static final _spacing = 16.0;

  @override
  Widget build(BuildContext ctx) {
    // final fields = [fieldUser(ctx), fieldPassword(ctx)];

    final fields = <Widget>[
      Text(L10n.of(ctx)!.t8(widget.titleL10nKey!)!,
          style: Theme.of(ctx).textTheme.headline6),
    ];

    if (widget.errors!['messages'] != null) {
      fields.addAll([SizedBox(height: _spacing), _errorMessages(ctx)]);
    }

    if (widget.hasUsername) {
      fields.addAll([
        SizedBox(height: _spacing),
        widget.hasEmailAsUsername ? _emailField(ctx) : _nameField(ctx),
      ]);
    }

    fields.addAll([
      SizedBox(height: _spacing),
      _passwordField(),
    ]);

    if (widget.hasRepeatPassword) {
      fields.addAll([
        SizedBox(height: _spacing),
        _repeatPasswordField(ctx),
      ]); // todo: validation
    }

    fields.addAll([
      Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            if (widget.onForgotPassword != null) _forgotPasswordButton(ctx),
            SizedBox(width: _spacing),
            _submitButton(ctx),
          ]),
    ]);

    return _form(ctx, fields);
  }

  _form(BuildContext ctx, List<Widget> fields) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: SingleChildScrollView(
          child: Form(
              key: _formKey,
              child: AutofillGroup(
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: fields,
              )))),
    );
  }

  Text _errorMessages(BuildContext ctx) {
    return Text(widget.errors!['messages']!.join('\n'),
        style: Theme.of(ctx)
            .textTheme
            .bodyText1!
            .copyWith(color: Theme.of(ctx).errorColor));
  }

  StringInput _emailField(BuildContext ctx) {
    return StringInput(
        fieldKey: _fieldKeyEmail,
        l10nKey: 'User.email',
        iconData: Icons.email_rounded,
        initialValue: widget.initialValues!['email'],
        autofillHints: [AutofillHints.email],
        validator: Validators.compose([
          Validators.error<String?>(
              widget.errors!['email'], widget.initialValues!['email']),
          Validators.email(ctx),
          Validators.required(ctx),
        ]),
        autovalidateMode: widget.initialValues!['email'] != null
            ? AutovalidateMode.always
            : AutovalidateMode.disabled,
        onSaved: (value) => _credentials['email'] = value!.trim());
  }

  StringInput _nameField(BuildContext ctx) {
    return StringInput(
        l10nKey: 'User.name',
        iconData: Icons.folder_shared_rounded,
        initialValue: widget.initialValues!['name'],
        autofillHints: [AutofillHints.username],
        validator: Validators.error<String?>(
            widget.errors!['name'], widget.initialValues!['name']),
        autovalidateMode: widget.initialValues!['name'] != null
            ? AutovalidateMode.always
            : AutovalidateMode.disabled,
        onSaved: (value) => _credentials['name'] = value!.trim());
  }

  StringInput _repeatPasswordField(BuildContext ctx) {
    return StringInput(
        l10nKey: 'User.passwordRepeat',
        iconData: Icons.replay_rounded,
        obscureText: true,
        validator: Validators.equal(
          ctx,
          _fieldKeyPassword,
          'User.password',
        ),
        onSaved: (_) {});
  }

  StringInput _passwordField() {
    return StringInput(
        fieldKey: _fieldKeyPassword,
        l10nKey: 'User.password',
        iconData: Icons.security_rounded,
        initialValue: widget.initialValues!['password'],
        autofillHints: [AutofillHints.password],
        obscureText: true,
        validator: Validators.compose([
          Validators.error<String?>(
              widget.errors!['password'], widget.initialValues!['password']),
          // Validators.minLength(ctx, 4),
        ]),
        autovalidateMode: widget.initialValues!['password'] != null
            ? AutovalidateMode.always
            : AutovalidateMode.disabled,
        onSaved: (value) => _credentials['password'] = value?.trim() ?? '');
  }

  TextButton _submitButton(BuildContext ctx) {
    return TextButton(
        style: TextButton.styleFrom(padding: EdgeInsets.all(16.0)),
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            _formKey.currentState!.save();
            widget.onSubmit(_credentials);
          }
        },
        child: Text(L10n.of(ctx)!.t8(widget.submitL10nKey!)!.toUpperCase()));
  }

  TextButton _forgotPasswordButton(BuildContext ctx) {
    return TextButton(
        style: TextButton.styleFrom(
            primary: Theme.of(ctx).textTheme.caption!.color,
            padding: EdgeInsets.all(16.0)),
        onPressed: () {
          if (_fieldKeyEmail.currentState!.validate()) {
            _formKey.currentState!.save();
            widget.onForgotPassword!(_credentials);
          }
        },
        child: Text(L10n.of(ctx)!.t8('User.forgotPassword')!.toUpperCase()));
  }
}
