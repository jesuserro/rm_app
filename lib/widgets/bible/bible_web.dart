import 'package:easy_web_view/easy_web_view.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:remem_me/models/bibles/bibles.dart';
import 'package:remem_me/services/bible_query_service.dart';
import 'package:remem_me/theme.dart';
import 'package:remem_me/utils/color.dart';

class BibleWeb extends StatefulWidget {
  final BibleQuery? query;

  const BibleWeb(this.query, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _BibleWebState();
  }
}

class _BibleWebState extends State<BibleWeb> {
  String? clipboardText;

  @override
  void initState() {
    super.initState();
    BibleQueryService(); // todo: get query params
    // query = BibleQuery('ESV', 66, 1, 1, 2);
    widget.query!.get().then((value) async {
      await Clipboard.setData(ClipboardData(text: value));
      setState(() {
        clipboardText = value;
      });
    });
  }

  @override
  Widget build(BuildContext ctx) {
    return Dialog(
        child: Scaffold(
      body: EasyWebView(
        src: widget.query!.uri.toString(), onLoaded: () {  },
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
            height: 48.0,
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            alignment: Alignment.centerLeft,
            child: Text(widget.query!.version.site!.name!,
                style: AppTheme.dark.textTheme.headline6!.copyWith(
                  fontSize: 20.0,
                  fontWeight: FontWeight.w600,
                  color: Flat.amber,
                ))),
        color: Colors.black87,
      ),
      extendBody: !kIsWeb,  // todo: set to true when iframe absorbing pointer is fixed
      floatingActionButton: FloatingActionButton(
        child: clipboardText == null
                ? Padding(
                    padding: EdgeInsets.all(16.0),
                    child: CircularProgressIndicator(color: Colors.white))
                : Icon(Icons.paste_rounded),
        backgroundColor: Theme.of(ctx).colorScheme.primary,
        onPressed: () async {
          print('Paste pressed');
          final data = await Clipboard.getData(Clipboard.kTextPlain);
          Navigator.of(ctx).pop(data != null ? data.text : clipboardText);
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endDocked,
    ));
  }
}
