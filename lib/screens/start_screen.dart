import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/routes.dart';
import 'package:remem_me/utils/build_context.dart';
import 'package:remem_me/widgets/loading_indicator.dart';
import 'package:remem_me/widgets/start/basic_info.dart';
import 'package:remem_me/widgets/user/user_form.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository_http/repository_http.dart';

class StartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return BlocConsumer<AuthBloc, AuthState>(
      listener: (ctx, state) {
        if (state is LoginSuccess) {
          Navigator.of(ctx).pushReplacementNamed(AppRoutes.home);
        } else if (state is LoginFailure || state is ForgotPasswordFailure) {
          login(ctx, state);
        } else if (state is RegisterFailure) {
          register(ctx, state);
        }
      },
      builder: (ctx, state) {
        return Scaffold(
            appBar: AppBar(
              title: Text(L10n.of(ctx)!.t8('Start.title')!),
              actions: [
                IconButton(
                  onPressed: launchDocumentation(ctx),
                  icon: Icon(Icons.info_outline_rounded),
                ),
                // Setting(),
              ],
            ),
            body: Stack(children: [
              Column(children: [
                Padding(
                    padding: EdgeInsets.all(4.0), child: Text('BETA-VERSION')),
                Expanded(
                    child: BasicInfo(
                        authState: state,
                        onLogin: () => login(ctx, state),
                        onRegister: () => register(ctx, state))),
              ]),
              if(state is AuthInProgress) LoadingIndicator(),
            ]));
      },
    );
  }

  Future<void> login(BuildContext ctx, AuthState state) {
    return showDialog<void>(
        context: ctx,
        builder: (BuildContext context) {
          return Dialog(
              child: UserForm(
            errors: state is LoginFailure || state is ForgotPasswordFailure
                ? (state as AuthMessageState).messages
                : {},
            initialValues: state is AuthMessageState
                ? {
                    'email': state.credentials['email'],
                    'password': state.credentials['password']
                  }
                : const {},
            hasRepeatPassword: false,
            titleL10nKey: 'User.login.title',
            submitL10nKey: 'User.login.submit',
            onSubmit: (credentials) {
              BlocProvider.of<AuthBloc>(ctx).add(LoggedIn(
                  email: credentials['email']!,
                  password: credentials['password']!));
              Navigator.of(ctx).pop();
            },
            onForgotPassword: (credentials) {
              BlocProvider.of<AuthBloc>(ctx)
                  .add(PasswordForgotten(email: credentials['email']!));
              Navigator.of(ctx).pop();
            },
          ));
        });
  }

  Future<void> register(BuildContext ctx, AuthState state) {
    return showDialog<void>(
      context: ctx,
      builder: (BuildContext context) {
        return Dialog(
            child: UserForm(
          errors: state is RegisterFailure ? state.messages : {},
          initialValues: state is AuthMessageState
              ? {
                  'email': state.credentials['email'],
                  'password': state.credentials['password']
                }
              : const {},
          hasRepeatPassword: true,
          titleL10nKey: 'User.register.title',
          submitL10nKey: 'User.register.submit',
          onSubmit: (credentials) {
            BlocProvider.of<AuthBloc>(ctx).add(Registered(
                email: credentials['email']!,
                password: credentials['password']!));
            Navigator.of(ctx).pop();
          },
        ));
      },
    );
  }

  Future<void> resetPassword(BuildContext ctx, AuthState state) {
    return showDialog<void>(
      context: ctx,
      builder: (BuildContext context) {
        return Dialog(
            child: UserForm(
          errors: state is ForgotPasswordFailure ? state.messages : {},
          initialValues: state is AuthMessageState
              ? {
                  'email': state.credentials['email'],
                  'password': state.credentials['password']
                }
              : const {},
          hasRepeatPassword: true,
          titleL10nKey: 'User.register.title',
          submitL10nKey: 'User.register.submit',
          onSubmit: (credentials) {
            BlocProvider.of<AuthBloc>(ctx).add(Registered(
                email: credentials['email']!,
                password: credentials['password']!));
            Navigator.of(ctx).pop();
          },
        ));
      },
    );
  }
}
