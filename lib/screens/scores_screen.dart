import 'package:flutter/material.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/widgets/widgets.dart';


class ScoresScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

        return Scaffold(
          appBar: AppBar(
            title: Text(L10n.of(context)!.t8('scores')!),
          ),
          body: Scores(),
        );
  }
}