import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/study/study.dart';
import 'package:remem_me/widgets/study/study.dart';

import '../theme.dart';

class StudyScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StudyBloc, StudyState>(
        builder: (BuildContext ctx, StudyState state) {
      return Scaffold(
        appBar: AppBar(
          leading: ([Obfuscation, Puzzle, LineUp, Typing]
                  .contains(state.runtimeType))
              ? IconButton(
                  icon: Icon(Icons.arrow_back_rounded),
                  onPressed: () =>
                      BlocProvider.of<StudyBloc>(ctx).add(StudyStarted()),
                )
              : null,
          title: Text(state is Study
              ? state.verse!.reference!
              : L10n.of(ctx)!.t8('Study.title')!),
          elevation: 4.0,
        ),
        body: Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
          Expanded(child: TextPane()),
          Theme(
            data: AppTheme.dark,
            child: BottomAppBar(
              child: _control(ctx, state),
              //color: Colors.blueGrey[800],
              elevation: 4.0,
            ),
          )
        ]),
        resizeToAvoidBottomInset: true,
      );
    });
  }

  _control(BuildContext ctx, StudyState state) {
    switch (state.runtimeType) {
      case Obfuscation:
        return ObfuscationControl();
      case Puzzle:
        return PuzzleControl();
      case LineUp:
        return LineUpControl();
      case Typing:
        return TypingControl();
      default:
        return StudyControl();
    }
  }
}
