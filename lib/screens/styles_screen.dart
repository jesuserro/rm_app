import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';

class StylesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Styles'),
      ),
      body: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              FutureBuilder(
                future: PackageInfo.fromPlatform(),
                builder: (BuildContext context, AsyncSnapshot<PackageInfo> snapshot) {
                  return Text('RememberMe version ' + (snapshot.data?.version ?? ''));
              },),
              // Text('head1', style: Theme.of(context).textTheme.headline1),
              Text('head2', style: Theme.of(context).textTheme.headline2),
              Text('head3', style: Theme.of(context).textTheme.headline3),
              Text('head4', style: Theme.of(context).textTheme.headline4),
              Text('head5', style: Theme.of(context).textTheme.headline5),
              Text('head6', style: Theme.of(context).textTheme.headline6),
              Text('subtitle1', style: Theme.of(context).textTheme.subtitle1),
              Text('subtitle2', style: Theme.of(context).textTheme.subtitle2),
              Text('bodyText1', style: Theme.of(context).textTheme.bodyText1),
              Text('bodyText2', style: Theme.of(context).textTheme.bodyText2),
              Text('caption', style: Theme.of(context).textTheme.caption),
              Text('overline', style: Theme.of(context).textTheme.overline),
              Text('button', style: Theme.of(context).textTheme.button),
              Text('colorScheme.secondary',
                  style: Theme.of(context)
                      .textTheme
                      .button!
                      .copyWith(color: Theme.of(context).colorScheme.secondary)),
              TextButton(onPressed: () {}, child: Text('TextButton')),
              ElevatedButton(onPressed: () {}, child: Text('ElevatedButton'))
            ],
          )),
    );
  }
}
