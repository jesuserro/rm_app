import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/utils/build_context.dart';
import 'package:remem_me/widgets/collection/collection.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository_core/repository_core.dart';

import '../routes.dart';

typedef OnImportCallback = Function(List<VerseEntity> verses);

class CollectionDetailScreen extends StatelessWidget {
  final OnImportCallback onImport;

  const CollectionDetailScreen({Key? key, required this.onImport})
      : super(key: key);

  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<CollectionDetailBloc, CollectionDetailState>(
        builder: (BuildContext ctx, CollectionDetailState state) {
      return state is CollectionDetailLoadSuccess ? Scaffold(
          appBar: AppBar(
            title: Text(state.collection.name),
            actions: [
              IconButton(
                  icon: Icon(Icons.download_sharp),
                  onPressed: () {
                    _import(ctx, state);
                  })
            ],
          ),
          body: LayoutBuilder(
              builder: (BuildContext ctx, BoxConstraints constraints) {
            return Column(children: [
              Container(
                  constraints:
                      BoxConstraints(maxHeight: constraints.maxHeight / 2),
                  child: Card(
                      child: Scrollbar(
                          child: SingleChildScrollView(
                              child: CollectionContent(state.collection))))),
              Expanded(
                  child: VersesList(
                      verses: state.collection.verses,
                      createTile: (verse) => DefaultVerseTile(verse)))
            ]);
          })) : LoadingIndicator();
    });
  }

  void _import(BuildContext ctx, CollectionDetailLoadSuccess collectionState) {
    final account = currentAccount(ctx);
    if (account != null) {
      final tag = TagEntity(collectionState.collection.label, account: account.id);
      BlocProvider.of<ProgressBloc>(ctx)
          .stream
          .where(
              (state) => state.id == tag.id && state.progress == Progress.STOP)
          .listen((state) {
        BlocProvider.of<VersesBloc>(ctx)
            .add(EntitiesAdded(collectionState.collection.verses
                .map((verse) => verse.copyWith(
                      account: account.id,
                      tags: {tag.id: tag.text},
                    ))
                .toList()));
        Navigator.of(ctx).popUntil(ModalRoute.withName(AppRoutes.home));
      });
      BlocProvider.of<ProgressBloc>(ctx).add(ProgressStarted(tag.id));
      BlocProvider.of<TagsBloc>(ctx).add(EntitiesAdded<TagEntity>([tag]));
    }
  }
}
