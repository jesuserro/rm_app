import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/widgets/widgets.dart';

import '../routes.dart';

// This is the type used by the popup menu.
enum FlashcardMenuAction { edit, study, share }

/// Navigation endpoint for flashcards
/// Listens for the current verse (which keeps changing during a reviewing process)
/// and creates a new flashcard screen for it
class FlashcardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<FlashcardBloc, FlashcardState>(
        builder: (BuildContext ctx, FlashcardState state) => Scaffold(
              appBar: AppBar(
                title: Text(_titleText(ctx, state)),
                actions: _menu(ctx, state),
              ),
              body: FlashcardController(),
            ));
  }

  _titleText(BuildContext ctx, FlashcardState state) {
    print('_titleText of ' + state.toString());
    if (state is FlashcardRun) {
      if (state.reverse ||
          state.box == Box.KNOWN && state is SingleFlashcardRun) {
        return state.verse.reference;
      } else {
        if (state.inverse) {
          return (L10n.of(ctx)!.locale.languageCode == 'es')
              ? '¿'
              : '' + L10n.of(ctx)!.t8('reference')! + '?';
        } else {
          return (L10n.of(ctx)!.locale.languageCode == 'es')
              ? '¿'
              : '' + L10n.of(ctx)!.t8('passage')! + '?';
        }
      }
    } else {
      return '';
    }
  }

  /// Creates a menu for actions related to the verse
  List<Widget> _menu(BuildContext ctx, FlashcardState state) {
    return <Widget>[
      IconButton(
        icon: Icon(Icons.edit_rounded),
        onPressed: () => _do(ctx, state, FlashcardMenuAction.edit),
      ),
      IconButton(
        icon: Icon(Icons.school_rounded),
        onPressed: () => _do(ctx, state, FlashcardMenuAction.study),
      ),
      /*PopupMenuButton<FlashcardMenuAction>(
        onSelected: (FlashcardMenuAction action) => _do(ctx, state, action),
        itemBuilder: (BuildContext context) =>
            <PopupMenuEntry<FlashcardMenuAction>>[
          PopupMenuItem<FlashcardMenuAction>(
            value: FlashcardMenuAction.share,
            child: Text(L10n.of(ctx)!.t8('menu_share')!),
          ),
        ],
      )*/
    ];
  }

  /// Perfoms actions initiated from the menu
  _do(BuildContext ctx, FlashcardState state,
      FlashcardMenuAction action) async {
    switch (action) {
      case FlashcardMenuAction.edit:
        Verse? verse = await Navigator.pushNamed(ctx, AppRoutes.editVerse,
            arguments: (state as FlashcardRun).verse);
        if (verse != null) {
          BlocProvider.of<FlashcardBloc>(ctx).add(FlashcardVerseUpdated(verse));
        }
        break;

      case FlashcardMenuAction.study:
        Navigator.of(ctx).pushNamed(AppRoutes.study,
            arguments: (state as FlashcardRun).verse);
        break;

      case FlashcardMenuAction.share:
        //Navigator.pop(ctx);
        break;
    }
  }
}
