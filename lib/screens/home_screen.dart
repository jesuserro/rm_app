import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/text_to_speech/tts.dart';
import 'package:remem_me/routes.dart';
import 'package:remem_me/services/legacy_service.dart';
import 'package:remem_me/utils/build_context.dart';
import 'package:remem_me/utils/color.dart';
import 'package:remem_me/widgets/search/search.dart';
import 'package:remem_me/widgets/user/user.dart';
import 'package:remem_me/widgets/widgets.dart';
import 'package:repository_core/repository_core.dart';
import 'package:repository_http/repository_http.dart';

final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return DefaultTabController(
      length: 3,
      child: LayoutBuilder(
        builder: (context, constraints) {
          if (constraints.maxWidth <= 960) {
            return _scaffoldSmall(ctx);
          } else if (constraints.maxWidth <= 1280) {
            return _scaffoldMedium(ctx);
          } else {
            return _scaffoldLarge(ctx);
          }
        },
      ),
    );
  }

  Scaffold _scaffoldSmall(BuildContext ctx) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: _appBar(ctx),
      body: _tabView(ctx),
      floatingActionButton: _fabRow(ctx),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      drawer: _navigationDrawer(ctx, true),
      endDrawer: NavigationTags(),
    );
  }

  Widget _scaffoldMedium(BuildContext ctx) {
    return Row(children: [
      Expanded(
        flex: 1,
        child: _navigationDrawer(ctx, false),
      ),
      Expanded(
          flex: 3,
          child: Scaffold(
            key: _scaffoldKey,
            appBar: _appBar(ctx),
            body: Container(
                decoration: BoxDecoration(
                  border: Border(
                    left: BorderSide(width: 1.0, color: AppColor.dark),
                  ),
                ),
                child: _tabView(ctx)),
            floatingActionButton: _fabRow(ctx),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
            endDrawer: NavigationTags(),
          ))
    ]);
  }

  Widget _scaffoldLarge(BuildContext ctx) {
    return Row(children: [
      Expanded(flex: 1, child: _navigationDrawer(ctx, false)),
      Expanded(
          flex: 3,
          child: Scaffold(
            key: _scaffoldKey,
            appBar: _appBar(ctx, hasEndDrawer: false),
            body: Container(
                decoration: BoxDecoration(
                  border: Border.symmetric(
                    vertical: BorderSide(width: 1.0, color: AppColor.dark),
                  ),
                ),
                child: _tabView(ctx)),
            floatingActionButton: _fabRow(ctx),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
            endDrawer: NavigationTags(),
          )),
      Expanded(flex: 1, child: NavigationTags(popping: false)),
    ]);
  }

  Widget _navigationDrawer(BuildContext ctx, bool popping) {
    return NavigationDrawer(
      onAction: (action, {payload}) => _do(ctx, action, payload),
      popping: popping,
    );
  }

  TabBarView _tabView(BuildContext ctx) {
    return TabBarView(children: [
      BlocProvider<OrderedVersesBloc>.value(
        value: BlocProvider.of<OrderedVersesNewBloc>(ctx),
        child: OrderedVerses(),
      ),
      BlocProvider<OrderedVersesBloc>.value(
        value: BlocProvider.of<OrderedVersesDueBloc>(ctx),
        child: OrderedVerses(),
      ),
      BlocProvider<OrderedVersesBloc>.value(
        value: BlocProvider.of<OrderedVersesKnownBloc>(ctx),
        child: OrderedVerses(),
      ),
    ]);
  }

  PreferredSizeWidget _appBar(BuildContext ctx, {bool hasEndDrawer = true}) {
    return StackedBar(
        base: _homeBar(ctx, hasEndDrawer),
        overlay: BlocBuilder<SelectionBloc, SelectionState>(
            builder: (context, state) {
          return state.isSearchActive
              ? SearchBar()
              : state.selection.length > 0
                  ? SelectionBar()
                  : SizedBox.shrink();
        }));
  }

  PreferredSizeWidget _homeBar(BuildContext ctx, bool hasEndDrawer) {
    return AppBar(
      title: HomeTitle(),
      actions: [
        IconButton(
          icon: Icon(Icons.search_rounded),
          onPressed: () {
            BlocProvider.of<SelectionBloc>(ctx).add(SearchActivated());
          },
        ),
        if (hasEndDrawer)
          IconButton(
            icon: Icon(Icons.label_outline_rounded),
            onPressed: () => _scaffoldKey.currentState!.openEndDrawer(),
          ),
      ],
      bottom: BoxTabBar(),
    );
  }

  Widget _fabRow(BuildContext ctx) {
    return BlocBuilder<AccountsBloc, EntitiesState<AccountEntity>>(builder:
        (BuildContext ctx, EntitiesState<AccountEntity> accountsState) {
      return Visibility(
          visible: accountsState is AccountsLoadSuccess,
          child: BlocBuilder<TtsBloc, TtsState>(
              builder: (BuildContext ctx, TtsState ttsState) {
            return ttsState is TtsOn ? TtsFabRow() : BoxFabRow();
          }));
    });
  }

  void _do(BuildContext ctx, HomeMenuAction action, dynamic payload) {
    switch (action) {
      case HomeMenuAction.attachAccount:
        _attachAccount(ctx);
        break;
      case HomeMenuAction.login:
        // TODO: Handle this case.
        break;
      case HomeMenuAction.logout:
        BlocProvider.of<AuthBloc>(ctx).add(LoggedOut());
        Navigator.of(ctx).pushReplacementNamed(AppRoutes.start);
        break;
      case HomeMenuAction.addAccount:
        Navigator.pushNamed(ctx, AppRoutes.addAccount);
        break;
      case HomeMenuAction.editAccount:
        Navigator.pushNamed(ctx, AppRoutes.editAccount,
            arguments: currentAccount(ctx));
        break;
      case HomeMenuAction.accountBin:
        Navigator.pushNamed(ctx, AppRoutes.accountBin);
        break;
    }
  }

  Future<void> _attachAccount(BuildContext ctx) async {
    Map<String, String> initialCredentials = const {};
    Map<String, List<String>>? errors = const {};
    while (errors != null) {
      errors = await (showDialog<Map<String, List<String>>>(
          barrierDismissible: false,
          context: ctx,
          builder: (BuildContext context) => Dialog(
                  child: UserForm(
                initialValues: initialCredentials,
                errors: errors,
                hasRepeatPassword: false,
                hasEmailAsUsername: false,
                titleL10nKey: 'User.login.title',
                submitL10nKey: 'User.login.submit',
                onSubmit: (Map<String, String> credentials) async {
                  initialCredentials = credentials;
                  try {
                    final account = await LegacyService().attachAccount(
                        name: credentials['name'],
                        password: credentials['password']);
                    BlocProvider.of<AccountsBloc>(ctx).add(
                        // todo: Looking up a deactivated widget's ancestor is unsafe.
                        EntitiesAdded<AccountEntity>([account],
                            persist: false));
                    Navigator.of(ctx).pop();
                  } on MessageException catch (e) {
                    Navigator.of(ctx).pop(e.messages);
                  }
                },
              ))));
    }
  }
}
