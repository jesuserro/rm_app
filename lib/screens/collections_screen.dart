import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/widgets/collection/collection.dart';
import 'package:remem_me/widgets/search/search.dart';

class CollectionsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return BlocBuilder<CollectionsBloc, CollectionsState>(
        builder: (BuildContext ctx, CollectionsState state) {
      return Scaffold(
        appBar: _appBar(ctx, state),
        body: CollectionsList(),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      );
    });
  }

  PreferredSizeWidget _appBar(BuildContext ctx, CollectionsState state) {
    return StackedBar(
      base: _titleBar(ctx, state),
      overlay: state.isSearchVisible ? GlobalSearch() : SizedBox.shrink(),
    );
  }

  PreferredSizeWidget _titleBar(BuildContext ctx, CollectionsState state) {
    return AppBar(
      title: CollectionsTitle(),
      actions: [
        IconButton(
          icon: Icon(Icons.search_rounded),
          onPressed: () {
            BlocProvider.of<CollectionsBloc>(ctx)
                .add(SearchDisplayed(true));
          },
        )
      ],
      bottom: QueryBar(),
    );
  }
}
