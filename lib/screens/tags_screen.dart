import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/entities/entities.dart';
import 'package:remem_me/blocs/verses/verses_event.dart';
import 'package:remem_me/dialogs/dialogs.dart';
import 'package:remem_me/utils/build_context.dart';
import 'package:remem_me/widgets/empty_list.dart';
import 'package:remem_me/widgets/loading_indicator.dart';
import 'package:repository_core/repository_core.dart';

import '../routes.dart';

/// Navigation endpoint for tag management
/// Creates a TagScreen
/// Screen for viewing and managing a list of tags
class TagsScreen extends StatelessWidget {
  final icon = Icons.label_rounded;

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
        appBar: AppBar(title: Text(L10n.of(ctx)!.t8('menu_sets')!), actions: [
          IconButton(
            icon: Icon(Icons.restore_from_trash_rounded),
            onPressed: () => Navigator.pushNamed(ctx, AppRoutes.tagBin,
                arguments: currentAccount(ctx)))
        ]),
        body: Container(margin: EdgeInsets.all(16.0), child: content(ctx)),
        floatingActionButton: _btnAddTag(ctx),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat);
  }

  Widget content(BuildContext ctx) {
    return BlocBuilder<TagsBloc, EntitiesState>(
      builder: (BuildContext ctx, EntitiesState state) {
        if (state is EntitiesLoadSuccess) {
          return Stack(children: [
            _listWidget(ctx, state.entities as List<TagEntity>),
            if (state.isUpdating) LoadingIndicator(),
          ]);
        } else {
          return LoadingIndicator();
        }
      },
    );
  }

  Widget _listWidget(BuildContext ctx, List<TagEntity> tags) {
    return tags.length > 0
        ? ListView.builder(
            itemCount: tags.length,
            itemBuilder: (ctx, itemPosition) {
              TagEntity tag = tags[itemPosition];
              return tagItem(ctx, tag);
            },
          )
        : EmptyList(icon: icon);
  }

  Widget tagItem(BuildContext ctx, TagEntity tag) {
    return ListTile(
      leading: _btnEditTag(ctx, tag),
      title: Text(tag.text),
      trailing: _btnDeleteTag(ctx, tag),
    );
  }

  IconButton _btnDeleteTag(BuildContext ctx, TagEntity tag) {
    return IconButton(
      icon: Icon(Icons.delete_rounded),
      onPressed: () {
        BlocProvider.of<TagsBloc>(ctx)
            .add(EntitiesUpdated<TagEntity>([tag.copyWith(deleted: true)]));
        BlocProvider.of<VersesBloc>(ctx).add(TagDeleted(tag.id));
      },
    );
  }

  Widget _btnEditTag(BuildContext ctx, TagEntity tag) {
    return IconButton(
      icon: Icon(Icons.edit_rounded),
      onPressed: () async {
        await showDialog<TagEntity>(
            context: ctx,
            builder: (BuildContext context) => TagEditDialog(
                  tag: tag,
                  onSave: (TagEntity tag) async {
                    await RepositoryProvider.of<TagsRepository>(ctx)
                        .update(tag);
                    BlocProvider.of<TagsBloc>(ctx)
                        .add(EntitiesUpdated<TagEntity>([tag], persist: false));
                  },
                ));
      },
    );
  }

  Widget _btnAddTag(BuildContext ctx) {
    return FloatingActionButton(
      onPressed: () async {
        await showDialog<TagEntity>(
            context: ctx,
            builder: (BuildContext context) => TagEditDialog(
                  onSave: (TagEntity tag) async {
                    await RepositoryProvider.of<TagsRepository>(ctx)
                        .create(tag);
                    BlocProvider.of<TagsBloc>(ctx)
                        .add(EntitiesAdded<TagEntity>([tag], persist: false));
                  },
                ));
      },
      backgroundColor: Theme.of(ctx).colorScheme.secondary,
      child: Icon(
        Icons.add_rounded,
      ),
    );
  }
}
