import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/accounts/accounts.dart';
import 'package:remem_me/models/bibles/bibles.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/services/bible_query_service.dart';
import 'package:remem_me/theme.dart';
import 'package:remem_me/widgets/bible/bible.dart';
import 'package:remem_me/widgets/form/form.dart';
import 'package:remem_me/widgets/loading_indicator.dart';
import 'package:repository_hive/repository_hive.dart';

import '../widgets/form/entity_form.dart';

class EditVerseScreen extends EntityForm<Verse> {
  const EditVerseScreen({required OnSaveCallback<Verse> onSave, Verse? verse})
      : super(onSave: onSave, entity: verse);

  @override
  _EditVerseScreenState createState() => _EditVerseScreenState();
}

class _EditVerseScreenState extends EntityFormState<Verse, EditVerseScreen> {
  static final _referenceKey = GlobalKey<FormFieldState>();
  static final _sourceKey = GlobalKey<FormFieldState>();
  static final _passageKey = GlobalKey<FormFieldState>();

  String? _reference;
  String? _passage;
  String? _topic;
  String? _source;
  String? _image;

  BibleQuery? _query;
  Account? _currentAccount;
  var _includeNumbers = false;

  @override
  void initState() {
    super.initState();
    _currentAccount =
        (BlocProvider.of<AccountsBloc>(context).state as AccountsLoadSuccess)
            .currentAccount;
    Settings().loadBool(Settings.INCLUDE_NUMBERS).then(
        (value) => setState(() => _includeNumbers = value ?? _includeNumbers));
    BibleQueryService()
        .init(_currentAccount!.language)
        .then((_) => _createQuery());
  }

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
        appBar: AppBar(
            title: _query == null
                ? Text(
                    widget.entity?.reference ?? L10n.of(ctx)!.t8('new_verses')!)
                : null,
            leading: IconButton(
              icon: Icon(Icons.save_rounded),
              onPressed: saveForm,
            ),
            actions: _query != null
                ? [
                    Icon(Icons.format_list_numbered_rounded),
                    Switch(
                        value: _includeNumbers,
                        activeColor: AppTheme.appBar.colorScheme.secondary,
                        inactiveThumbColor:
                            AppTheme.light.colorScheme.background,
                        onChanged: (value) async {
                          await Settings()
                              .saveBool(Settings.INCLUDE_NUMBERS, value);
                          setState(() => _includeNumbers = value);
                        }),
                    SizedBox(width: 96)
                  ]
                : []),
        body: Stack(children: [
          Padding(
              padding: EdgeInsets.all(16.0),
              child: Form(
                  key: formKey,
                  child: ListView(children: fields(ctx)),
                  onChanged: _createQuery)),
          if (isUpdating) LoadingIndicator(),
        ]),
        floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
        floatingActionButton: _query != null
            ? FloatingActionButton(
                backgroundColor: AppTheme.appBar.colorScheme.secondaryVariant,
                onPressed: _retrievePassage,
                child: Icon(Icons.menu_book_rounded))
            : null);
  }

  void _retrievePassage() async {
    final result = await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return BibleWeb(_query);
        });
    if (result != null) _passageKey.currentState!.didChange(result);
  }

  @override
  Verse updatedEntity() {
    return widget.entity!.copyWith(
        reference: _reference,
        passage: _passage,
        topic: _topic,
        source: _source,
        image: _image,
        nullValues: [
          if (_topic == null) 'topic',
          if (_source == null) 'source',
          if (_image == null) 'image',
        ]);
  }

  @override
  Verse createdEntity() {
    return Verse(_reference, _passage,
        topic: _topic,
        source: _source,
        image: _image,
        account: _currentAccount!.id);
  }

  List<Widget> fields(BuildContext ctx) {
    return super.fields(ctx)
      ..addAll([
        StringInput(
            fieldKey: _referenceKey,
            l10nKey: 'reference',
            initialValue: widget.entity?.reference,
            autofocus: widget.entity == null,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: Validators.compose([
              Validators.error<String?>(errors['reference'], _reference),
              Validators.required<String?>(ctx)
            ]),
            onSaved: (value) => _reference = value),
        SizedBox(height: spacing),
        Row(children: [
          Expanded(
              child: StringInput(
                  l10nKey: 'topic',
                  initialValue: widget.entity?.topic,
                  onSaved: (value) => _topic = value)),
          SizedBox(width: spacing),
          SizedBox(
              width: 128,
              child: StringInput(
                  fieldKey: _sourceKey,
                  l10nKey: 'source',
                  initialValue: widget.entity?.source,
                  optionBuilder: _sourceOptionBuilder,
                  onSaved: (value) => _source = value)),
        ]),
        SizedBox(height: spacing),
        StringInput(
            fieldKey: _passageKey,
            maxLines: 20,
            l10nKey: 'passage',
            initialValue: widget.entity?.passage,
            validator: Validators.required<String?>(ctx),
            onSaved: (value) => _passage = value),
        SizedBox(height: spacing),
        StringInput(
            l10nKey: 'Verse.image',
            initialValue: widget.entity?.image,
            validator: Validators.url(ctx),
            onSaved: (value) => _image = value),
      ]);
  }

  void _createQuery() {
    if (_referenceKey.currentState!.value != null &&
        _sourceKey.currentState!.value != null) {
      setState(() {
        _query = BibleQueryService().bibleQuery(_sourceKey.currentState!.value,
            _referenceKey.currentState!.value, _currentAccount!.books!);
      });
    }
  }

  OptionBuilder _sourceOptionBuilder = (BuildContext context) {
    if (BibleQueryService().versions != null) {
      return BibleQueryService()
          .versions!
          .entries
          .map((entry) => PopupMenuItem<String>(
                value: entry.key,
                child: Text(entry.value.name!),
              ))
          .toList();
    } else {
      return [];
    }
  };
}
