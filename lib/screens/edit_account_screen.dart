import 'package:flutter/material.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/widgets/loading_indicator.dart';
import 'package:repository_core/repository_core.dart';

import '../widgets/form/entity_form.dart';
import '../widgets/form/form.dart';

class EditAccountScreen extends EntityForm<Account> {
  const EditAccountScreen(
      {required OnSaveCallback<Account> onSave,
      OnSaveCallback<Account>? onDelete,
      Account? account})
      : super(onSave: onSave, onDelete: onDelete, entity: account);

  @override
  _EditAccountScreenState createState() => _EditAccountScreenState();
}

class _EditAccountScreenState
    extends EntityFormState<Account, EditAccountScreen> {
  String? _name;
  String? _language;
  String? _langRef;
  double? _reviewFrequency;
  int? _reviewLimit;
  int? _dailyGoal;
  String? _canon;

  Map<String?, String?>? _languageMap;
  Map<double, String?>? _frequencyMap;
  Map<String, String?>? _canonMap;

  @override
  void didChangeDependencies() {
    _options(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.entity?.name ?? L10n.of(ctx)!.t8('menu_account')!),
          leading:
              IconButton(icon: Icon(Icons.save_rounded), onPressed: saveForm),
          actions: [
            if (widget.onDelete != null)
              IconButton(icon: Icon(Icons.delete), onPressed: deleteEntity),
          ],
        ),
        body: Stack(children: [
          Padding(
              padding: EdgeInsets.all(16.0),
              child: Form(
                  key: formKey,
                  child: ListView(
                    children: fields(ctx),
                  ))),
          if (isUpdating) LoadingIndicator(),
        ]));
  }

  @override
  Account updatedEntity() {
    return widget.entity!.copyWith(
        name: _name,
        language: _language,
        langRef: _langRef,
        reviewFrequency: _reviewFrequency,
        reviewLimit: _reviewLimit,
        dailyGoal: _dailyGoal,
        canon: _canon,
        nullValues: [
          if (_langRef == null) 'langRef',
          if (_reviewFrequency == null) 'reviewFrequency',
          if (_reviewLimit == null) 'reviewLimit',
          if (_dailyGoal == null) 'dailyGoal',
        ]);
  }

  @override
  Account createdEntity() {
    return Account(_name,
        language: _language,
        langRef: _langRef,
        reviewFrequency: _reviewFrequency,
        reviewLimit: _reviewLimit,
        dailyGoal: _dailyGoal,
        orderNew: VerseOrder.CANON,
        orderDue: VerseOrder.LEVEL,
        orderKnown: VerseOrder.DATE,
        orderAll: VerseOrder.CANON,
        canon: _canon);
  }

  List<Widget> fields(BuildContext ctx) {
    return super.fields(ctx)
      ..addAll([
        StringInput(
            l10nKey: 'Account.name',
            iconData: Icons.folder_shared_rounded,
            initialValue: widget.entity?.name,
            autofocus: widget.entity == null,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: Validators.compose([
              Validators.error<String?>(errors['name'], _name),
              Validators.required<String?>(ctx)
            ]),
            onSaved: (value) => _name = value),
        SizedBox(height: spacing),
        DropdownInput<String?>(
            l10nKey: 'Account.language',
            iconData: Icons.language_rounded,
            items: _languageMap,
            initialValue: widget.entity?.language ??
                Localizations.localeOf(ctx).toString(),
            // eg. 'en_US'
            validator: Validators.compose([
              Validators.error<String?>(errors['language'], _language),
              Validators.required<String?>(ctx)
            ]),
            onSaved: (value) => _language = value),
        SizedBox(height: spacing),
        DropdownInput<String?>(
            l10nKey: 'Account.langRef',
            iconData: Icons.language_rounded,
            items: _languageMap,
            initialValue: widget.entity?.langRef,
            validator: Validators.error<String?>(errors['_langRef'], _langRef),
            onSaved: (value) => _langRef = value),
        SizedBox(height: spacing),
        DropdownInput<double>(
            l10nKey: 'review_base',
            l10nKeySummary: 'review_base_summary',
            iconData: Icons.date_range_rounded,
            items: _frequencyMap,
            initialValue: widget.entity?.reviewFrequency ?? 2.0,
            validator: Validators.compose([
              Validators.error<double?>(
                  errors['reviewFrequency'], _reviewFrequency),
              Validators.required<double?>(ctx)
            ]),
            onSaved: (value) => _reviewFrequency = value),
        SizedBox(height: spacing),
        IntegerInput(
            l10nKey: 'review_limit',
            l10nKeySummary: 'review_limit_summary',
            iconData: Icons.event,
            initialValue: widget.entity?.reviewLimit,
            validator:
                Validators.error<int?>(errors['reviewLimit'], _reviewLimit),
            onSaved: (value) => _reviewLimit = value),
        SizedBox(height: spacing),
        IntegerInput(
            l10nKey: 'daily_goal',
            iconData: Icons.flag,
            initialValue: widget.entity?.dailyGoal,
            validator: Validators.error<int?>(errors['dailyGoal'], _dailyGoal),
            onSaved: (value) => _dailyGoal = value),
        SizedBox(height: spacing),
        DropdownInput<String>(
            l10nKey: 'Account.canon',
            iconData: Icons.sort,
            items: _canonMap,
            initialValue: widget.entity?.canon ?? 'lut',
            validator: Validators.compose([
              Validators.error<String?>(errors['canon'], _canon),
              Validators.required<String?>(ctx)
            ]),
            onSaved: (value) => _canon = value),
      ]);
  }

  void _options(BuildContext ctx) {
    _languageMap = {
      //Localizations.localeOf(context).toString():
      //LocaleNames.of(context).nameOf(Localizations.localeOf(context).toString()),
      'de': LocaleNames.of(ctx)!.nameOf('de'),
      'en': LocaleNames.of(ctx)!.nameOf('en'),
    };
    _languageMap!.putIfAbsent(
        Localizations.localeOf(ctx).toString(),
        () => LocaleNames.of(ctx)!
            .nameOf(Localizations.localeOf(ctx).toString()));
    if (widget.entity?.language != null) {
      _languageMap!.putIfAbsent(widget.entity!.language,
          () => LocaleNames.of(ctx)!.nameOf(widget.entity!.language));
    }
    if (widget.entity?.langRef != null) {
      _languageMap!.putIfAbsent(
          widget.entity!.langRef,
          () => LocaleNames.of(ctx)!
              .nameOf(Localizations.localeOf(ctx).toString()));
    }
    _frequencyMap = {
      1.25: L10n.of(ctx)!.t8('Frequency.veryOften'),
      1.5: L10n.of(ctx)!.t8('Frequency.often'),
      2.0: L10n.of(ctx)!.t8('Frequency.normally'),
      2.5: L10n.of(ctx)!.t8('Frequency.rarely'),
      3.0: L10n.of(ctx)!.t8('Frequency.veryRarely')
    };

    _canonMap = {
      'lut': L10n.of(ctx)!.t8('Canon.lut'),
    };
  }
}
