import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/entities/entities.dart';
import 'package:remem_me/dialogs/deck_publish_dialog.dart';
import 'package:remem_me/screens/tags_screen.dart';
import 'package:repository_core/repository_core.dart';

/// Navigation endpoint for deck management
/// Screen for viewing and managing a list of decks
class DecksScreen extends TagsScreen {
  final icon = Icons.public_rounded;

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: AppBar(
        title: Text(L10n.of(ctx)!.t8('Decks.edit')!),
      ),
      body: Container(margin: EdgeInsets.all(16.0), child: content(ctx)),
    );
  }

  Widget tagItem(BuildContext ctx, TagEntity tag) {
    return ListTile(
      leading:
          Icon(tag.published! ? Icons.public_rounded : Icons.public_off_rounded),
      title: Text(tag.text),
      onTap: () =>
          tag.published! ? _updateDeck(ctx, tag) : _createDeck(ctx, tag),
    );
  }

  Future<void> _createDeck(BuildContext ctx, TagEntity tag) async {
    final repository = RepositoryProvider.of<DecksRepository>(ctx);
    await showDialog<DeckEntity>(
        context: ctx,
        builder: (BuildContext context) => DeckPublishDialog(
              deck: DeckEntity(id: tag.id, name: tag.text),
              onSave: (deck) async {
                await repository.create(deck);
                BlocProvider.of<TagsBloc>(ctx).add(EntitiesUpdated<TagEntity>(
                    [tag.copyWith(published: true)],
                    persist: false));
              },
            ));
  }

  Future<void> _updateDeck(BuildContext ctx, TagEntity tag) async {
    final repository = RepositoryProvider.of<DecksRepository>(ctx);
    final deck = await repository.read(tag.id);
    await showDialog<DeckEntity>(
        context: ctx,
        builder: (BuildContext context) => DeckPublishDialog(
              deck: deck,
              onSave: (deck) async {
                await repository.update(deck);
                BlocProvider.of<TagsBloc>(ctx).add(EntitiesUpdated<TagEntity>(
                    [tag.copyWith(published: true)],
                    persist: false));
              },
              onDelete: (deck) async {
                await repository.delete(deck);
                BlocProvider.of<TagsBloc>(ctx).add(EntitiesUpdated<TagEntity>(
                    [tag.copyWith(published: false)],
                    persist: false));
              },
            ));
  }
}
