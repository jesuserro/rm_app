import 'package:flutter/material.dart';
import 'package:remem_me/widgets/form/entity_form.dart';
import 'package:remem_me/widgets/form/form.dart';
import 'package:remem_me/widgets/loading_indicator.dart';
import 'package:repository_core/repository_core.dart';

class DeckPublishDialog extends EntityForm<DeckEntity> {
  const DeckPublishDialog(
      {required OnSaveCallback<DeckEntity> onSave,
      OnSaveCallback<DeckEntity>? onDelete,
      DeckEntity? deck})
      : super(onSave: onSave, onDelete: onDelete, entity: deck);

  @override
  State<StatefulWidget> createState() {
    return _DeckPublishDialogState();
  }
}

/// Dialog for editing the name of a deck
class _DeckPublishDialogState
    extends EntityFormState<DeckEntity, DeckPublishDialog> {
  String? _name;
  String? _description;
  String? _website;
  String? _image;

  // todo: loadingindicator when deleting

  @override
  Widget build(BuildContext ctx) {
    return AlertDialog(
      content: Container(
          width: double.maxFinite,
          child: Stack(children: [
            Form(key: formKey, child: ListView(children: fields(ctx))),
            if (isUpdating) Positioned.fill(child: LoadingIndicator()),
          ])),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.public_off_rounded),
          onPressed: deleteEntity,
        ),
        IconButton(
          icon: Icon(
            Icons.public_rounded,
            color: Theme.of(ctx).colorScheme.primary,
          ), //Text(L10ns.of(ctx).t8('ok')),
          onPressed: saveForm,
        ),
      ],
    );
  }

  @override
  List<Widget> fields(BuildContext ctx) {
    return super.fields(ctx)
      ..addAll([
        StringInput(
            l10nKey: 'Deck.name',
            iconData: Icons.public_rounded,
            initialValue: widget.entity?.name,
            autofocus: widget.entity == null,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: Validators.compose([
              Validators.error<String?>(errors['name'], _name),
              Validators.required<String?>(ctx),
              Validators.minLength(ctx, 3)
            ]),
            onSaved: (value) => _name = value),
        SizedBox(height: spacing),
        StringInput(
            l10nKey: 'Deck.description',
            iconData: Icons.description_outlined,
            maxLines: 20,
            initialValue: widget.entity?.description,
            autofocus: widget.entity == null,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: Validators.compose([
              Validators.error<String?>(errors['description'], _description),
              Validators.required<String?>(ctx),
              Validators.minLength(ctx, 10)
            ]),
            onSaved: (value) => _description = value),
        SizedBox(height: spacing),
        StringInput(
            l10nKey: 'Deck.website',
            iconData: Icons.link_rounded,
            initialValue: widget.entity?.website,
            autofocus: widget.entity == null,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            validator: Validators.compose([
              Validators.url(ctx),
              Validators.error<String?>(errors['website'], _website),
            ]),
            onSaved: (value) => _website = value),
        /*SizedBox(height: spacing),
      StringInput(
          l10nKey: 'Deck.image',
          iconData: Icons.image_rounded,
          initialValue: widget.entity?.image,
          autofocus: widget.entity == null,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          validator: Validators.compose([
            Validators.url(ctx),
            Validators.error<String>(errors['image'], _image),
          ]),
          onSaved: (value) => _image = value),*/
      ]);
  }

  @override
  DeckEntity createdEntity() {
    return DeckEntity(
        name: _name!,
        description: _description!,
        website: _website,
        image: _image);
  }

  @override
  DeckEntity updatedEntity() {
    return widget.entity!.copyWith(
        name: _name,
        description: _description,
        website: _website,
        image: _image,
        nullValues: [
          if (_website == null) 'website',
          if (_image == null) 'image',
        ]);
  }
}
