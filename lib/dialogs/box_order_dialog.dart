import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/models/box.dart';
import 'package:repository_core/repository_core.dart';

class BoxOrderDialog extends StatelessWidget {
  const BoxOrderDialog({
    Key? key,
    required this.tab, this.prevOrder,
  }) : super(key: key);

  final Box tab;
  final VerseOrder? prevOrder;

  @override
  Widget build(BuildContext ctx) {
    return SimpleDialog(
      title: Text(L10n.of(ctx)!.t8('order_by')!),
      children: <Widget>[
        _option(ctx, VerseOrder.ALPHABET, 'order_alphabet'),
        _option(ctx, VerseOrder.CANON, 'order_canon'),
        _option(ctx, VerseOrder.TOPIC, 'order_topic'),
        _option(ctx, VerseOrder.DATE, 'order_date'),
        _option(ctx, VerseOrder.LEVEL, 'order_level'),
        _option(ctx, VerseOrder.RANDOM, 'order_random'),
      ],
    );
  }

  Widget _option(BuildContext ctx, VerseOrder value, String l10nKey) {
    return RadioListTile<VerseOrder>(
          title: Text(L10n.of(ctx)!.t8(l10nKey)!),
          value: value,
          groupValue: this.prevOrder,
          onChanged: (VerseOrder? value) {
            Navigator.pop(ctx, value);
          },
        );
  }
}
