import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'utils/color.dart';

class AppTheme {
  static const Color appColorDark = Color(0xFFA30131);
  static const Color appColorLight = Color(0xffffbbbb);

  static final AppBarTheme appBarTheme = AppBarTheme(
    backgroundColor: AppColor.dark,
    brightness: Brightness.dark,
  );

  static final FloatingActionButtonThemeData fabTheme =
      FloatingActionButtonThemeData(backgroundColor: Colors.grey[700]);

  static final PageTransitionsTheme pageTransitionsTheme = PageTransitionsTheme(builders: {
    TargetPlatform.iOS: ZoomPageTransitionsBuilder(),
    TargetPlatform.android: ZoomPageTransitionsBuilder(),
  });

  static final TextTheme thinTextTheme = TextTheme(
    headline4: TextStyle(),
    // title: TextStyle(fontSize: 16.0, height: 1.2, fontWeight: FontWeight.w300),
    subtitle2:
        TextStyle(fontWeight: FontWeight.w300, fontSize: 18.0, height: 2.0),
    bodyText2: TextStyle(fontWeight: FontWeight.w300),
    // subhead: TextStyle(fontSize: 16.0, height: 1.2, fontWeight: FontWeight.w300),
    // caption: TextStyle(fontSize: 12.0, height: 1.2, fontWeight: FontWeight.w300),
    overline: TextStyle(
        fontSize: 12.0, fontWeight: FontWeight.w300, letterSpacing: 0.0),
  );

  static ThemeData light = ThemeData.from(
    colorScheme: AppColorScheme.light,
    textTheme: thinTextTheme.apply(displayColor: appColorDark),
  ).copyWith(
      pageTransitionsTheme: pageTransitionsTheme,
      appBarTheme: appBarTheme,
      indicatorColor: Flat.amber,
      floatingActionButtonTheme: fabTheme,
      iconTheme: IconThemeData(color: Colors.black45));

  static ThemeData dark = ThemeData.from(
    colorScheme: AppColorScheme.dark,
    textTheme: thinTextTheme.apply(displayColor: FlatDark.amber),
  ).copyWith(
    pageTransitionsTheme: pageTransitionsTheme,
    appBarTheme: appBarTheme,
    indicatorColor: Flat.amber,
    floatingActionButtonTheme: fabTheme,
    iconTheme: IconThemeData(color: Colors.white60),
    selectedRowColor: Colors.white10,
    toggleableActiveColor: FlatDark.amber,
  );

  static ThemeData appBar = ThemeData.from(
    colorScheme: AppColorScheme.appBar,
    textTheme: thinTextTheme,
  ).copyWith(
    appBarTheme: appBarTheme,
    indicatorColor: Flat.amber,
  );

/*static ThemeData light = ThemeData(
      //brightness: Brightness.light,
      appBarTheme: appBarTheme,
      colorScheme: AppColorScheme.light,
      textTheme: thinTextTheme.apply(displayColor: appColorDark),
      backgroundColor: Colors.blueGrey[100],
      selectedRowColor: Colors.blueGrey[50],
      dialogBackgroundColor: Colors.white,
      scaffoldBackgroundColor: Colors.white,
      toggleableActiveColor: AppColorScheme.dark.secondary,
      iconTheme: IconThemeData(color: Colors.black45), // same as ListTileTheme.iconColor
/*    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(primary: Flat.indigo)
    ),
    textButtonTheme: TextButtonThemeData(
      style: TextButton.styleFrom(primary: FlatDark.indigo)
    ),*/
      );*/

/*static ThemeData dark = ThemeData(
    brightness: Brightness.dark,
    appBarTheme: appBarTheme,
    colorScheme: AppColorScheme.dark,
    textTheme: thinTextTheme.apply(displayColor: appColorLight),
    toggleableActiveColor: AppColorScheme.dark.secondary,
    backgroundColor: Colors.blueGrey[700],
    selectedRowColor: Colors.blueGrey[800],
    dialogBackgroundColor: Colors.blueGrey[800],
    scaffoldBackgroundColor: Colors.blueGrey[900],
    // elevatedButtonTheme: ElevatedButtonThemeData(
    //    style: ElevatedButton.styleFrom(primary: Flat.indigo)),
    //textButtonTheme:
    //    TextButtonThemeData(style: TextButton.styleFrom(primary: Flat.indigo)),
  );*/
}
