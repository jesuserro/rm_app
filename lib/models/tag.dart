import 'package:repository_core/repository_core.dart';

class Tag extends TagEntity {
  final int? size;

  Tag(
    text, {
    included,
    int? account,
    bool? published,
    int? id,
    int? modified,
    bool? deleted,
    this.size, // transient
  }) : super(text,
            included: included,
            account: account,
            published: published,
            id: id,
            modified: modified,
            deleted: deleted);

  Tag.fromEntity(TagEntity entity, {this.size})
      : super(entity.text,
            included: entity.included,
            account: entity.account,
            published: entity.published,
            id: entity.id,
            modified: entity.modified,
            deleted: entity.deleted);

  Tag copyWith({
    String? text,
    bool? included,
    int? account,
    bool? published,
    int? id,
    int? modified,
    bool? deleted,
    int? size,
    List<String> nullValues = const [],
  }) {
    return Tag(
      text ?? this.text,
      included:
          included ?? (nullValues.contains('included') ? null : this.included),
      account: account ?? this.account,
      published: published ?? this.published,
      id: id ?? this.id,
      modified: modified ?? this.modified,
      deleted: deleted ?? this.deleted,
      size: size ?? (nullValues.contains('size') ? null : this.size),
    );
  }
}
