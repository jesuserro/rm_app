import 'package:equatable/equatable.dart';

class Pos extends Equatable {
  final int line;
  final int word;

  const Pos(this.line, this.word);

  @override
  List<Object> get props => [this.line, this.word];

  @override
  String toString() => 'Pos { line: $line, word: $word }';
}