import 'package:equatable/equatable.dart';

class Publisher extends Equatable {
  final int? id;
  final String? name;
  final String? language;
  final String? langRef;

  const Publisher(this.id, this.name, {this.language, this.langRef});

  @override
  List<Object?> get props => [id, name, language, langRef];


  Publisher copyWith({
    int? id,
    String? name,
    String? language,
    String? langRef,
  }) {
    return Publisher(id ?? this.id, name ?? this.name,
        language: language ?? this.language,
        langRef: langRef ?? this.langRef);
  }


  @override
  String toString() {
    return 'Collection { id: $id, name: $name, '
        'language: $language, langRef: $langRef }';
  }

  static Publisher fromJson(Map<String, dynamic> json) {
    return Publisher(
      json['id'] as int?,
      json['name'] as String?,
      language: json['language'] as String?,
      langRef: json['langRef'] as String?,
    );
  }
}
