import 'package:equatable/equatable.dart';

import 'collections.dart';

class Query extends Equatable {
  final String? search;
  final String? language;
  final CollectionOrder order;

  const Query({
    this.search,
    this.language = 'en',
    this.order = CollectionOrder.featured,
  });

  @override
  List<Object?> get props => [search, language, order];

  @override
  String toString() =>
      'Query { search: $search, language: $language, order: $order }';

  Query copyWith({
    String? search,
    String? language,
    CollectionOrder? order,
  }) {
    return Query(
      search: search ?? this.search,
      language: language ?? this.language,
      order: order ?? this.order,
    );
  }
}