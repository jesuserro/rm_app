class VisibilityFilter {
  final String query;
  final List<int> tags;

  VisibilityFilter({this.query = '', this.tags = const []});

  VisibilityFilter copyWith({String? query, List<int>? tags}) => VisibilityFilter(
        query: query ?? this.query,
        tags: tags ?? this.tags,
      );

  @override
  String toString() => 'VisibilityFilter { query: $query, tags: $tags }';
}
