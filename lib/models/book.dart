class Book {
  final String? key;
  final int? index;
  final String? name;

  Book(this.key, this.index, this.name);
}