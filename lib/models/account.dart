import 'package:remem_me/models/book.dart';
import 'package:repository_core/repository_core.dart';

class Account extends AccountEntity {
  final Map<String, Book>? books;

  Account(
    name, {
    language,
    langRef,
    reviewFrequency,
    reviewLimit,
    dailyGoal,
    orderNew,
    orderDue,
    orderKnown,
    orderAll,
    canon,
    int? id,
    int? modified,
    bool? deleted,
    this.books, // transient
  }) : super(name,
            language: language,
            langRef: langRef,
            reviewFrequency: reviewFrequency,
            reviewLimit: reviewLimit,
            dailyGoal: dailyGoal,
            orderNew: orderNew,
            orderDue: orderDue,
            orderKnown: orderKnown,
            orderAll: orderAll,
            canon: canon,
            id: id,
            modified: modified,
            deleted: deleted);

  Account.fromEntity(AccountEntity entity, {this.books = const {}})
      : super(entity.name,
            language: entity.language,
            langRef: entity.langRef,
            reviewFrequency: entity.reviewFrequency,
            reviewLimit: entity.reviewLimit,
            dailyGoal: entity.dailyGoal,
            orderNew: entity.orderNew,
            orderDue: entity.orderDue,
            orderKnown: entity.orderKnown,
            orderAll: entity.orderAll,
            canon: entity.canon,
            id: entity.id,
            modified: entity.modified,
            deleted: entity.deleted);

  Account copyWith({
    String? name,
    String? language,
    String? langRef,
    double? reviewFrequency,
    int? reviewLimit,
    int? dailyGoal,
    VerseOrder? orderNew,
    VerseOrder? orderDue,
    VerseOrder? orderKnown,
    VerseOrder? orderAll,
    String? canon,
    int? id,
    int? modified,
    bool? deleted,
    Map<String, Book>? books,
    List<String> nullValues = const [],
  }) {
    return Account(
      name ?? this.name,
      language: language ?? this.language,
      langRef:
          langRef ?? (nullValues.contains('langRef') ? null : this.langRef),
      reviewFrequency: reviewFrequency ?? this.reviewFrequency,
      reviewLimit: reviewLimit ??
          (nullValues.contains('reviewLimit') ? null : this.reviewLimit),
      dailyGoal: dailyGoal ??
          (nullValues.contains('dailyGoal') ? null : this.dailyGoal),
      orderNew: orderNew ?? this.orderNew,
      orderDue: orderDue ?? this.orderDue,
      orderKnown: orderKnown ?? this.orderKnown,
      orderAll: orderAll ?? this.orderAll,
      canon: canon ?? this.canon,
      id: id ?? this.id,
      modified: modified ?? this.modified,
      deleted: deleted ?? this.deleted,
      books: books ?? this.books,
    );
  }

  String referenceLanguage() {
    return langRef ?? language;
  }
}
