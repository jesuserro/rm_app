import 'package:flutter/material.dart';
import 'package:repository_core/repository_core.dart';
import 'package:time_machine/time_machine.dart';

class Verse extends VerseEntity {
  final LocalDate? due;
  final Color? color;
  final int? rank;  // canonical order index

  Verse(
    String? reference,
    String? passage, {
    String? source,
    String? topic,
    String? image,
    LocalDate? commit,
    LocalDate? review,
    int? level = -1,
    Map<int?, String?> tags = const {},
    int? account,
    int? id,
    int? modified,
    bool? deleted,
    this.due, // transient
    this.color, // transient
    this.rank, // transient
  }) : super(reference, passage,
            source: source,
            topic: topic,
            image: image,
            commit: commit,
            review: review,
            level: level,
            tags: tags,
            account: account,
            id: id,
            modified: modified,
            deleted: deleted);

  Verse.fromEntity(VerseEntity entity, {this.due, this.color, this.rank = 0})
      : super(entity.reference, entity.passage,
            source: entity.source,
            topic: entity.topic,
            image: entity.image,
            commit: entity.commit,
            review: entity.review,
            level: entity.level,
            tags: entity.tags,
            account: entity.account,
            id: entity.id,
            modified: entity.modified,
            deleted: entity.deleted);

  Verse copyWith({
    // todo: allow null values -> https://medium.com/@julianoaklein/a-better-way-to-copy-a-immutable-object-using-dart-9a92963b3ae2
    String? reference,
    String? passage,
    String? source,
    String? topic,
    String? image,
    LocalDate? commit,
    LocalDate? review,
    int? level,
    Map<int?, String?>? tags,
    int? account,
    int? id,
    int? modified,
    bool? deleted,
    LocalDate? due,
    Color? color,
    int? rank,
    List<String> nullValues = const [],
  }) {
    return Verse(
      reference ?? this.reference,
      passage ?? this.passage,
      source: source ?? (nullValues.contains('source') ? null : this.source),
      topic: topic ?? (nullValues.contains('topic') ? null : this.topic),
      image: image ?? (nullValues.contains('image') ? null : this.image),
      commit: commit ?? (nullValues.contains('commit') ? null : this.commit),
      review: review ?? (nullValues.contains('review') ? null : this.review),
      level: level ?? this.level,
      tags: tags ?? this.tags,
      account: account ?? this.account,
      id: id ?? this.id,
      modified: modified ?? this.modified,
      deleted: deleted ?? this.deleted,
      due: due ?? (nullValues.contains('due') ? null : this.due),
      color: color ?? (nullValues.contains('color') ? null : this.color),
      rank: rank ?? this.rank,
    );
  }
}
