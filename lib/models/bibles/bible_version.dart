import 'bible_site.dart';

class BibleVersion {
  final String? name;
  final String? language;
  final String? tag;
  final BibleSite? site;
  final Map<String, String>? books;

  BibleVersion(this.name, this.language, this.tag, this.site, this.books);
}