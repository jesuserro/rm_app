import 'package:html_unescape/html_unescape_small.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:remem_me/services/bible_query_service.dart';
import 'package:repository_hive/repository_hive.dart';
import 'bibles.dart';

class BibleQuery {
  Uri? _uri;
  late Uri _proxyUri;
  final BibleVersion version;
  final String bookKey;
  final int chapter;
  final int first;
  final int last;

  Uri? get uri {
    return _uri;
  }

  BibleQuery(versionKey, this.version, this.bookKey, this.chapter, this.first,
      this.last) {
    _uri = _getUri();
    _proxyUri = _getProxyUri(versionKey);
  }

  Future<String> get() async {
    final response =
        await http.get(!kIsWeb || version.site!.cors! ? _uri! : _proxyUri);
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body.substring(0, 100)}');
    final pattern = RegExp(
        _interpolate(version.site!.pattern!, lastPlusOne: true),
        dotAll: true);
    var result = pattern.firstMatch(response.body)![0]!;
    result = HtmlUnescape().convert(result);
    final includeNumbers = await Settings().loadBool(Settings.INCLUDE_NUMBERS);
    result = _extractText(result, includeNumbers ?? false);
    return result;
  }

  String _extractText(String result, bool includeNumbers) {
    final numberReplacements =
        includeNumbers ? version.site!.keepNumbers : version.site!.removeNumbers;
    final replacements = {
      ...numberReplacements,
      ...version.site!.removeClutter,
      '<br />': '\n',
    };
    replacements.forEach((key, value) {
      result = result.replaceAll(RegExp(key, dotAll: true), value);
    });
    return result;
  }

  Uri _getUri() {
    return Uri.parse(_interpolate(version.site!.url!));
  }

  Uri _getProxyUri(String? versionKey) {
    return Uri.parse(
        '$BIBLE_SERVER/${_interpolate(BIBLE_PROXY_PATH, versionKey: versionKey)}');
  }

  String _interpolate(String text,
      {bool lastPlusOne = false, String? versionKey}) {
    final start = first == 0 ? '1' : first;
    final end = last == 0
        ? ''
        : lastPlusOne
            ? last + 1
            : last;
    return text
        .replaceAll('%(tag)s', versionKey ?? version.tag!)
        .replaceAll('%(book)s', version.books![bookKey]!)
        .replaceAll('%(chap)d', chapter.toString())
        .replaceAll('%(start)d', start.toString())
        .replaceAll('%(end)d', end.toString());
  }
}
