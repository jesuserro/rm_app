import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:remem_me/models/bibles/bibles.dart';
import 'package:remem_me/models/models.dart';

import 'book_service.dart';
import 'verse_service.dart';

// const BIBLE_SERVER = 'http://192.168.1.110:8002'; // lan
// const BIBLE_SERVER = 'air.local:8002'; // ios
const BIBLE_SERVER = 'https://bible.remem.me'; // production
const BIBLE_PROXY_PATH =
    'proxy/?version=%(tag)s&book=%(book)s&chap=%(chap)d&verses=%(start)d-%(end)d';

/// Needs to be instantiated with a language before first use
class BibleQueryService {
  static final BibleQueryService _instance = BibleQueryService._internal();
  String? _language;
  late Map<String, BibleSite> sites;
  Map<String, BibleVersion>? versions;
  late Map<String, Map<String, String>> books;

  BibleQueryService._internal();

  factory BibleQueryService() {
    return _instance;
  }

  Future<void> init(String lang) async {
    lang = BookService().availableLocale(lang);
    if (_language != lang) {
      _language = lang;
      final uri = Uri.parse('$BIBLE_SERVER/patterns/$_language.json');
      final response = await http.get(uri);
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      final body = json.decode(utf8.decode(response.body.codeUnits));
      books = _booksFromJson(body);
      sites = _sitesFromJson(body);
      versions = _versionsFromJson(body);
    }
  }

  BibleQuery? bibleQuery(String versionKey, String reference,
      Map<String, Book> books) {
    final book = VerseService().book(reference, books);
    final chapter = VerseService().chapter(reference);
    var first = VerseService().first(reference);
    var last = VerseService().last(reference);
    BibleVersion? version = versions?[versionKey];
    if (version != null && book.key != null && chapter > 0) {
      return BibleQuery(versionKey, version, book.key!, chapter,
          first > 0 ? first : 1, last > 0 ? last : first);
    } else {
      return null;
    }
  }

  Map<String, BibleVersion> _versionsFromJson(json) {
    return Map<String, BibleVersion>.fromIterable(
      json['versions'].entries,
      key: (entry) => entry.key,
      value: (entry) => BibleVersion(
        entry.value['name'],
        entry.value['language'],
        entry.value['tag'],
        sites[entry.value['site']],
        books[entry.value['books']],
      ),
    );
  }

  Map<String, BibleSite> _sitesFromJson(json) {
    return Map<String, BibleSite>.fromIterable(
      json['sites'].entries,
      key: (entry) => entry.key,
      value: (entry) => BibleSite.fromJson(entry.value),
    );
  }

  Map<String, Map<String, String>> _booksFromJson(json) {
    return Map<String, Map<String, String>>.fromIterable(
      json['books'].entries,
      key: (books) => books.key,
      value: (books) => Map<String, String>.from(books.value),
    );
  }
}
