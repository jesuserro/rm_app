import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:remem_me/models/collections/collection.dart';
import 'package:remem_me/models/collections/collections.dart';
import 'package:remem_me/models/collections/query.dart';
import 'package:remem_me/models/models.dart';
import 'package:repository_core/repository_core.dart';
import 'package:repository_http/repository_http.dart';


const _fetchLimit = 20;


class CollectionService {
  static CollectionService? _instance;
  final http.Client httpClient;

  factory CollectionService(http.Client httpClient) {
    if (_instance == null) {
      _instance = CollectionService._internal(httpClient);
    }
    return _instance!;
  }

  CollectionService._internal(this.httpClient);


  Future<List<Collection>> fetchCollections(Query query, {offset = 0}) async {
    final uri = await replicationUri('/v1/collections/', {
      'search': query.search,
      'language': query.language,
      'ordering': _ordering(query.order),
      'limit': _fetchLimit.toString(),
      'offset': offset.toString()
    });
    final response = await httpClient.get(uri);
    if (response.statusCode == 200) {
      final body = json.decode(utf8.decode(response.body.codeUnits));
      return Batch.fromJson(body).results;
    }
    throw Exception('error fetching collections');
  }


  Future<Collection> fetchCollection(int id) async {
    final uri = await replicationUri('/v1/collections/${id.toString()}/');
    final response = await httpClient.get(uri);
    if (response.statusCode == 200) {
      final body = json.decode(utf8.decode(response.body.codeUnits));
      return Collection.fromJson(body);
    }
    throw Exception('error fetching verses');
  }


  bool hasReachedMax(int collectionsCount) =>
      collectionsCount < _fetchLimit;


  _ordering(CollectionOrder order) {
    switch(order) {
      case CollectionOrder.featured:
        return '-${describeEnum(CollectionOrder.featured)},-${describeEnum(CollectionOrder.downloads)}';
      case CollectionOrder.downloads:
      case CollectionOrder.created:
        return '-' + describeEnum(order);
      default:
        return describeEnum(order);
    }
  }


}



class Batch {
  final int? count;
  final String? next;
  final String? previous;
  final List<Collection> results;

  Batch(this.count, this.next, this.previous, this.results);

  static Batch fromJson(Map<String, dynamic> json) {
    return Batch(
        json['count'] as int?,
        json['next'] as String?,
        json['previous'] as String?,
        (json['results'] as List).map((dynamic json) {
          return Collection.fromJson(json);
        }).toList());
  }
}