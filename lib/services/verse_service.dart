import 'dart:math';
import 'dart:ui';

import 'package:collection/collection.dart' show IterableExtension;
import 'package:flutter/material.dart';
import 'package:remem_me/models/models.dart';
import 'package:remem_me/utils/color.dart';
import 'package:repository_core/repository_core.dart';
import 'package:time_machine/time_machine.dart';

class VerseService {
  static final VerseService _instance = VerseService._internal();

  factory VerseService() {
    return _instance;
  }

  VerseService._internal();

  List<Verse> mapVersesToBoxAll(List<Verse> verses, VerseOrder order) {
    // move to service
    final result = verses.where((verse) {
      return true;
    }).toList();
    result.sort(compare(order));
    return result;
  }

  List<Verse> mapVersesToBoxNew(List<Verse> verses, VerseOrder order) {
    final result = verses.where((verse) {
      return verse.due == null;
    }).toList();
    result.sort(compare(order));
    return result;
  }

  List<Verse> mapVersesToBoxDue(List<Verse> verses, VerseOrder order) {
    final today = LocalDate.today();
    final result = verses.where((verse) {
      return verse.due != null && verse.due! <= today;
    }).toList();
    result.sort(compare(order));
    return result;
  }

  List<Verse> mapVersesToBoxKnown(List<Verse> verses, VerseOrder order) {
    final today = LocalDate.today();
    final result = verses.where((verse) {
      return verse.due != null && verse.due! > today;
    }).toList();
    result.sort(compare(order));
    return result;
  }

  static int Function(Verse, Verse) compare(VerseOrder order) {
    // move to service
    switch (order) {
      case VerseOrder.ALPHABET:
        return (a, b) => _or(a, b, order, VerseOrder.TOPIC);
      case VerseOrder.CANON:
        return (a, b) => _or(a, b, order, VerseOrder.ALPHABET);
      case VerseOrder.TOPIC:
        return (a, b) => _or(a, b, order, VerseOrder.CANON, VerseOrder.ALPHABET);
      case VerseOrder.DATE:
        return (a, b) => _or(a, b, order, VerseOrder.CANON, VerseOrder.ALPHABET);
      case VerseOrder.LEVEL:
        return (a, b) => _or(a, b, order, VerseOrder.CANON, VerseOrder.ALPHABET);
      case VerseOrder.RANDOM:
        return _compare(order);
    }
  }

  static int Function(Verse, Verse) _compare(VerseOrder order) {
    // move to service
    switch (order) {
      case VerseOrder.ALPHABET:
        return (a, b) => a.reference!.compareTo(b.reference!);
      case VerseOrder.CANON:
        return (a, b) => (a.rank ?? 0).compareTo(b.rank ?? 0);
      case VerseOrder.TOPIC:
        return (a, b) => (a.topic ?? '').compareTo(b.topic ?? '');
      case VerseOrder.DATE:
        final zero = LocalDate(2001, 1, 1);
        return (a, b) => (a.due ?? zero).compareTo(b.due ?? zero);
      case VerseOrder.LEVEL:
        return (a, b) => a.level!.compareTo(b.level!);
      case VerseOrder.RANDOM:
        return (a, b) => Random().nextInt(2) * 2 - 1;
    }
  }

  static int _or(Verse a, Verse b, VerseOrder order1, VerseOrder order2,
      [VerseOrder? order3]) {
    final result1 = _compare(order1)(a, b);
    if (result1 != 0) return result1;
    final result2 = _compare(order2)(a, b);
    if (result2 != 0 || order3 == null) return result2;
    return _compare(order3)(a, b);
  }

  List<Verse> fromEntities(
      List<VerseEntity> entities, Map<String, Book>? books) {
    return entities
        .map<Verse>((VerseEntity entity) => Verse.fromEntity(entity,
            due: due(
                commit: entity.commit,
                review: entity.review,
                level: entity.level),
            color: color(entity.level!),
            rank: rank(entity.reference!, books!)))
        .toList();
  }

  Color color(int level) {
    return level < 0 ? Flat.grey : FlatDark.wheel[level % Flat.wheel.length];
  }

  LocalDate? due({LocalDate? commit, LocalDate? review, int? level}) {
    if (review != null && level! > -1) {
      return review.addDays(_dueOffset(level));
    } else {
      return commit;
    }
  }

  int _dueOffset(int level) {
    return level > 0 ? pow(2, level - 1) as int : 0;
  }

  Verse committed(Verse v) {
    return v.copyWith(
        commit: LocalDate.today(),
        due: due(commit: LocalDate.today()),
        level: 0,
        color: color(0));
  }

  Verse forgotten(Verse v) {
    return v.copyWith(
        review: LocalDate.today(),
        due: due(review: LocalDate.today(), level: 0),
        level: 0,
        color: color(0));
  }

  Verse remembered(Verse v) {
    final level = v.level! + 1;
    return v.copyWith(
        review: LocalDate.today(),
        due: due(review: LocalDate.today(), level: level),
        level: level,
        color: color(level));
  }

  Verse movedToNew(Verse v) {
    return v
        .copyWith(level: -1, nullValues: ['commit', 'review', 'due', 'color']);
  }

  Verse movedToDue(Verse v) {
    assert(v.review != null && v.level! > 0);
    final level = v.level! - 1;
    final review = LocalDate.today().subtractDays(_dueOffset(level));
    return v.copyWith(
        review: LocalDate.today().subtractDays(_dueOffset(level)),
        due: due(review: review, level: level),
        level: level,
        color: color(level));
  }

  Verse addTag(Verse v, TagEntity tag) {
    final tags = Map<int?, String?>.of(v.tags);
    tags[tag.id] = tag.text;
    return v.copyWith(tags: tags);
  }

  Verse removeTag(Verse v, TagEntity tag) {
    final tags = Map<int?, String?>.of(v.tags);
    tags.remove(tag.id);
    return v.copyWith(tags: tags);
  }

  bool hasTag(Verse v, TagEntity tag) {
    return v.tags.containsKey(tag.id);
  }

  int rank(String reference, Map<String, Book> books) {
    return book(reference, books).index! * 1000000 +
        chapter(reference) * 1000 +
        first(reference);
  }

  Book book(String reference, Map<String, Book> books) {
    final pattern = books.keys.firstWhereOrNull((key) =>
        reference.toLowerCase().startsWith(RegExp('$key\\.?\\s*(?=\\d)')));
    return pattern == null ? Book(null, 0, null) : books[pattern]!;
  }

  int chapter(String reference) {
    final pattern = RegExp(r'(?<=\D+)\d+');
    final match = pattern.firstMatch(reference);
    return match == null ? 0 : int.parse(match.group(0)!);
  }

  int first(String reference) {
    final pattern = RegExp(r'(?<=\d+\s*[,.:])\d+');
    final match = pattern.firstMatch(reference);
    return match == null ? 0 : int.parse(match.group(0)!);
  }

  int last(String reference) {
    final pattern = RegExp(r'(?<=\d+\s*[-])\d+$');
    final match = pattern.firstMatch(reference);
    return match == null ? 0 : int.parse(match.group(0)!);
  }
}
