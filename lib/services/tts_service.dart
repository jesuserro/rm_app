import 'dart:async';

import 'package:flutter_tts/flutter_tts.dart';
import 'package:remem_me/blocs/text_to_speech/tts.dart';
import 'package:remem_me/models/models.dart';

class TtsService {
  static TtsService? _instance;
  final TtsBloc ttsBloc;

  factory TtsService(TtsBloc ttsBloc) {
    if (_instance == null) {
      _instance = TtsService._internal(ttsBloc);
    }
    return _instance!;
  }

  TtsService._internal(this.ttsBloc);

  final flutterTts = FlutterTts();

  Future speak(Verse verse, Account account) async {
    // await flutterTts.awaitSpeakCompletion(true);
    flutterTts.setCompletionHandler(() {
      flutterTts.setCompletionHandler(() {
        ttsBloc.add(TtsCompleted());
      });
      _speakPas(verse, account);
    });
    _speakRef(verse, account);
  }

  Future _speakRef(Verse verse, Account account) async {
    await _setLanguage(account.referenceLanguage());
    var ref = await flutterTts.speak(verse.reference!);
    print('Speak $ref');
  }

  Future _speakPas(Verse verse, Account account) async {
    if (account.langRef != null) {
      await _setLanguage(account.language);
    }
    var pas = await flutterTts.speak(verse.passage!);
    print('Speak $pas');
  }

  Future stop() async {
    var result = await flutterTts.stop();
    print('Stop speaking $result');
  }

  _setLanguage(String accountLocale) async {
    String lang;
    String? ctry;
    if (accountLocale.length > 2) {
      final parts = accountLocale.split(RegExp('_'));
      lang = parts[0];
      ctry = parts[1];
    } else {
      lang = accountLocale;
    }
    if (ctry != null && await (flutterTts.isLanguageAvailable('$lang-$ctry') as FutureOr<bool>)) {
      await flutterTts.setLanguage('$lang-$ctry');
    } else if (await (flutterTts.isLanguageAvailable(lang) as FutureOr<bool>)) {
      await flutterTts.setLanguage(lang);
    } else {
      List<dynamic> locales = await (flutterTts.getLanguages as FutureOr<List<dynamic>>);
      final alternative = locales.firstWhere(
              (locale) => locale is String && locale.startsWith(RegExp(lang)));
      if (alternative != null) {
        await flutterTts.setLanguage(alternative);
      } else {
        await flutterTts.setLanguage('en');
      }
    }
  }
}
