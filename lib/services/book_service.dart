import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:remem_me/models/models.dart';

class BookService {
  static final BookService _instance = BookService._internal();
  static const LOCALES = ['en', 'de'];
  static const CANONS = ['lut'];

  factory BookService() {
    return _instance;
  }

  BookService._internal();

  Future<Map<String, Book>> loadBooks(String locale, String canon) async {
    try {
      String booksJson =
          await rootBundle.loadString('assets/l10n/${availableLocale(locale)}_books.json');
      Map<String, dynamic> booksJsonMap = json.decode(booksJson);
      String canonJson =
          await rootBundle.loadString('assets/canon/${availableCanon(canon)}.json');
      Map<String, dynamic>? canonJsonMap = json.decode(canonJson);
      final books = booksJsonMap.map((key, value) {
        return MapEntry(key, Book(value[0], canonJsonMap![value[0]], value[1]));
      });
      return books;
    } on FlutterError catch (e) {
      print('Error while loading books. ${e.message}');
      return {};
    }
  }

  String availableLocale(String intended) {
    if (LOCALES.contains(intended)) return intended;
    final reduced = intended.substring(0, 2);
    if (LOCALES.any((available) => available.startsWith(reduced)))
      return reduced;
    return 'en';
  }


  String availableCanon(String intended) {
    if (CANONS.contains(intended)) return intended;
    return 'lut';
  }
}
