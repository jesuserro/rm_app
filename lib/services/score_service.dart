import 'package:remem_me/models/models.dart';
import 'package:repository_core/repository_core.dart';


class ScoreService {
  static final ScoreService _instance = ScoreService._internal();

  factory ScoreService() {
    return _instance;
  }

  ScoreService._internal();

  int numActive(List<Verse> verses) {
    return verses.where((verse) => verse.due == null).toList().length;
  }

  int numDue(List<Verse> verses) {
    return verses.where((verse) => verse.due != null).toList().length;
  }

  int totalScore(List<ScoreEntity> scores) {
    return scores.fold(0, (prev, element) => prev + element.change!);
  }
}