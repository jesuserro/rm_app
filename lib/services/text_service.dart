import 'dart:math';

import 'package:remem_me/models/models.dart';

class TextService {
  static final TextService _instance = TextService._internal();

  // (?<!y)x   negative lookbehind
  // x(?!y)    negative lookahead
  // \p{P}     any kind of punctuation character
  static final deliminator = RegExp(r'(?<!^\p{P})\s+(?!\p{P}+(\s+|$))',
      unicode: true);

  // todo: make language dependent, e.g. inject localized TextService using Provider
  final language = 'en';

  factory TextService() {
    return _instance;
  }

  TextService._internal();


  List<String> lines(String text) {
    return text.split(RegExp(r'\n+'));
  }

  List<List<String>> textToWords(String text) {
    return text
        .split(RegExp(r'\n+'))
        .map((line) =>

            line.trim().split(deliminator))
        .toList();
  }

  String strip(String word) {
    return word
        .toLowerCase()
        .replaceAll(RegExp(r'^\p{P}+', unicode: true), '')
        .replaceAll(RegExp(r'\p{P}+$', unicode: true), '');
  }

  int punctuationBefore(String word) {
    return RegExp(r'^\p{P}+', unicode: true)
        .firstMatch(word)
        ?.group(0)
        ?.length ??
        0;
  }

  int punctuationAfter(String word) {
    return RegExp(r'\p{P}+$', unicode: true)
        .firstMatch(word)
        ?.group(0)
        ?.length ??
        0;
  }

  Pos lastPos(List<List<String>> lines) {
    return Pos(lines.length - 1, lines[lines.length - 1].length - 1);
  }

  List<List<bool>> wordsToMarks(List<List<String>> lines) {
    final List<List<bool>> marks = [];
    for (var i = 0; i < lines.length; i++) {
      final line = <bool>[];
      for (var j = 0; j < lines[i].length; j++) {
        line.add(false);
      }
      marks.add(line);
    }
    return marks;
  }

  List<List<T>> copyItems<T>(List<List<T>> rows) {
    return rows.map((row) => [...row]).toList();
  }

  int countWordsOfText(String? text) {
    if (language.startsWith(RegExp(r'zh'))) {
      return 0; // todo: implement Chinese, Korean, Hebrew, Arabic...
    } else {
      return countWordsOfRows(textToWords(text!));
    }
  }

  int countWordsOfRows(List<List<String>> rows) {
    return rows.fold(0, (count, row) => count + row.length);
  }

  int countMarks(List<List<bool>> rows) {
    return rows.fold(
        0,
        (count, line) =>
            count + line.fold(0, ((count, mark) => mark ? count + 1 : count)));
  }

  bool startsWith(String? word, String letters) {
    return letters.length > 0 &&
        strip(word!).toLowerCase().startsWith(letters.toLowerCase());
  }

  String beginning(String text) {
    final firstWords = lines(text)[0].split(deliminator);
    return firstWords.getRange(0, min(3, firstWords.length)).join(' ') + ('…');
  }

  void mark(int index, List<List<bool>> marked) {
    var current = 0;
    for (var i = 0; i < marked.length; i++) {
      for (var j = 0; j < marked[i].length; j++) {
        if (marked[i][j]) continue; // not counting 'true'
        if (current == index) {
          marked[i][j] = true;
          return;
        }
        current += 1;
      }
    }
  }

}
