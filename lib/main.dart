import 'package:bloc/bloc.dart';
import 'package:provider/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_localized_locales/flutter_localized_locales.dart';
import 'package:message_core/message_core.dart';
import 'package:remem_me/blocs/blocs.dart';
import 'package:remem_me/blocs/text_to_speech/tts.dart';
import 'package:remem_me/routes.dart';
import 'package:repository_core/repository_core.dart';
import 'package:repository_http/repository_http.dart';

import 'theme.dart';

void main() {
  // We can set a Bloc's observer to an instance of `SimpleBlocObserver`.
  // This will allow us to handle all transitions and errors in SimpleBlocObserver.
  Bloc.observer = SimpleBlocObserver();
  runApp(
      // Provide top level blocs here, others in routes
      MultiBlocProvider(
          providers: [
        BlocProvider(create: _authBloc),
        BlocProvider(create: (_) => ProgressBloc()),
      ],
          child: Provider(
              create: (ctx) => HttpService(BlocProvider.of<ProgressBloc>(ctx)),
              child: MultiRepositoryProvider(
                  providers: [
                    RepositoryProvider<AccountsRepository>(create: (BuildContext ctx) => AccountsRepositoryHttp(
                      ctx.read<AuthBloc>().service, ctx.read<HttpService>())),
                    RepositoryProvider<VersesRepository>(create: (BuildContext ctx) => VersesRepositoryHttp(
                      ctx.read<AuthBloc>().service, ctx.read<HttpService>())),
                    RepositoryProvider<TagsRepository>(create: (BuildContext ctx) => TagsRepositoryHttp(
                      ctx.read<AuthBloc>().service, ctx.read<HttpService>())),
                  ],
                  child: BlocProvider(
                    create: _accountsBloc,
                    child: MultiBlocProvider(
                        providers: [
                          BlocProvider(
                            create: _versesBloc,
                          ),
                          BlocProvider(
                            create: _tagsBloc,
                          ),
                          BlocProvider(
                            lazy: false,
                            create: _scoresBloc,
                          ),
                          BlocProvider(
                            create: _ttsBloc,
                          ),
                        ],
                        child: BlocProvider(
                          create: _taggedVersesBloc,
                          child: VersesApp(),
                        )),
                  )))));
}

class VersesApp extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: AppTheme.light,
      darkTheme: AppTheme.dark,
      supportedLocales: [
        // compare with _L10nsDelegate.isSupported
        Locale('en'),
        Locale('de'),
      ],
      localizationsDelegates: [
        L10n.delegate,
        LocaleNamesLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      onGenerateTitle: (BuildContext ctx) => L10n.of(ctx)!.t8('app_name')!,
      onGenerateRoute: AppRoutes.generateRoute,
      initialRoute: AppRoutes.start,
      scaffoldMessengerKey: scaffoldMessengerKey,
    );
  }
}

TaggedVersesBloc _taggedVersesBloc(BuildContext ctx) => TaggedVersesBloc(
    versesBloc: BlocProvider.of<VersesBloc>(ctx),
    tagsBloc: BlocProvider.of<TagsBloc>(ctx));

ScoresBloc _scoresBloc(BuildContext ctx) => ScoresBloc(
    scoresRepository: ScoresRepositoryHttp(
        ctx.read<AuthBloc>().service, ctx.read<HttpService>()),
    accountsBloc: BlocProvider.of<AccountsBloc>(ctx));

TagsBloc _tagsBloc(BuildContext ctx) => TagsBloc(
    tagsRepository: RepositoryProvider.of<TagsRepository>(ctx),
    accountsBloc: BlocProvider.of<AccountsBloc>(ctx));

VersesBloc _versesBloc(BuildContext ctx) => VersesBloc(
    versesRepository: RepositoryProvider.of<VersesRepository>(ctx),
    accountsBloc: BlocProvider.of<AccountsBloc>(ctx));

AccountsBloc _accountsBloc(BuildContext ctx) => AccountsBloc(
    authBloc: BlocProvider.of<AuthBloc>(ctx),
    accountsRepository: RepositoryProvider.of<AccountsRepository>(ctx));

AuthBloc _authBloc(BuildContext ctx) =>
    AuthBloc()..add(AuthLoadedFromSettings());

TtsBloc _ttsBloc(BuildContext ctx) =>
    TtsBloc(accountsBloc: BlocProvider.of<AccountsBloc>(ctx));
