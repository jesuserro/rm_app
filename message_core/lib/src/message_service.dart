import 'package:message_core/message_core.dart';
import 'package:flutter/material.dart';

class MessageService {
  static final MessageService _instance = MessageService._internal();

  factory MessageService() {
    return _instance;
  }

  MessageService._internal();

  void info(String message) {
    scaffoldMessengerKey.currentState!
        .showSnackBar(SnackBar(content: Text(message),));
  }

  void warn(String message) {
    scaffoldMessengerKey.currentState!
        .showSnackBar(SnackBar(content: Text(message), duration: const Duration(seconds: 5),));
  }
}
