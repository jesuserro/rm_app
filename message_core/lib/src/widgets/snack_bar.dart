import 'package:flutter/material.dart';

class MessageSnackBar extends StatelessWidget {
  final String message;

  const MessageSnackBar(this.message, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SnackBar(content: Text(message));
  }
}
