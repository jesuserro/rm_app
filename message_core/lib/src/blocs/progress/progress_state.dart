import 'package:equatable/equatable.dart';

import '../../../message_core.dart';



class ProgressState extends Equatable {
  final int? id;
  final Progress progress;

  ProgressState(this.id, this.progress);

  @override
  List<Object?> get props => [id, progress];


  ProgressState copyWith({
    int? id,
    Progress? progress,
  }) {
    return ProgressState(
        id ?? this.id,
        progress ?? this.progress,
    );
  }


  @override
  String toString() => 'Progress { id: $id, progress: $progress }';
}

