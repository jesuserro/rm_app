import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../message_core.dart';

/// Tracks progress of activities like saving a list of entities
/// Can be extended to be used with a progress indicator
class ProgressBloc extends Bloc<ProgressEvent, ProgressState> {
  ProgressBloc() : super(ProgressState(null, Progress.STOP));

  @override
  Stream<ProgressState> mapEventToState(ProgressEvent event) async* {
    if (event is ProgressStarted) {
      yield* _mapProgressStartedToState(event);
    } else if (event is ProgressStopped) {
      yield* _mapProgressStoppedToState(event);
    }
  }


  Stream<ProgressState> _mapProgressStartedToState(ProgressStarted event) async* {
    yield ProgressState(event.id, Progress.RUN);
  }


  Stream<ProgressState> _mapProgressStoppedToState(ProgressStopped event) async* {
    yield ProgressState(event.id, Progress.STOP);
  }
}
