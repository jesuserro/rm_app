import 'package:equatable/equatable.dart';

abstract class ProgressEvent extends Equatable {
  final int id;
  const ProgressEvent(this.id);

  @override
  List<Object> get props => [id];
}

class ProgressStarted extends ProgressEvent {

  const ProgressStarted(int id): super(id);

  @override
  String toString() => 'ProgressStarted { id: $id }';
}

class ProgressStopped extends ProgressEvent {

  const ProgressStopped(int id): super(id);

  @override
  String toString() => 'ProgressStopped { id: $id }';
}
