import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MissingTranslationException implements Exception {
  String key;
  MissingTranslationException(this.key);
  String toString() => 'No translation found for $key.';
}

class L10n {
  static L10n? _current;
  final Locale locale;

  L10n(this.locale);

  // Helper method to keep the code in the widgets concise
  // Localizations are accessed using an InheritedWidget "of" syntax
  static L10n? of(BuildContext context) {
    return Localizations.of<L10n>(context, L10n);
  }

  // Static member to have a simple access to the delegate from the MaterialApp
  static const LocalizationsDelegate<L10n> delegate = _L10nsDelegate();

  // Convenience access to the current L10n without context
  static L10n? get current {
    return _current;
  }

  late Map<String, String> _localizedStrings;

  Future<bool> load() async {
    // Load the language JSON file from the "l10n" folder
    String jsonString =
    await rootBundle.loadString('assets/l10n/${locale.languageCode}.json');
    Map<String, dynamic> jsonMap = json.decode(jsonString);

    _localizedStrings = jsonMap.map((key, value) {
      return MapEntry(key, value.toString());
    });

    return true;
  }

  // This method will be called from every widget which needs a localized text
  String? t8(String key, [List<String>? params]) {
    if(_localizedStrings[key] == null) return key;
    return params != null ? interpolate(_localizedStrings[key], params) : _localizedStrings[key];
  }

  /// Replaces numbered placeholders ($1, $2, ...) with Strings
  static String? interpolate(String? text, List<String> params) {
    for(int i = 0; i < params.length; i++) {
      final placeholder = RegExp('#${i + 1}');
      text = text!.replaceFirst(placeholder, params[i]);
    }
    return text;
  }
}

// LocalizationsDelegate is a factory for a set of localized resources
// In this case, the localized strings will be gotten in an L10ns object
class _L10nsDelegate extends LocalizationsDelegate<L10n> {
  // This delegate instance will never change (it doesn't even have fields!)
  // It can provide a constant constructor.
  const _L10nsDelegate();

  @override
  bool isSupported(Locale locale) {
    // Include all of your supported language codes here
    return ['en', 'de'].contains(locale.languageCode);
  }

  @override
  Future<L10n> load(Locale locale) async {
    // L10ns class is where the JSON loading actually runs
    L10n localizations = new L10n(locale);
    await localizations.load();
    L10n._current = localizations;
    return localizations;
  }

  @override
  bool shouldReload(_L10nsDelegate old) => false;
}
