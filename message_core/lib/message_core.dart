library message_core;

import 'package:flutter/material.dart';

export 'src/widgets/snack_bar.dart';
export 'src/message_service.dart';
export 'src/models/progress.dart';
export 'src/blocs/progress/progress.dart';
export 'src/l10n.dart';
export 'src/models/message_exception.dart';

final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

