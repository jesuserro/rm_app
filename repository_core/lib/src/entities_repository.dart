import 'dart:async';
import 'dart:core';

import 'package:repository_core/src/models/entity.dart';

import 'models/entity.dart';



/// A class that Loads and Persists entities. The data layer of the app.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as entities_repository_simple or entities_repository_web.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment, such as
/// web or Flutter.
abstract class EntitiesRepository<T extends Entity> {
  /// Loads entities first from File storage. If they don't exist or encounter an
  /// error, it attempts to load the Entities from a Web Client.
  Future<List<T>> readList({ int? account, bool? deleted });

  // Persists entities to local disk and the web
  Future<void> createList(List<T> entities);

  // Persists entities to local disk and the web
  Future<void> updateList(List<T> entities);

  Future<void> deleteList(List<T> entities);

  Future<T?> read(int id);

  Future<void> create(T entity);

  Future<void> update(T entity);

  Future<void> delete(T entity);
}
