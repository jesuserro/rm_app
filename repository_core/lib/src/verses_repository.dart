import 'dart:core';

import '../repository_core.dart';
import 'models/verse.dart';



/// A class that Loads and Persists verses. The data layer of the app.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as verses_repository_simple or verses_repository_web.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment, such as
/// web or Flutter.
abstract class VersesRepository extends EntitiesRepository<VerseEntity> {
}
