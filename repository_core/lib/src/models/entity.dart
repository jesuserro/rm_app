import 'package:equatable/equatable.dart';
import 'package:repository_core/src/services/id_generator.dart';

class Entity extends Equatable {
  final int id;
  final int modified;
  final bool deleted;

  Entity({
    int? id,
    int? modified,
    bool? deleted,
  })  : this.id = id ?? IdGenerator().next(),
        this.modified = modified ?? DateTime.now().millisecondsSinceEpoch,
        this.deleted = deleted ?? false;

  Entity copyWith({
    int? id,
    int? modified,
    bool? deleted,
  }) {
    return Entity(
      id: id ?? this.id,
      modified: modified ?? this.modified,
      deleted: deleted ?? this.deleted,
    );
  }

  @override
  List<Object?> get props => [id, modified, deleted];


  @override
  String toString() {
    return 'Entity { id: $id, modified: $modified, deleted: $deleted }';
  }

  Map<String, Object?> toJson() {
    return {
      'id': id,
      'modified': modified,
      'deleted': deleted,
    };
  }

  static Entity fromJson(Map<String, dynamic> json) {
    return Entity(
      id: json['id'] as int?,
      modified: json['modified'] as int?,
      deleted: json['deleted'] as bool?,
    );
  }

  String? get label {
    return id.toString();
  }
}
