import 'entity.dart';

class TagEntity extends Entity {
  final String text;
  final bool? included;
  final int? account;
  final bool? published; // read only

  TagEntity(this.text,
      {this.included, this.account, this.published = false, int? id, int? modified, bool? deleted,})
      : super(id: id, modified: modified, deleted: deleted);

  TagEntity copyWith({
    String? text,
    bool? included,
    int? account,
    bool? published,
    int? id,
    int? modified,
    bool? deleted,
    List<String> nullValues = const [],
  }) {
    return TagEntity(text ?? this.text,
        included: included ??
            (nullValues.contains('included') ? null : this.included),
        account: account ?? this.account,
        published: published ?? this.published,
        id: id ?? this.id,
        modified: modified ?? this.modified,
        deleted: deleted ?? this.deleted);
  }

  @override
  List<Object?> get props => super.props + [text, included, account, published];

  @override
  String toString() {
    return 'Tag {  text: $text,  included: $included, account: $account, '
        'published: $published, '
        'id: $id, modified: $modified, deleted: $deleted }';
  }

  Map<String, Object?> toJson() {
    final map = super.toJson();
    map.addAll({
      'text': text,
      'included': included,
      'account': account,
    });
    return map;
  }

  static TagEntity fromJson(Map<String, dynamic> json) {
    return TagEntity(
      json['text'] as String,
      included: json['included'] as bool?,
      account: json['account'] as int?,
      published: json['published'] as bool?,
      id: json['id'] as int?,
      modified: json['modified'] as int?,
      deleted: json['deleted'] as bool?,
    );
  }

  @override
  String? get label {
    return text;
  }
}
