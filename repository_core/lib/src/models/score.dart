import 'package:time_machine/time_machine.dart';
import 'package:time_machine/time_machine_text_patterns.dart';

import 'entity.dart';

class ScoreEntity extends Entity {
  final int? account;
  final LocalDate? date;
  final String? vkey;
  final int? change;

  ScoreEntity(
      {required this.vkey,
      required this.date,
      required this.change,
      required this.account,
      int? id,
      int? modified,
      bool? deleted})
      : super(id: id, modified: modified, deleted: deleted);

  ScoreEntity copyWith({
    String? vkey,
    LocalDate? date,
    int? change,
    int? account,
    int? id,
    int? modified,
    bool? deleted,
    List<String> nullValues = const [],
  }) {
    return ScoreEntity(
        vkey: vkey ?? this.vkey,
        date: date ?? this.date,
        change: change ?? this.change,
        account: account ?? this.account,
        id: id ?? this.id,
        modified: modified ?? this.modified,
        deleted: deleted ?? this.deleted);
  }

  @override
  List<Object?> get props => super.props + [vkey, date, change, account];

  @override
  String toString() {
    return 'Score {  vkey: $vkey,  date: $date, change: $change, '
        'account: $account, id: $id, modified: $modified, deleted: $deleted }';
  }

  Map<String, Object?> toJson() {
    final map = super.toJson();
    map.addAll({
      'vkey': vkey,
      'date': date != null ? LocalDatePattern.iso.format(date!) : null,
      'change': change,
      'account': account,
    });
    return map;
  }

  static ScoreEntity fromJson(Map<String, dynamic> json) {
    return ScoreEntity(
      vkey: json['vkey'] as String?,
      date: json['date'] != null
          ? LocalDatePattern.iso.parse(json['date'] as String).value
          : null,
      change: json['change'] as int?,
      account: json['account'] as int?,
      id: json['id'] as int?,
      modified: json['modified'] as int?,
      deleted: json['deleted'] as bool?,
    );
  }
}
