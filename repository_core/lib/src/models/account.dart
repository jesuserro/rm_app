import '../../repository_core.dart';
import 'entity.dart';

class AccountEntity extends Entity {
  final String name;
  final String language;
  final String? langRef;
  final double? reviewFrequency;
  final int? reviewLimit;
  final int? dailyGoal;
  final String canon; // todo: add to database
  final VerseOrder orderNew;
  final VerseOrder orderDue;
  final VerseOrder orderKnown;
  final VerseOrder orderAll;


  AccountEntity(this.name,
      {this.language = 'en',
      this.langRef = 'en',
      this.reviewFrequency = 2.0,
      this.reviewLimit = 100,
      this.dailyGoal,
      this.canon = 'lut',
      this.orderNew = VerseOrder.CANON,
      this.orderDue = VerseOrder.LEVEL,
      this.orderKnown = VerseOrder.DATE,
      this.orderAll = VerseOrder.CANON,
      int? id,
      int? modified,
      bool? deleted})
      : super(id: id, modified: modified, deleted: deleted);

  AccountEntity copyWith({
    String? name,
    String? language,
    String? langRef,
    double? reviewFrequency,
    int? reviewLimit,
    int? dailyGoal,
    VerseOrder? orderNew,
    VerseOrder? orderDue,
    VerseOrder? orderKnown,
    VerseOrder? orderAll,
    int? id,
    int? modified,
    bool? deleted,
    List<String> nullValues = const [],
  }) {
    return AccountEntity(name ?? this.name,
        language: language ?? this.language,
        langRef:
            langRef ?? (nullValues.contains('langRef') ? null : this.langRef),
        reviewFrequency: reviewFrequency ?? this.reviewFrequency,
        reviewLimit: reviewLimit ??
            (nullValues.contains('reviewLimit') ? null : this.reviewLimit),
        dailyGoal: dailyGoal ??
            (nullValues.contains('dailyGoal') ? null : this.dailyGoal),
        orderNew: orderNew ?? this.orderNew,
        orderDue: orderDue ?? this.orderDue,
        orderKnown: orderKnown ?? this.orderKnown,
        orderAll: orderAll ?? this.orderAll,
        id: id ?? this.id,
        modified: modified ?? this.modified,
        deleted: deleted ?? this.deleted);
  }

  @override
  List<Object?> get props =>
      super.props +
      [
        name,
        language,
        langRef,
        reviewFrequency,
        reviewLimit,
        dailyGoal,
        orderNew,
        orderDue,
        orderKnown,
        orderAll,
        canon,
      ];

  @override
  String toString() {
    return 'Account {  name: $name,  language: $language,  langRef: $langRef, '
        'reviewFrequency: $reviewFrequency, reviewLimit: $reviewLimit,  '
        'dailyGoal: $dailyGoal, orderNew: $orderNew, orderDue: $orderDue, '
        'orderKnown: $orderKnown, orderAll: $orderAll, '
        'id: $id, modified: $modified, deleted: $deleted }';
  }

  Map<String, Object?> toJson() {
    final map = super.toJson();
    map.addAll({
      'name': name,
      'language': language,
      'lang_ref': langRef,
      'review_frequency': reviewFrequency,
      'review_limit': reviewLimit,
      'daily_goal': dailyGoal,
      'order_new': orderNew.index,
      'order_due': orderDue.index,
      'order_known': orderKnown.index,
      'order_all': orderAll.index,
    });
    return map;
  }

  static AccountEntity fromJson(Map<String, dynamic> json) {
    return AccountEntity(
      json['name'] as String,
      language: json['language'] as String,
      langRef: json['lang_ref'] as String?,
      reviewFrequency: json['review_frequency'] as double?,
      reviewLimit: json['review_limit'] as int?,
      dailyGoal: json['daily_goal'] as int?,
      orderNew: VerseOrder.values[json['order_new'] as int],
      orderDue: VerseOrder.values[json['order_due'] as int],
      orderKnown: VerseOrder.values[json['order_known'] as int],
      orderAll: VerseOrder.values[json['order_all'] as int],
      id: json['id'] as int?,
      modified: json['modified'] as int?,
      deleted: json['deleted'] as bool?,
    );
  }

  @override
  String? get label {
    return name;
  }
}
