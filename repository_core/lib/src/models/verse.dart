import 'package:repository_core/repository_core.dart';
import 'package:time_machine/time_machine.dart';
import 'package:time_machine/time_machine_text_patterns.dart';

class VerseEntity extends Entity {
  final String? reference;
  final String? passage;
  final String? source;
  final String? topic;
  final String? image;
  final LocalDate? commit;
  final LocalDate? review;
  final int? level;
  final Map<int?, String?> tags;
  final int? account;

  VerseEntity(this.reference, this.passage,
      {this.source,
      this.topic,
        this.image,
        this.commit,
      this.review,
      this.level = -1,
      this.tags = const {},
      this.account,
      int? id,
      int? modified,
      bool? deleted})
      : super(id: id, modified: modified, deleted: deleted);


  VerseEntity copyWith({
    String? reference,
    String? passage,
    String? source,
    String? topic,
    String? image,
    LocalDate? commit,
    LocalDate? review,
    int? level,
    Map<int, String>? tags,
    int? account,
    int? id,
    int? modified,
    bool? deleted,
    List<String> nullValues = const [],
  }) {
    return VerseEntity(
      reference ?? this.reference,
      passage ?? this.passage,
      source: source ?? (nullValues.contains('source') ? null : this.source),
      topic: topic ?? (nullValues.contains('topic') ? null : this.topic),
      image: image ?? (nullValues.contains('image') ? null : this.image),
      commit: commit ?? (nullValues.contains('commit') ? null : this.commit),
      review: review ?? (nullValues.contains('review') ? null : this.review),
      level: level ?? this.level,
      tags: tags ?? this.tags,
      account: account ?? this.account,
      id: id ?? this.id,
      modified: modified ?? this.modified,
      deleted: deleted ?? this.deleted,
    );
  }


  @override
  List<Object?> get props =>
      super.props +
      [passage, reference, source, topic, image, commit, review, level, tags, account];

  @override
  String toString() {
    return 'Verse {  reference: $reference, source: $source, topic: $topic, image: $image, '
        'passage: ${passage!.length > 10 ? passage!.replaceRange(10, passage!.length, '…') : passage}, '
        'commit: $commit, review: $review, level: $level, tags: $tags, '
        'account: $account, id: $id, modified: $modified, deleted: $deleted }';
  }

  @override
  Map<String, Object?> toJson() {
    final map = super.toJson();
    map.addAll({
      'reference': reference,
      'passage': passage,
      'source': source,
      'topic': topic,
      'image': image,
      'commit': commit != null ? LocalDatePattern.iso.format(commit!) : null,
      'review': review != null ? LocalDatePattern.iso.format(review!) : null,
      'level': level,
      'tags': tags.keys.toList(),
      'account': account,
    });
    return map;
  }

  static VerseEntity fromJson(Map<String, dynamic> json) {
    return VerseEntity(
      json['reference'] as String?,
      json['passage'] as String?,
      source: json['source'] as String?,
      topic: json['topic'] as String?,
      image: (json['image'] ?? json['safe_image']) as String?,
      commit: json['commit'] != null
          ? LocalDatePattern.iso.parse(json['commit'] as String).value
          : null,
      review: json['review'] != null
          ? LocalDatePattern.iso.parse(json['review'] as String).value
          : null,
      level: json['level'] != null ? json['level'] as int? : -1,
      tags: json['tags'] != null
          ? Map.fromIterable(json['tags'] as Iterable<dynamic>, key: (e) => e, value: (e) => null)
          : {},
      account: json['account'] as int?,
      id: json['id'] as int?,
      modified: json['modified'] as int?,
      deleted: json['deleted'] as bool?,
    );
  }


  @override
  String get label {
    return '$reference / $passage';
  }
}
