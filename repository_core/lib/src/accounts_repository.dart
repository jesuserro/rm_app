import 'dart:core';

import 'package:repository_core/src/entities_repository.dart';

import 'models/account.dart';



/// A class that Loads and Persists accounts. The data layer of the app.
///
/// How and where it stores the entities should defined in a concrete
/// implementation, such as accounts_repository_simple or accounts_repository_web.
///
/// The domain layer should depend on this abstract class, and each app can
/// inject the correct implementation depending on the environment, such as
/// web or Flutter.
abstract class AccountsRepository extends EntitiesRepository<AccountEntity> {}
