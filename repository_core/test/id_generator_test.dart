import 'package:test/test.dart';
import 'package:repository_core/src/services/id_generator.dart';

void main() {
  group('IdGenerator', () {

    test('Should generate id', () async {
      final offset = 1577836800000;
      final before = DateTime.now().millisecondsSinceEpoch;
      expect(before, greaterThan(offset));
      final id = IdGenerator().next();
      final after = DateTime.now().millisecondsSinceEpoch;
      expect(id, greaterThan(((before - offset) << 12) + 0));
      expect(id, lessThan(((after - offset) << 12) + 4096));
    });
  });
}
